console.log('main.js');


$('.handhelds_menu').click(function() {
    $(this).parent('.header').toggleClass('expanded');
    $('.handhelds_topbar').toggleClass('hidded');
});




var catalog = $('.catalog_block').isotope({
    itemSelector: '.catalog_block_item',
    percentPosition: true,
    masonry: {
        // columnWidth: 100,
        columnWidth: '.catalog_sizer',
        gutter: '.gutter_catalog'
    }
});


$('.faq_blocks').isotope({
    // options
    // layoutMode: 'fitRows',
    sortBy: 'original-order',
    sortAscending: true,
    percentPosition: true,
    itemSelector: '.faq_block',
    masonry: {
        columnWidth: '.faq_sizer',
        gutter: '.faq_gutter',
    }
});




// for debug only
// document.body.contentEditable = 'true';

$('.loading_resources').fadeOut("slow");


var zoomerInit = function(imgZoomer, zoomPlace) {
    if ($('img').is('.imgTrigger')) {
        var zoomImage = function(imgTrigger, paneContainerer) {



            var Drifteringer = new Drift(imgTrigger, {
                paneContainer: paneContainerer,
                inlinePane: false,
                inlineOffsetY: -85,
                containInline: true,
                sourceAttribute: 'data-zoom',
                // If present (and a function), this will be called
                // whenever the ZoomPane is shown.
                onShow: function() {
                    console.log('show');
                    $('.goodZoom').toggleClass('goodZoom_show');
                },
                // If present (and a function), this will be called
                // whenever the ZoomPane is hidden.
                onHide: function() {
                    console.log('hide');
                    $('.goodZoom').removeClass('goodZoom_show');
                }
            });
        };
        // console.log('imgTrigger true');
        zoomImage(imgZoomer, zoomPlace);
    } else {
        // console.log('imgTrigger false');
        return false;

    }
};




var goodSliderGallery = function(goodSliderClass, goodThumbsClass) {

    if (goodSliderClass.length) {

        var goodSlide = new Swiper(goodSliderClass, {
            // spaceBetween: 0,
            // spaceBetween: 10,
            lazyLoading: true,
            // centeredSlides: true,
            // touchRatio: 0.2,
            // loopedSlides:1,
            // slidesPerView: 1,

            onSlideChangeEnd: function(swiper) {
                // console.log('slide change end - after');
                // console.log(swiper);
                // console.log(swiper.activeIndex);
                // console.debug($('.swiper-slide-active img'));

                // инициализируем zoom для текущей картинки в слайдере товара при свайпе

            },
            onLazyImageReady: function(swiper) {
                // console.log(swiper.slides[swiper.activeIndex].firstElementChild);

                // инициализируем zoom для первой картинки в слайдере товара
                var zoomTarget = swiper.slides[swiper.activeIndex].firstElementChild;
                var zoomContainerGood = $('.goodZoom')[0];
                // var zoomContainer = $('.goodZoom');

                // console.log("ZoomTarget: " + zoomTarget);
                // console.log("zoomContainer: " + zoomContainerGood);

                zoomerInit(zoomTarget, zoomContainerGood);

            }
            // loop: true,
        });
    }


    if (goodThumbsClass.length) {

        var goodThumbs = new Swiper(goodThumbsClass, {
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            // touchRatio: 0.2,
            // loop: true,
            slideToClickedSlide: true
        });
        goodSlide.params.control = goodThumbs;
        goodThumbs.params.control = goodSlide;
    }

    // return false;
};











var swiper_news = new Swiper('.news_slider', {
    //    pagination: '.swiper-pagination',
    nextButton: '.swiper_block_slider_nav_next',
    prevButton: '.swiper_block_slider_nav_prev',
    autoplay: 6000,
    slidesPerView: 4,
    paginationClickable: true,
    spaceBetween: 76,
    breakpoints: {
        1440: {
            slidesPerView: 4,
            spaceBetween: 40
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 40
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 20
        }
        // ,
        // 320: {
        //     slidesPerView: 1,
        //     spaceBetween: 10
        // }
    }
});


var swiper_sert = new Swiper('.cert_slider', {
    //    pagination: '.swiper-pagination',
    nextButton: '.swiper_block_slider_nav_next',
    prevButton: '.swiper_block_slider_nav_prev',
    autoplay: false,
    slidesPerView: 5,
    paginationClickable: true,
    spaceBetween: 67,
    breakpoints: {
        1440: {
            slidesPerView: 4,
            spaceBetween: 40
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 40
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 20
        }
        // ,
        // 320: {
        //     slidesPerView: 1,
        //     spaceBetween: 10
        // }
    }
});


var swiper_stati = new Swiper('.stati_slider', {
    //    pagination: '.swiper-pagination',
    nextButton: '.swiper_block_slider_nav_next',
    prevButton: '.swiper_block_slider_nav_prev',
    autoplay: false,
    slidesPerView: 2,
    paginationClickable: true,
    spaceBetween: 76,
    breakpoints: {
        1440: {
            slidesPerView: 1,
            spaceBetween: 76
        },
    }
});









// активация счетчиков
runCounters = function() {
    $('.prefer_counter').each(function() {
        $(this).find('span').prop('Counter', 0).animate({
            Counter: $(this).find('span').text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function(now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
};
// пресеты для скроллревил
// пресеты для скроллревил
// пресеты для скроллревил
// пресеты для скроллревил





// ScrollReveal
// window.sr = ScrollReveal();


fooReveal2 = {
    // delay: 200,
    distance: '90px',
    easing: 'ease-in-out',
    rotate: { z: 10 },
    scale: 1.1,
    duration: 2000,
    // rotate: { x: 120 },
    // Callbacks that fire for each triggered element reveal, and reset.
    beforeReveal: function(domEl) {
        // console.log('beforeReveal-runCounters');
        runCounters();
    },
    beforeReset: function(domEl) {
        // console.log('222');
        // console.log('beforeReset-runCounters');
    },

    // Callbacks that fire for each completed element reveal, and reset.
    afterReveal: function(domEl) {
        // console.log('afterReveal-runCounters');
        // console.log('333');
    },
    afterReset: function(domEl) {
        // console.log('afterReset-runCounters');
        // console.log('444');
    }
};



// sr.reveal('.prefers_counters', function(){
//     console.log('1111');
// }, 700);

window.sr = ScrollReveal();
if ($('#preferzzzz').length) {
    var waypoint = new Waypoint({
        element: document.getElementById('preferzzzz'),
        handler: function() {
            // console.log('PREFER_COUNTERS: ');
            // console.log($("#preferzzzz").offset().top);
            sr.reveal('.prefers_counters', fooReveal2, 700);
        },
        offset: 130
    });
}



// sr.reveal('.prefers_counters');




// sr.reveal('.for_what_block', { duration: 2000 }, 900);
// console.log(window.sr);


// var fooReveal0 = {
//     delay: 200,
//     distance: '30px',
//     easing: 'ease-in-out',
//     // rotate: { z: 10 },
//     duration: 800,
//     scale: 1.1,
//     beforeReveal: function(domEl) {
//         console.log('Появление слогана');
//         $('.slogan').textillate({ in : {
//                 effect: 'fadeInDown'
//             }
//         });
//     },
//     beforeReset: function(domEl) {
//         // console.log('222');
//     },
//     // Callbacks that fire for each completed element reveal, and reset.
//     afterReveal: function(domEl) {
//         // console.log('333');
//     },
//     afterReset: function(domEl) {
//         // console.log('444');
//     }
// };




// var fooReveal1 = {
//     delay: 200,
//     distance: '90px',
//     easing: 'ease-in-out',
//     rotate: { z: 10 },
//     scale: 1.1,
//     duration: 2000,
//     beforeReveal: function(domEl) {
//         // console.log('1111');
//     },
//     beforeReset: function(domEl) {
//         // console.log('222');

//     },

//     // Callbacks that fire for each completed element reveal, and reset.
//     afterReveal: function(domEl) {
//         // console.log('333');
//         $('.for_what_block').each(function(index, el) {

//             $(this).css('transition', '0.2s all ease');
//         });

//     },
//     afterReset: function(domEl) {
//         console.log('444');

//     }

// };


// var fooReveal3 = {
//     delay: 200,
//     distance: '90px',
//     easing: 'ease-in-out',
//     rotate: { z: 80 },
//     scale: 3,
//     duration: 2000

// };

// пресеты для скроллревил
// пресеты для скроллревил
// пресеты для скроллревил
// пресеты для скроллревил


// sr.reveal('.for_what_block', fooReveal);
// sr.reveal('.for_what_block', { delay: 500, scale: 0.9, duration: 2000 }, 900);

// sr.reveal('.catalog_block_item', fooReveal1, 90);

// sr.reveal('.logo_list_item', fooReveal2, 10);
// sr.reveal('.prefers_counters');
// sr.reveal('.main_logo', fooReveal2, 10);


// скролим до слогана
// проверяем слоган
// if ($('#slogan').length) {

//     var waypoint = new Waypoint({
//         element: document.getElementById('menu_logo'),
//         handler: function(direction) {
//             sr.reveal('.for_what_block', fooReveal0, 300);

//             // console.log('Scrolled to waypoint!')
//         }
//     });
// }



// sr.reveal('.bar');


// $.stellar({
//     // scrollProperty: 'transform'
// });

// паралаксы
// var s = skrollr.init({});

// $('#showExamples').click(function(e){
//     e.stopPropagation();
//     e.preventDefault();
//     $('#examplesList').toggle();
// });

// $('html').click(function(){
//     $('#examplesList').hide();
// });

// подняться наверх
// var scrollDiv = $("#totop");
// $(scrollDiv).click(function() {
//     jQuery("html:not(:animated),body:not(:animated)").animate({ scrollTop: 0 }, 1000, 'easeInOutExpo');
// });

// плавный скролл везде
// $(document).bind('mousewheel', function(e) {
//     var nt = $(document.body).scrollTop() - (e.originalEvent.wheelDeltaY);
//     e.preventDefault();

//     $(document.body).stop().animate({
//         scrollTop: nt
//     }, 300);
// });

// var s = skrollr.init({
//     smoothScrolling: true
// });





$('.popup-modal').magnificPopup({
    type: 'inline',
    preloader: false,
    showCloseBtn: true,
    mainClass: 'mfp-fade',

    // focus: '#username',
    modal: false,
    callbacks: {
        open: function() {
            $('header').css('padding-right', 0);
            // Will fire when this exact popup is opened
            // this - is Magnific Popup object
            // console.log('111');

            // инициализация слайдера изображений товара в модальном окне
            var goodsliderPopup = $('.mfp-content').children().children().children('.good_slider');
            var goodthumbsPopup = $('.mfp-content').children().children().children('.good_thumbs');
            goodSliderGallery(goodsliderPopup, goodthumbsPopup);

        }
    }
});










// var filters = {};


// var $objects = $('.objects_list_items').isotope({
//     // options
//     itemSelector: '.objects_list_item',
//     // layoutMode: 'fitColumns',
//     percentPosition: true,
//     masonry: {
//         columnWidth: '.grid-sizer',
//         gutter: '.gutter-sizer',
//     }
// });

// // фильтруем категории
// $('.object_tag').click(function() {
//     $('.object_tag').removeClass('object_tag__selected');
//     $(this).addClass('object_tag__selected');
//     console.debug($(this).data('filter'));
//     switch ($(this).data('filter')) {
//         case '.category-a':
//             $objects.isotope({ filter: '.category-a' });
//             break;

//         case '.category-b':
//             $objects.isotope({ filter: '.category-b' });
//             break;

//         case '.category-c':
//             $objects.isotope({ filter: '.category-c' });
//             break;

//         case '.show_all':
//             $objects.isotope({ filter: '*' });
//             break;
//     }
// });

// $('.object_tag:first-child').trigger('click');






// This will create a single gallery from all elements that have class "gallery-item"
$('.objects_list_item, .popup_img').magnificPopup({
    removalDelay: 800,
    mainClass: 'mfp-fade',
    type: 'image',
    tLoading: 'Загрузка...',
    open: function() {
        $('header').css('padding-right', 0);
    },
    gallery: {
        enabled: false
    }
});

$('.view_object_image').magnificPopup({
    removalDelay: 800,
    mainClass: 'mfp-fade',
    type: 'image',
    tLoading: 'Загрузка...',
    open: function() {
        $('header').css('padding-right', 0);
    },
    gallery: {
        enabled: true
    }
});

$('.certificat').magnificPopup({
    removalDelay: 800,
    mainClass: 'mfp-fade',
    type: 'image',
    tLoading: 'Загрузка...',
    open: function() {
        $('header').css('padding-right', 0);
    },
    gallery: {
        enabled: true
    }
});


$('.bigpic').magnificPopup({
    removalDelay: 800,
    mainClass: 'mfp-fade',
    type: 'image',
    tLoading: 'Загрузка...',
    open: function() {
        $('header').css('padding-right', 0);
    },
    gallery: {
        enabled: true
    }
});





$('.ajax_popup').magnificPopup({
    removalDelay: 300,
    mainClass: 'mfp-fade',
    type: 'ajax',
    tLoading: 'Загрузка...',
    open: function() {
        $('header').css('padding-right', 0);
    },
    ajax: {
        settings: null, // Ajax settings object that will extend default one - http://api.jquery.com/jQuery.ajax/#jQuery-ajax-settings
        // For example:
        // settings: {cache:false, async:false}

        cursor: 'mfp-ajax-cur', // CSS class that will be added to body during the loading (adds "progress" cursor)
        tError: '<a href="%url%">Контент</a> не может быть загружен.' //  Error message, can contain %curr% and %total% tags if gallery is enabled
    },
    callbacks: {
        // beforeOpen: function() {
        //     this.st.mainClass = this.st.el.attr('data-effect');
        // },
        parseAjax: function(mfpResponse) {
            // mfpResponse.data is a "data" object from ajax "success" callback
            // for simple HTML file, it will be just String
            // You may modify it to change contents of the popup
            // For example, to show just #some-element:
            // mfpResponse.data = $(mfpResponse.data).find('#some-element');

            // mfpResponse.data must be a String or a DOM (jQuery) element

            // console.log('Ajax content loaded:', mfpResponse);
            console.log('Ajax content loaded');
        },
        ajaxContentAdded: function() {
            // Ajax content is loaded and appended to DOM



            // Прячем лишние параграфы и выводим первый
            // Прячем лишние параграфы
            // goodSliderGallery();
            $('.mfp-content p').css({
                'display': 'none'
            });
            // выводим первый
            $('.mfp-content p:first').first().css({
                'display': 'block',
                'max-height': '125px'
                // 'overflow': 'hidden'
            });



            // инициализируем zoom для первой картинки в слайдере товара
            // zoomerInit(document.querySelector('.swiper-slide>img'), document.querySelector('.goog_info'));
            // console.log(this.content);
        }
    }
});




$('.wallpaper_item').hover(function() {
    $(this).addClass('wallpaper_item__hovered');
}, function() {
    $(this).removeClass('wallpaper_item__hovered');

});





// скрываем меню при скроле вниз
// if ($("header").is('.header_notmain')) {
//     // console.debug('is');
//     // var menuoffsettop = $(".header_notmain").offset().top;
//     // $(window).scroll(function() {
//     //     if ($(window).scrollTop() > menuoffsettop) {
//     //         $(".header_notmain").css({
//     //             "opacity": "0"
//     //         });

//     //     } else {
//     //         $(".header_notmain").css({
//     //             "opacity": "1"
//     //         });

//     //     }
//     // });
//     // показываем меню после #showmenuhere
//     // var waypoint = new Waypoint({
//     //     element: document.getElementById('showmenuhere'),
//     //     handler: function(direction) {
//     //         if (direction == 'down') {
//     //             $(".header_notmain").addClass("top_nav__fixed");
//     //         } else if (direction == 'up') {
//     //             $(".header_notmain").removeClass("top_nav__fixed");
//     //         }
//     //     }
//     // });
// } else {

//     // console.debug('notis');
// }


// якоря
$('.anchor').click(function() {
    var link = $(this).attr('href');
    console.log(link);
    $("html:not(:animated),body:not(:animated)").animate({ scrollTop: $(link).offset().top - 70 }, 2000, 'easeInOutExpo');
    return false;
});

// $('.anchor').on('click', function(event) {
//     var link = $(this).attr('href');
//     $("html:not(:animated),body:not(:animated)").animate({ scrollTop: $(link).offset().top }, 2000, 'easeInOutExpo');
//     console.log(link);
//     event.preventDefault();
//     return false;

// });




// generic tools to help with the custom checkbox
function UTIL() {}
UTIL.prototype.bind_onclick = function(o, f) { // chain object onclick event to preserve prior statements (like jquery bind)
    var prev = o.onclick;
    if (typeof o.onclick != 'function') o.onclick = f;
    else o.onclick = function() {
        if (prev) { prev(); }
        f();
    };
};
UTIL.prototype.bind_onload = function(f) { // chain window onload event to preserve prior statements (like jquery bind)
    var prev = window.onload;
    if (typeof window.onload != 'function') window.onload = f;
    else window.onload = function() {
        if (prev) { prev(); }
        f();
    };
};
// generic css class style match functions similar to jquery
UTIL.prototype.trim = function(h) {
    return h.replace(/^\s+|\s+$/g, '');
};
UTIL.prototype.sregex = function(n) {
    return new RegExp('(?:^|\\s+)' + n + '(?:\\s+|$)');
};
UTIL.prototype.hasClass = function(o, n) {
    var r = this.sregex(n);
    return r.test(o.className);
};
UTIL.prototype.addClass = function(o, n) {
    if (!this.hasClass(o, n))
        o.className = this.trim(o.className + ' ' + n);
};
UTIL.prototype.removeClass = function(o, n) {
    var r = this.sregex(n);
    o.className = o.className.replace(r, '');
    o.className = this.trim(o.className);
};
var U = new UTIL();

function getElementsByClassSpecial(node, classname) {
    var a = [];
    var re = new RegExp('(^| )' + classname + '( |$)');
    var els = node.getElementsByTagName("*");
    for (var i = 0, j = els.length; i < j; i++)
        if (re.test(els[i].className)) a.push(els[i]);
    return a;
}


// specific to customized checkbox
function chk_labels(obj, init) {
    var objs = document.getElementsByTagName('LABEL');
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].htmlFor == obj.id) {
            if (!init) { // cycle through each label belonging to checkbox
                if (!U.hasClass(objs[i], 'chk')) { // adjust class of label to checked style, set checked
                    if (obj.type.toLowerCase() == 'radio') {
                        var radGroup = objs[i].className;
                        var res = radGroup.split(" ");
                        var newRes = res[0] + " " + res[1];
                        var relLabels = getElementsByClassSpecial(document.body, newRes);
                        for (var r = 0; r < relLabels.length; r++) {
                            U.removeClass(relLabels[r], 'chk');
                            U.addClass(relLabels[r], 'clr');
                        }
                        U.removeClass(objs[i], 'clr');
                        U.addClass(objs[i], 'chk');
                        obj.checked = true;
                    } else {
                        U.removeClass(objs[i], 'clr');
                        U.addClass(objs[i], 'chk');
                        obj.checked = true;
                    }
                } else { // adjust class of label to unchecked style, clear checked
                    U.removeClass(objs[i], 'chk');
                    U.addClass(objs[i], 'clr');
                    obj.checked = false;
                }
                return true;
            } else { // initialize on page load
                if (obj.checked) { // adjust class of label to checked style
                    U.removeClass(objs[i], 'clr');
                    U.addClass(objs[i], 'chk');
                } else { // adjust class of label to unchecked style
                    U.removeClass(objs[i], 'chk');
                    U.addClass(objs[i], 'clr');
                }
            }
        }
    }
}

// radio buttons
function chk_events(init) {
    var objs = document.getElementsByTagName('INPUT');
    if (typeof init == 'undefined') init = false;
    else init = init ? true : false;
    for (var i = 0; i < objs.length; i++) {
        if (objs[i].type.toLowerCase() == 'checkbox' || objs[i].type.toLowerCase() == 'radio') {
            if (!init) {
                U.bind_onclick(objs[i], function() {
                    chk_labels(this, init); // bind checkbox click event handler
                });
            } else chk_labels(objs[i], init); // initialize state of checkbox onload
        }
    }
}

U.bind_onload(function() { // bind window onload event
    chk_events(false); // bind click event handler to all checkboxes
    chk_events(true); // initialize
});









// mixItUp
$(function() {
    // var $filterSelect = $('input[type=radio][name=radiog_lite]'),
    $SortSelect = $('#SortSelect'),
        $container = $('#Container');

    $container.mixItUp();

    // $filterSelect.on('change', function() {
    //     $container.mixItUp('filter', this.value);
    // });


    // init Isotope
    var $containerGrid = $container.isotope({
        itemSelector: '.column_list',
        layoutMode: 'fitRows'
    });


    // bind filter on select change
    // $filterSelect.on('change', function() {
    //     // get filter value from option value
    //     var filterValue = this.value;
    //     $containerGrid.isotope({ filter: filterValue });
    // });

    $SortSelect.on('change', function() {
        console.debug('town triggered');
        // get filter value from option value
        var filterValueSel = this.value;
        $containerGrid.isotope({ filter: filterValueSel });
    });





});


$(function() {
    // var $filterSelect = $('input[type=radio][name=radiog_lite]'),
    $SortSelect = $('#SortSelect2'),
        $container = $('#Container2');

    $container.mixItUp();

    // $filterSelect.on('change', function() {
    //     $container.mixItUp('filter', this.value);
    // });


    // init Isotope
    var $containerGrid = $container.isotope({
        itemSelector: '.column_list',
        layoutMode: 'fitRows'
    });


    // bind filter on select change
    // $filterSelect.on('change', function() {
    //     // get filter value from option value
    //     var filterValue = this.value;
    //     $containerGrid.isotope({ filter: filterValue });
    // });

    $SortSelect.on('change', function() {
        console.debug('town triggered');
        // get filter value from option value
        var filterValueSel = this.value;
        $containerGrid.isotope({ filter: filterValueSel });
    });





});










var validateFormFu = function(formSelector) {

    // JavaScript-валидация формы
    $.extend($.validator.messages, {
        required: "Это обязательное поле.",
        remote: "Пожалуйста, поправьте это поле.",
        email: "Введите правильный email адрес.",
        url: "Пожалуйста, введите корректный URL.",
        date: "Укажите верную дату.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: $.validator.format("Please enter no more than {0} characters."),
        minlength: $.validator.format("Please enter at least {0} characters."),
        rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
        range: $.validator.format("Please enter a value between {0} and {1}."),
        max: $.validator.format("Please enter a value less than or equal to {0}."),
        min: $.validator.format("Please enter a value greater than or equal to {0}.")
    });

    formSelector.validate({
        errorClass: "error",
        // onkeyup: false,
        // onfocusout: false,
        submitHandler: function() {
            // alert("Submitted, thanks!");
            form.submit();
        }
    });
};

validateFormFu($('.validate_form'));

// jquery.maskedinputs
$(".phone_number").mask("+7(999) 999-9999");



$('.respTabs').responsiveTabs({
    startCollapsed: 'accordion'
});



// инициализация слайдера изображений товара в товарной карточке
var good_slidePage = $('.good_wr_page').children('.good_images').children('.good_slider');
var good_thumbsPage = $('.good_wr_page').children('.good_images').children('.good_thumbs');
goodSliderGallery(good_slidePage, good_thumbsPage);


// показ при скроле подняться наверх
$(window).scroll(function() {
    if ($(this).scrollTop() > 300) {
        $('.upbutton').fadeIn();
    } else {
        $('.upbutton').fadeOut();
    }
});

// подняться наверх
$(".upbutton").click(function() {
    $("html:not(:animated),body:not(:animated)").animate({ scrollTop: 0 }, 1000, 'easeInOutExpo')
});




// Следим за изменением ресайза
window.addEventListener("resize", function() {
    $('.section_onefortwocols').css('height', $('.section_onefortwocols').css('height'));
}, false);


// функция спойлера
var spoilerAdd = function(targetSection) {
    $(targetSection).wrap(function() {
        if ($(this).hasClass('spoilered')) {
            return "<div class='spoiler'></div>";
        } else {
            return "";
        }
    });
    $('.spoiler').after('<div class="show_text" title="Развернуть/скрыть"></div>');
    $('.show_text').click(function() {
        $('.spoiler').toggleClass('spoiler_show', 1000, "easeInOutExpo");
        $(this).toggleClass('show_text_top');
    });
};

// Добавляем спойлер на класс блок с классом section_onefortwocols
spoilerAdd('.section_onefortwocols');


// window.addEventListener("orientationchange", function() {
//     // Announce the new orientation number
//     alert(window.orientation);
// }, false);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////максимкин код///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
getContent();
$('.callback_block_form').on('submit', function(event) {
    var url = $(this).attr('action');
    var that = $(this);
    $.ajax({
        url: url,
        type: 'POST',
        // dataType: 'json',
        data: $(this).serialize(),
    }).done(function(data) {
        that.html(data);
    }).fail(function(data) {
        console.log("error");
    });
    return false;
});
$('.show_more').on('click', function(e) {
    e.preventDefault();
    var urll = $(this).data('requesurl');
    var page = $(this).data('page');
    var total = $(this).data('total');
    var perpage = $(this).data('perpage');
    var template = $(this).data('template');
    var that = $(this);
    var url = window.location.href;
    page++;
    if ((page + 1) * perpage >= total) {
        $(this).hide('slow');
    }
    $.ajax({
        url: urll,
        dataType: 'text',
        data: {
            p: page,
            transform: template
        },
    }).done(function(data) {
        $(data).insertBefore('.show_more');
        window.history.pushState({}, null, updateQueryStringParameter(url, "p", page));
    }).fail(function() {
        console.log("error");
    }).always(function() {
        that.data('page', page);
    });
    console.log(url);
    console.log(page);
});

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    console.debug(re);
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}
getContent();




function getContent() {
    $('.small_button').on('click', function(event) {
        event.preventDefault();
        var tagName = $(this).data('tag');
        var that = $(this);
        var url = window.location.href;
        var path = 'fields_filter[tags][eq][]';
        var newUrl = tagsQueryStringParameter(url, path, encodeURIComponent(tagName));

        $.ajax({
                url: newUrl,
            })
            .done(function(data) {
                $('#showmenuhere').replaceWith(data);
                window.history.pushState({}, null, newUrl);
                getContent();
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    });

    function tagsQueryStringParameter(uri, path, value) {
        // drop pag
        console.debug(uri);

        uri = uri.replace(/([?&]p=[\d]+)/g, "");
        console.debug(uri);

        var key = path.replace(/([\[\]\&])/g, '\\$1');
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        var result;
        if (uri.match(re)) {
            var re2 = new RegExp("([?&])" + key + "=" + value, "i");
            // вот тут если надо удалить
            if (uri.match(re2)) {
                var block = uri.match(re2);
                block = block[0].replace(/([\[\]\&\?])/g, '\\$1');
                var re3 = new RegExp(block, "i");
                result = uri.replace(re3, '');
                result = result.replace(/\&/, '?');
            }
            // вот тут если добавить
            else {
                result = uri + separator + path + "=" + value;
            }
        } else {
            result = uri + separator + path + "=" + value;
        }
        return result;
    }
}



$('input[name="diametr_mm"]').click(function() {
    $(this).parent().children().children().removeClass('current_var');
    $('.tolwina_stali_mm label').each(function(index) {
        $(this).css('display', 'none');
    });
    if ($(this).prop('checked')) {
        idInput = $(this).attr('id');
        $(this).parent().children('[for="' + idInput + '"]').children().toggleClass('current_var');
        $('.valuetd').each(function(index) {

            if (idInput == $(this).text()) {
                // console.log($(this).text()+' 123');
                $(this).parent().parent().children().children('.valuetdtolwina').each(function(index) {
                    // console.log($(this).text()+' 890');
                    // console.log(index);

                    tolwinaInputId = $(this).text();
                    $('[for="' + tolwinaInputId + '"]').hide();
                    // console.log(tolwinaInputId);
                    if (tolwinaInputId == $(this).text()) {
                        $('[for="' + tolwinaInputId + '"]').show();
                        // console.log('1111');
                    } else {}

                });
            }
        });
        $('.tolwina_stali_mm').css('display', 'block');
        // if ($('input[name="tolwina_stali_mm"]').prop('checked')) {
        //   $('input[name="tolwina_stali_mm"]').trigger('click');
        // }
        $('input[name="marka_stali"]').each(function(index) {
            if ($(this).prop('checked')) {
                $(this).trigger('click');
            }
        });
    }
});

$('input[name="wxb_mm"]').click(function() {
    $(this).parent().children().children().removeClass('current_var');
    if ($(this).prop('checked')) {
        idInput = $(this).attr('id');
        $(this).parent().children('[for="' + idInput + '"]').children().toggleClass('current_var');
        $('.tolwina_stali_mm').css('display', 'block');
        $('input[name="marka_stali"]').each(function(index) {
            if ($(this).prop('checked')) {
                $(this).trigger('click');
            }
        });
    }
});



// $('input[name="diametr_mm"]:first-child').trigger('click');


$('input[name="marka_stali"]').click(function() {
    // console.log(idInput);
    $(this).parent().children().children().removeClass('current_var');
    if ($(this).prop('checked')) {
        var tolwinaInput = $(this).attr('id');
        $(this).parent().children('[for="' + tolwinaInput + '"]').children().toggleClass('current_var');

        $('.valuetd').each(function(index) {

            if (idInput == $(this).text()) {
                // console.log($(this).text()+' 123');
                $(this).parent().parent().children().children('.valuetdtolwina').each(function(index) {
                    // console.log($(this).text()+' 890');

                    if (tolwinaInput == $(this).text()) {
                        $(this).parent().parent().parent().parent().show('ease');
                        // console.log('1111');
                    } else {
                        $(this).parent().parent().parent().parent().hide('ease');
                        // console.log('22222');
                    }

                });
            } else {
                $(this).parent().parent().parent().parent().hide('ease');
            }
        });
    }
});

$("input#button_support").click(function() {
    window.location.href = 'http://support.dymohodvulkan.ru/';
});

$("#phone_field").mask("+7(999) 999-9999");
$("#telefon_field").mask("+7(999) 999-9999");

if (('.prop_variants input:first-child').length) {
    $('.prop_variants input:first-child').click().click();
}



$('input[type=file]').nicefileinput();


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////конец максимкинового кода///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////