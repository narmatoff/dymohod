// Pace.on('done', function() {
var styles = [{
    "featureType": "administrative",
    "elementType": "labels.text.fill",
    "stylers": [{
        "color": "#444444"
    }]
}, {
    "featureType": "landscape",
    "elementType": "all",
    "stylers": [{
        "color": "#f2f2f2"
    }]
}, {
    "featureType": "poi",
    "elementType": "all",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "road",
    "elementType": "all",
    "stylers": [{
        "saturation": -100
    }, {
        "lightness": 45
    }]
}, {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [{
        "color": "#ff4e19"
    }, {
        "saturation": "100"
    }, {
        "lightness": "0"
    }, {
        "weight": "0.60"
    }, {
        "visibility": "on"
    }]
}, {
    "featureType": "road.highway",
    "elementType": "all",
    "stylers": [{
        "visibility": "simplified"
    }]
}, {
    "featureType": "road.arterial",
    "elementType": "labels.icon",
    "stylers": [{
        "visibility": "on"
    }]
}, {
    "featureType": "transit",
    "elementType": "all",
    "stylers": [{
        "visibility": "on"
    }]
}, {
    "featureType": "water",
    "elementType": "all",
    "stylers": [{
        "color": "#b5b5b5"
    }, {
        "visibility": "on"
    }]
}];
var inputVal = '';
var initMap = function() {
    google.maps.Map.prototype.panToWithOffset = function(latlng, offsetX, offsetY) {
        var map = this;
        var ov = new google.maps.OverlayView();
        ov.onAdd = function() {
            var proj = this.getProjection();
            var aPoint = proj.fromLatLngToContainerPixel(latlng);
            aPoint.x = aPoint.x + offsetX;
            aPoint.y = aPoint.y + offsetY;
            map.panTo(proj.fromContainerPixelToLatLng(aPoint));
        };
        ov.draw = function() {};
        ov.setMap(this);
    };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 3,
        gridSize: 50,
        center: {
            lat: 64.076053,
            lng: 93.156686
        }
    });
    var MARKER_PATH = '/templates/dymohod/dist/img/svg/baloon_middle.svg';
    // console.log(MARKER_PATH);
    map.setOptions({
        styles: styles,
        // draggable: true,
        // zoomControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: false
    });
    // Create a <script> tag and set the USGS URL as the source.
    // var script = document.createElement('script');
    // // This example uses a local copy of the GeoJSON stored at
    // // http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojsonp
    // script.src = 'http://vulkan.dev.lum.ru/udata/content/whereBuy/?json-callback=eqfeed_callback';
    // // script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
    // document.getElementsByTagName('head')[0].appendChild(script);
    addScript('/udata/content/whereBuy/?json-callback=eqfeed_callback');
    // addScript('js/markers.js');
    var markers = [];
    window.eqfeed_callback = function(results) {



        for (var i = 0; i < results.features.length; i++) {
            var coords = results.features[i].geometry.coordinates;

            var latLng = new google.maps.LatLng(coords[0], coords[1]);

            // добивка с дрожанием
            if(markers.length != 0) {
                for (j=0; j < markers.length; j++) {
                    var existingMarker = markers[j];
                    var pos = existingMarker.getPosition();
                    if (latLng.equals(pos)) {
                        var a = 360.0 / markers.length;
                        var newLat = pos.lat() + -0.000035 * Math.cos((+a*j) / 180 * Math.PI);  //x
                        var newLng = pos.lng() + -0.000035 * Math.sin((+a*j) / 180 * Math.PI);  //Y
                        var latLng = new google.maps.LatLng(newLat,newLng);
                    }
                }
            }
            // добивка с дрожанием

            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                animation: google.maps.Animation.DROP,
                icon: MARKER_PATH,
                // передаем id в google.maps.event.addListener(markers[i], 'click', function() {....}
                id: i
            });
            // console.debug(marker);
            // формируем массив markers с маркерами marker
            markers.push(marker);
            // вызываем по клику фу showInfoWindow для вывода инфо-окна
            infowindow = new google.maps.InfoWindow({});
            // console.log(markers[i]);
            var contentString = function(indexBaloon) {
                var prop = results.features[indexBaloon].properties;
                var result = '<div class="map_filterblock">';
                if (prop.place_name) {
                    result += '<div class="map_searchblock_name">' + prop.place_name + '</div>';
                }
                result += '<div class="column_list"><div class="column_list_name"><ul>';
                if (prop.place_addr) {
                    result += '<li><span class="column_list_text">' + prop.place_addr + '</span></li>';
                }
                if (prop.place_tel) {
                    result += '<li><span class="column_list_text"><b>Тел.</b><a href="tel:' + prop.place_tel + '">' + prop.place_tel + '</a>';
                    if (prop.place_tel2) {
                        result += ', <a href="tel:' + prop.place_tel2 + '">' + prop.place_tel2 + '</a>';
                    }
                    result += '</span></li>';
                }
                if (prop.place_worktime) {
                    result += '<li><span class="column_list_text"><b>Режим работы: </b>' + prop.place_worktime + '</result += span></li>';
                }
                if (prop.place_site) {
                    result += '<li><span class="column_list_text"><b>Сайт: </b><a href="' + prop.place_site + '" target="_blank">' + prop.place_site + '</a></span></li>';
                }
                if (prop.place_email) {
                    result += '<li><span class="column_list_text"><b>E-mail: </b><a href="mailto:kamin@mail.ru">' + prop.place_email + '</a></span></li>';
                }
                result += '</ul></div></div>';
                result += '<div class="map_filtbtn">';
                result += '<div>';
                result += '<img src="/templates/dymohod/dist/img/svg/marshrut.svg" alt=""><a href="' + prop.place_roadmap + '" target="_blank">Построить<br/> маршрут</a>';
                result += '</div>';
                if (prop.place_tel) {
                    result += '<div>';
                    result += '<img src="/templates/dymohod/dist/img/svg/online_call.svg" alt=""><a href="tel:' + prop.place_tel + '">Перезвонить сейчас?</a>';
                    result += '</div>';
                }
                result += '</div>';
                result += '<div class="column_list"><div class="column_list_name"><ul>';
                if (prop.service) {
                    result += '<li><span class="column_list_text"><b>Оказывает услуги: </b>' + prop.service + '</span></li>';
                }
                // if (prop.manager_spec) {
                //     result += '<li><span class="column_list_text">' + prop.manager_spec + '</span></li>';
                // }
                // if (prop.manager_phone) {
                //     result += '<li><span class="column_list_text"><b>Тел: </b><a href="tel:' + prop.manager_phone + '">' + prop.manager_phone + '</a></span></li>';
                // }
                // if (prop.manager_email) {
                //     result += '<li><span class="column_list_text"><b>E-mail: </b><a href="' + prop.manager_email + '">' + prop.manager_email + '</a></span></li>';
                // }
                result += '</ul></div></div>';
                result += '</div>';
                return result;
            };
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, this);
                infowindow.setContent(contentString(this.id));
                var mapWidth = map.getDiv().offsetWidth;
                var mapHeight = map.getDiv().offsetHeight;
                var offsetHeight = mapHeight / 2;
                map.panToWithOffset(this.getPosition(), 0, -offsetHeight);
                // map.panToWithOffset(this.getPosition(), 30, 550);
                // console.log(contentString(this.id));
                takeInfoWin = function() {}
                $('.map_spinner').hide();
            });
            map.addListener('drag', function() {
                $('.map_spinner').hide();
            });
            map.addListener('zoom_changed', function() {
                $('.map_spinner').hide();
            });
            // console.log(results.features[i].properties.place_name);
        };
        dataCords = function(idSs) {
            // $('.ui-menu-item').click(function() {
            // var currentPos = [0, 1];
            var simpleCoords = [];
            for (var i = 0; i <= 1; i++) {
                // console.log("for "+ results.features[idSs].geometry.coordinates[i]);
                simpleCoords.push(results.features[idSs].geometry.coordinates[i]);
            }
            // console.info("simpleCoords: "+simpleCoords);
            var simpCrds1 = marker.getPosition();
            var simpCrds = simpleCoords;
            var resArray = eval(simpCrds);
            // console.log("get position: " + typeof simpCrds1 + ", get position value: " + simpCrds1);
            // console.debug(simpCrds1);
            resArray = new google.maps.LatLng({
                lat: simpCrds[0],
                lng: simpCrds[1]
            });
            // console.log("get position2: " + typeof resArray + ", get position2 value: " + resArray);
            // console.debug(resArray);
            // console.log("get position2: " + coordinatezz);
            // markers[idSs].click();
            map.setZoom(12);
            map.setCenter(resArray);

                       
            infowindow.close();
            // console.log("устанавливаем зум и координаты по центру");
            // setTimeout(function() {
            // infowindow.open(map, marker);
            // infowindow.setContent(contentString(idSs));
            // console.log("устанавливаем конетн и открываем инфоокно");
            // }, 1000);
            return false;
        };
        // кластеризуем маркеры
        var markerCluster = new MarkerClusterer(map, markers, {
            imagePath: '/templates/dymohod/dist/img/map/clusters/m'
        });
        // Событие по клику на кластер
        // google.maps.event.addListener(markerCluster, 'clusterclick', function(cluster) {
        //     // your code here
        // });
        // console.log(results);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        ////////////////////////////////////////////////////автокомплит//////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var objectzz = results;
        markers = [];
        for (var i = 0; i < results.features.length; i++) {
            markers.push(results.features[i].properties);
            // console.log("Маркеры: " + results.features[i].properties);
        }


        // console.log(markers);
        $("#project").autocomplete({
            appendTo: ".map_searchblock_results",
            minLength: 0,
            source: markers,
            focus: function(event, ui) {
                // $("#project").val(ui.item.place_name);
                return false;
            },
            select: function(event, ui) {
                inputVal = $(this).val();
                $('.map_searchblock_backbutton').show();

                if (ui.item.place_name) {
                    $("#project").val(ui.item.place_name);
                    $("#project-id").val(ui.item.place_name);
                }
                if (ui.item.place_addr) {
                    $("#project-addr").html("<b>Адрес:</b> " + ui.item.place_addr);
                }
                if (ui.item.place_tel) {
                    var phone2 = '';
                    if (ui.item.place_tel2) {
                        phone2 = ", <a href='tel:" + ui.item.place_tel2 + "'>" + ui.item.place_tel2 + "</a>";
                    }
                    $("#project-phone").html("<b>Тел.:</b> <a href='tel:" + ui.item.place_tel + "'>" + ui.item.place_tel + "</a>" + phone2);
                }
                if (ui.item.place_worktime) {
                    $("#project-time").html("<b>Режим работы.:</b> " + ui.item.place_worktime);
                }
                if (ui.item.place_site) {
                    $("#project-site").html("<b>Сайт:</b> <a href='" + ui.item.place_site + "' target='_blank'>" + ui.item.place_site + "</a>");
                }
                if (ui.item.place_email) {
                    $("#project-email").html("<b>Email:</b> <a href='mailto:" + ui.item.place_email + "'>" + ui.item.place_email + "</a>");
                }
               if (ui.item.service) {
                    $("#project-email").html("<b>Оказывает услуги: </b>" + ui.item.service);
                }
                if (ui.item.manager_name) {
                    $("#project-manager").html("<b>Менеджер:</b>" + ui.item.manager_name);
                }
                if (ui.item.place_pic) {
                    $("#project-icon").attr("src", "img/map/pics/" + ui.item.place_pic);
                }
                // console.log(ui.item.number);
                // var placeLl = [];
                // placeLl.push('lat:' + ui.item.place_ll.lat);
                // placeLl.push('lng:' + ui.item.place_ll.lng);
                // console.log('Запуск фу инфоокна: ' + placeLl);
                // var tPlase = {
                //     lat: ui.item.place_ll.lat,
                //     lng: ui.item.place_ll.lng
                // };
                // console.log('Запуск фу инфоокна');
                dataCords(ui.item.number);
                window.setTimeout(function() {
                    $('.map_spinner').show();
                }, 1000);
                window.setTimeout(function() {
                    $('.map_spinner').hide();
                }, 2100);
                $('.map_searchblock_details').addClass('map_searchblock_details_show');
                return false;
            }
        }).autocomplete("instance")._renderItem = function(ul, item) {
            return $("<li>").append("<div>" + item.place_name + "<br>" + item.place_addr + "</div>").appendTo(ul);
        };
        $('.map_searchblock_backbutton').click(function(){
            $(this).hide();
        });
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        //////////////////////////////////////////////////конец автокомплита/////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    };
};
// addScript('https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js');
// addScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyD9iCxyR97TuK7IOxdKlx5lf6FsfuQm4MI');
if ($('#map').length) {
    // console.log('map found');
    initMap();
} else {
    // console.log('map not found');
}
$('#backbutton').on('click', function(e) {
	console.log(inputVal);
	$('#project').val(inputVal);
	$('#project').keydown();
	e.preventDefault();
});
console.log('buy_map.js');
});

function addScript(src) {
    var script = document.createElement('script');
    script.src = src;
    script.async = true; // чтобы гарантировать порядок
    document.head.appendChild(script);
    return
};