<?php
class content_custom extends def_module
{

	public $module;

	public function whereBuy($asJSON = false)
	{

		$domainsCollection = domainsCollection::getInstance();
		$regedit = regedit::getInstance();
		$domain_id = '1';
		$lang_id = cmsController::getInstance()->getCurrentLang()->getId();


		$siteUrl = $domainsCollection->getDefaultDomain()->getHost();
		$siteTitle = $regedit->getVal("//settings/site_name/{$domain_id}/{$lang_id}/") ?
					$regedit->getVal("//settings/site_name/{$domain_id}/{$lang_id}") : $regedit->getVal("//settings/site_name");;

		$pages = new selector('pages');
		$pages->types('object-type')->id(126);
		// $pages->limit(0, 5);

		$result = [];
		$result['type'] = 'FeatureCollection';
		$result['metadata'] = ['url'=> $siteUrl, 'title'=> $siteTitle];

		$features = [];
		$i = 0;
		foreach ($pages as $page) {
			$mapObjeect = [];
			$name = $page->getValue('h1');

			$cordinate = $page->getValue('koordinaty');
			$cordinate = json_decode($cordinate);

			$mapObjeect['type'] = $name;

			$mapObjeect['properties']['number'] = $i;
			$mapObjeect['properties']['value'] = $name . ', ' . htmlspecialchars($page->getValue('adres')) ;
			$mapObjeect['properties']['place_name'] = $name;
			$mapObjeect['properties']['place_addr'] = htmlspecialchars($page->getValue('adres'));
			$mapObjeect['properties']['place_tel'] = $page->getValue('telefon');
			$mapObjeect['properties']['place_worktime'] = $page->getValue('rezhim_raboty');
			$mapObjeect['properties']['place_site'] = $page->getValue('sajt');
			$mapObjeect['properties']['place_email'] = $page->getValue('email');
			$mapObjeect['properties']['place_roadmap'] = 'https://www.google.com/maps/dir//' . $cordinate->lat . ',' . $cordinate->lng . '/';
			$mapObjeect['properties']['place_callback'] = '';
			$mapObjeect['properties']['place_pic'] = '';
			$mapObjeect['properties']['place_ll']['lat'] = $cordinate->lat;
			$mapObjeect['properties']['place_ll']['lng'] = $cordinate->lng;
			$mapObjeect['properties']['manager_name'] = 'Sir John Backsword';
			$mapObjeect['properties']['manager_spec'] = 'main Sir';
			$mapObjeect['properties']['manager_phone'] = $page->getValue('telefon');
			$mapObjeect['properties']['manager_email'] = $page->getValue('email');
			
			$geometryType = '';
			if ($page->getValue('izbrannyj')) {
				$geometryType = 'important';
			}
			if ($page->getValue('oficialnyj_predstavitel')) {
				$geometryType = 'official';
			}

			$mapObjeect['geometry']['type'] = 'Point';
			$mapObjeect['geometry']['coordinates'] = [$cordinate->lat, $cordinate->lng, 111.16];
			$mapObjeect['id'] = $page->id;
			$i++;
			$features[] = $mapObjeect;
			
		}

		$result['features'] = $features;

		if ($asJSON) {
			return $result;
		}

		$this->module->printJson($result);
	}

	/**
	 * Выводит в буфер данные в json формате
	 * @param mixed $data данные
	 * @throws coreException
	 */
	public function printJson($data) {
			/** @var HTTPOutputBuffer $buffer */
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType("text/javascript");
			$buffer->option("generation-time", false);
			$buffer->charset('utf-8');
			if (getRequest('json-callback')) {
				$buffer->push(getRequest('json-callback') . '(');
			}
			$buffer->push(json_encode($data, JSON_NUMERIC_CHECK));
			if (getRequest('json-callback')) {
				$buffer->push(')');
			}
			$buffer->end();
		}

	public function dropContent($id)
	{
		// $hierarchy = umiHierarchy::getInstance();
		// $n = $hierarchy->getElement($id);
		// $n->setValue('content', '');
		// $n->commit;
		return true;
	}


	/**
	 * Выводит список элементов типа "Страница контента"
	 * @param string $template имя шаблона (для TPL-шаблонизатора)
	 * @param int|string $path ID элемента или его адрес
	 * @param int $maxDepth максимальная глубина вложенности иерархии поиска элементов (во вложенных подразделах)
	 * @param int $perPage количество элементов на странице (при постраничной навигации)
	 * @param bool $ignorePaging игнорировать постраничную навигацию
	 * @param string $sortField имя поля, по которому нужно произвести сортировку элементов
	 * @param string $sortDirection направление сортировки ('asc' или 'desc')
	 * @param int $typeId ID типа данных. По умолчанию все страницы.
	 * @return array
	 * @throws publicException
	 * @throws selectorException
	 */
	public function getList($template = 'default', $path = 0, $maxDepth = 1, $perPage = 0,
			$ignorePaging = false, $sortField = '', $sortDirection = 'asc', $typeId = 0, $whereField = 0, $whereVal = 0) {

		$elements = new selector('pages');
		if ($typeId == 0) {
			$elements->types('hierarchy-type')->name('content', 'page');
		} else {
			$elements->types('object-type')->id($typeId);
		}

		$parentId = $this->module->analyzeRequiredPath($path);
		
		if (!$parentId && $parentId !== 0 && $path !== KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		if ($path !== KEYWORD_GRAB_ALL) {
			$maxDepthNum = intval($maxDepth) > 0 ? intval($maxDepth) : 1;
			$elements->where('hierarchy')->page($parentId)->childs($maxDepthNum);
		}

		$perPageNumber = intval($perPage);
		$limit = $perPageNumber > 0 ? $perPageNumber : $this->module->perPage;

		if (!$ignorePaging) {
			$currentPage = intval(getRequest('p'));
			$offset = $currentPage * $limit;
			$elements->limit($offset, $limit);
		}

		if ($sortField) {
			$direction = 'asc';
			if (in_array($sortDirection, ['asc', 'desc', 'rand'])) {
				$direction = $sortDirection;
			}

			try {
				$elements->order($sortField)->$direction();
			} catch (selectorException $e) {
				throw new publicException(getLabel('error-prop-not-found', null, $sortField));
			}
		}

        if($whereField){
            $elements->where($whereField)->isnotnull($whereVal);
        }

		selectorHelper::detectFilters($elements);

		$elements->option('load-all-props')->value(true);
		$elements->option('exclude-nested', false);

		$result = $elements->result();

		list($templateBlock, $templateBlockEmpty, $templateItem) =
				content::loadTemplates('content/' . $template, 'get_list_block', 'get_list_block_empty', 'get_list_item');

		$total = $elements->length();

		$data = [
				'items' => [
						'nodes:item' => null
				],
				'total' => $total,
				'per_page' => $limit,
				'parent_id' => $parentId
		];

		if ($total === 0) {
			return content::parseTemplate($templateBlockEmpty, $data, $parentId);
		}

		$linksHelper = umiLinksHelper::getInstance();
		$umiHierarchy = umiHierarchy::getInstance();

		$items = [];
		/** @var iUmiHierarchyElement|iUmiEntinty $page */
		foreach ($result as $page) {
			if (!$page instanceof iUmiHierarchyElement) {
				continue;
			}

			$itemData = [];

			$itemData['@id'] = $page->getId();
			$itemData['name'] = $page->getName();
			$itemData['@link'] = $linksHelper->getLinkByParts($page);
			$itemData['@xlink:href'] = 'upage://' . $page->getId();
			$itemData['@visible_in_menu'] = $page->getIsVisible();
			$items[] = content::parseTemplate($templateItem, $itemData, $page->getId());
			$umiHierarchy->unloadElement($page->getId());
		}

		$data['items']['nodes:item'] = content::parseTemplate($templateBlock, $items);
		return content::parseTemplate($templateBlock, $data, $parentId);
	}

    public function customIncludeQuickEditJs()
    {
        $config      = mainConfiguration::getInstance();
        $build       = regedit::getInstance()->getVal("modules/autoupdate/system_build");
        $permissions = permissionsCollection::getInstance();
        $userId      = $permissions->getUserId();

        $currentDomain        = cmsController::getInstance()->getCurrentDomain();
        $isAllowedMethod      = $permissions->isAllowedMethod($userId, "content", "sitetree");
        $isAllowedDomain      = $permissions->isAllowedDomain($userId, $currentDomain->getId());
        $isAllowedEditInPlace = $isAllowedMethod && $isAllowedDomain;

        if ($isAllowedEditInPlace) {
            $cmsController   = cmsController::getInstance();
            $langsCollection = langsCollection::getInstance();
            $langPrefix      = '';

            if ($cmsController->getLang()->getId() != $langsCollection->getDefaultLang()->getId()) {
                $langPrefix = $cmsController->getLang()->getPrefix();
            }

            $session         = session::getInstance();
            $sessionLifetime = $session->getMaxActiveTime();
            $csrfToken       = $session->get('csrf_token');
            $session->commit();

            $sessionAccess = $permissions->isAllowedModule($userId, 'config') ? 'true' : 'false';

            $eipTheme = $config->get("edit-in-place", "theme");

            if (strlen($eipTheme)) {
                $eipTheme = "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . substr($eipTheme, 1) . "?{$build}\" />";
            }

            $eipWYSIWYG = $config->get("edit-in-place", "wysiwyg");

            if (!strlen($eipWYSIWYG)) {
                $eipWYSIWYG = 'tinymce4';
            }

            // инициализация служебной информации о странице для frontend
            $pageId   = $cmsController->getCurrentElementId();
            $pageData = json_encode([
                'pageId'    => $pageId,
                'page'      => [
                    'alt-name' => ($pageId) ? umiHierarchy::getInstance()->getElement($pageId)->getAltName() : '',
                ],
                'title'     => def_module::parseTPLMacroses(macros_title()),
                'lang'      => $cmsController->getCurrentLang()->getPrefix(),
                'lang_id'   => $cmsController->getCurrentLang()->getId(),
                'domain'    => $cmsController->getCurrentDomain()->getHost(),
                'domain_id' => $cmsController->getCurrentDomain()->getId(),
                'meta'      => [
                    'keywords'    => macros_keywords(),
                    'description' => macros_describtion(),
                ],
            ]);
            $HTML = <<<HTML

				<link type="text/css" rel="stylesheet" href="/js/cms/compiled.css?{$build}" />
				{$eipTheme}

				<script type="text/javascript" charset="utf-8" src="/ulang/common.js?{$build}"></script>
				<script type="text/javascript" src="/js/cms/jquery.compiled.js?{$build}" charset="utf-8"></script>
				<script type="text/javascript" src="/js/cms/compiled.js?{$build}" charset="utf-8"></script>

				<script type="text/javascript">
					// вывод служебной информации о странице для frontend
					window.pageData = {$pageData};

					uAdmin({
						'lang_prefix': '{$langPrefix}',
						'csrf': '{$csrfToken}'
					});
					uAdmin({
						'lifetime' : {$sessionLifetime},
						'access'   : {$sessionAccess}
					}, 'session');
					uAdmin('type', '{$eipWYSIWYG}', 'wysiwyg');
				</script>

HTML;
            return $HTML;
        }

    }

    public function convertDate($_iTimeStamp, $_sFormat, $flag = false)
    {

        $aMonthLong = array(
            'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',
        );

        $aMonthShort = array(
            "янв",
            "фев",
            "мар",
            "апр",
            "май",
            "июнь",
            "июль",
            "авг",
            "сен",
            "окт",
            "ноя",
            "дек",
        );

        if ($_iTimeStamp == -1) {
            $_iTimeStamp = time();
        }

        $iFormatLength = strlen($_sFormat);
        $sDate         = "";
        $aaDate        = array();
        for ($i = 0; $i < $iFormatLength; $i++) {
            switch ($_sFormat[$i]) {
                case 'F':{
                        $sDate .= $aMonthLong[intval(date("n", $_iTimeStamp))];
                        $aaDate[$_sFormat[$i]] = $aMonthLong[intval(date("n", $_iTimeStamp))];
                        break;
                    }
                case 'M':{
                        $sDate .= $aMonthShort[intval(date("n", $_iTimeStamp)) - 1];
                        $aaDate[$_sFormat[$i]] = $aMonthShort[intval(date("n", $_iTimeStamp)) - 1];
                        break;
                    }
                default:{
                        $sDate .= date($_sFormat[$i], $_iTimeStamp);
                        if (date($_sFormat[$i], $_iTimeStamp) !== ' ') {
                            $aaDate[$_sFormat[$i]] = date($_sFormat[$i], $_iTimeStamp);
                        }
                    }
            }
        }
        if ($flag) {
            return $aaDate;
        }
        return $sDate;
    }


    public function isAjaxRequest()
    {
        $res = 'no';
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $res = 'yes';
        }
        return $res;
    }

    public function getTags($id = 0, $template = 'default', $dept = 1)
    {
        $array_res = array();

        list($items_template, $item_template) = def_module::loadTemplates('/content/' . $template, 'items', 'item');


        $select = new selector('pages');
        $select->where('hierarchy')->page($id)->childs($dept);

        $tags = array();

        foreach ($select as $key => $value) {
            $hierarchy = umiHierarchy::getInstance();
            $page      = $hierarchy->getElement($value->id);
            $t         = $page->getValue('tags');

            if (count($t) != 0 && $t != null) {
                $tags = array_merge($tags, $t);
            }
        }
        $tags = array_unique($tags);

        $fields_filter = getRequest('fields_filter');
        $filterArr = array();
        if (isset($fields_filter['tags']['eq'])) {
        	$filterArr = $fields_filter['tags']['eq'];
        }
        $items = array();
        foreach ($tags as $tag) {
        	$item = array();
			$item['attribute:value'] = $tag;
            
            if (in_array($tag, $filterArr)) {
				$item['attribute:is_selected'] = 'selected';
            }

            $item['node:text'] = $tag;

            $items[]                 = def_module::parseTemplate($item_template, $item);
        }
        $blockItems                   = array();
        $blockItems['subnodes:items'] = $items;

        return def_module::parseTemplate($items_template, $blockItems);
    }




}
