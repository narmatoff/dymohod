<?php
class catalog_custom extends def_module
{

	public function getCommercePDF($element_id = false)
	{
		if (!$element_id) {
			return false;
		}


		$hierarchy = umiHierarchy::getInstance();
        $page      = $hierarchy->getElement($element_id);

        if ($page instanceof iUmiHierarchyElement) {
			include(CURRENT_WORKING_DIR . "/mpdf60/mpdf.php");

			$product['id'] = $element_id;

			list($template) = def_module::loadTemplatesForMail('catalog/mail/pdf', 'pfd_to_dlr');
			$content = def_module::parseTemplateForMail($template, $product);
		
		// var_dump($content);die();

			// print_r($content);die();
			$mpdf = new mPDF('UTF-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10);
			$mpdf->WriteHTML($content);
			$mpdf->Output($element_id.'.pdf', 'D');
				// return $element_id;
        }

        return false;
	}

    /**
     * Выводит данные для формирования списка объектов каталога, с учетом параметров фильтрации
     * @param string $template имя шаблона отображения (только для tpl)
     * @param int $categoryId ид раздела каталога, объекты которого требуется вывести
     * @param int $limit ограничение количества выводимых объектов каталога
     * @param bool $ignorePaging игнорировать постраничную навигацию (то есть GET параметр 'p')
     * @param int $level уровень вложенности раздела каталога $categoryId, на котором размещены необходимые объекты каталога
     * @param bool $fieldName поле объекта каталога, по которому необходимо произвести сортировку
     * @param bool $isAsc порядок сортировки
     * @return mixed
     * @throws publicException если не удалось получить объект страницы по id = $categoryId
     */
    public function getSmartCatalog($template = 'default', $categoryId, $limit, $ignorePaging = false, $level = 1, $fieldName = false, $isAsc = true, $headProduct = false)
    {
        /**
         * @var CatalogMacros|catalog $module
         */
        $module = $this->module;

        if (!is_string($template)) {
            $template = 'default';
        }

        list(
            $itemsTemplate,
            $emptyItemsTemplate,
            $emptySearchTemplates,
            $itemTemplate
        ) = def_module::loadTemplates(
            'catalog/' . $template,
            'objects_block',
            'objects_block_empty',
            'objects_block_search_empty',
            'objects_block_line'
        );

        // if (!in_array($categoryId, [413,414])) {
        //     if ($headProduct) {
        //         cmsController::getInstance()->getModule("data")->setFilter($categoryId);
        //      $categoryId = 413;

        //     }
        // }
        $umiHierarchy = umiHierarchy::getInstance();
        /* @var iUmiHierarchyElement $category */
        $category = $umiHierarchy->getElement($categoryId);

        if (!$category instanceof iUmiHierarchyElement) {
            throw new publicException(__METHOD__ . ': cant get page by id = ' . $categoryId);
        }

        $limit       = ($limit) ? $limit : $this->module->per_page;
        $currentPage = ($ignorePaging) ? 0 : (int) getRequest('p');
        $offset      = $currentPage * $limit;

        if (!is_numeric($level)) {
            $level = 1;
        }

        $filteredProductsIds = null;
        $queriesMaker        = null;
        if (is_array(getRequest('filter'))) {
            $emptyItemsTemplate = $emptySearchTemplates;
            $queriesMaker       = $module->getCatalogQueriesMaker($category, $level);

            if (!$queriesMaker instanceof FilterQueriesMaker) {
                return $module->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
            }

            $filteredProductsIds = $queriesMaker->getFilteredEntitiesIds();

            if (count($filteredProductsIds) == 0) {
                return $module->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
            }
        }
        $products = new selector('pages');
        // $products->types('hierarchy-type')->id(85);
        if ($headProduct) {
            $products->types('object-type')->id('128');
        } else {
            $products->types('hierarchy-type')->name('catalog', 'object');
        }

        if (is_null($filteredProductsIds)) {
            $products->where('hierarchy')->page($categoryId)->childs($level);
        } else {
            $products->where('id')->equals($filteredProductsIds);
        }

        if ($fieldName) {
            if ($isAsc) {
                $products->order($fieldName)->asc();
            } else {
                $products->order($fieldName)->desc();
            }
        } else {
            $products->order('ord')->asc();
        }

        if ($queriesMaker instanceof FilterQueriesMaker) {
            if (!$queriesMaker->isPermissionsIgnored()) {
                $products->option('no-permissions')->value(true);
            }
        }
        // var_dump($products->query());

        $products->option('load-all-props')->value(true);
        $products->limit($offset, $limit);
        $pages = $products->result();
        $total = $products->length();

        if ($total == 0) {
            return $module->makeEmptyCatalogResponse($emptyItemsTemplate, $categoryId);
        }

        $result         = array();
        $items          = array();
        $umiLinksHelper = umiLinksHelper::getInstance();
        /* @var iUmiHierarchyElement|umiEntinty $page */
        foreach ($pages as $page) {
            $item                       = array();
            $pageId                     = $page->getId();
            $item['attribute:id']       = $pageId;
            $item['attribute:alt_name'] = $page->getAltName();
            $item['attribute:price']    = $page->getValue('price');
            $item['attribute:link']     = $umiLinksHelper->getLinkByParts($page);
            $item['xlink:href']         = 'upage://' . $pageId;
            $item['node:text']          = $page->getName();
            $items[]                    = catalog::parseTemplate($itemTemplate, $item, $pageId);
            catalog::pushEditable('catalog', 'object', $pageId);
            $umiHierarchy->unloadElement($pageId);
        }

        $result['subnodes:lines'] = $items;
        $result['numpages']       = umiPagenum::generateNumPage($total, $limit);
        $result['total']          = $total;
        $result['per_page']       = $limit;
        $result['category_id']    = $categoryId;

        return catalog::parseTemplate($itemsTemplate, $result, $categoryId);
    }

    public function getObjectParam($pageId = '', $fieldName = '')
    {
        if (!$pageId) {
            throw new publicException(__METHOD__ . ': cant get id');
        }

        if (!$fieldName) {
            throw new publicException(__METHOD__ . ': cant get fieldName');
        }

        $umiHierarchy = umiHierarchy::getInstance();
        $objectTypes  = umiObjectTypesCollection::getInstance();
		$fieldsCollection = umiFieldsCollection::getInstance();

        /* @var iUmiHierarchyElement $product */
        $product = $umiHierarchy->getElement($pageId);

        if (!$product instanceof iUmiHierarchyElement) {
            throw new publicException(__METHOD__ . ': cant get page by id = ' . $product);
        }

        $template = 'default';

        list(
            $itemsTemplate,
            $emptyItemsTemplate,
            $emptySearchTemplates,
            $itemTemplate
        ) = def_module::loadTemplates(
            'catalog/' . $template,
            'objects_block',
            'objects_block_empty',
            'objects_block_search_empty',
            'objects_block_line'
        );

        $rowLineSrc = $product->getValue($fieldName);
        if (!$rowLineSrc) {
        	return;
        }
        $rowLine = array();

        $mainFieldId = $product->getFieldId($fieldName);
        $mainField   = $fieldsCollection->getField($mainFieldId);

        foreach ($rowLineSrc as $row) {
            $rowLine[$row][$fieldName]['label'] = $mainField->getTitle();
            $rowLine[$row][$fieldName]['val'] = $this->getObjectName(intval($row));
        }
        asort($rowLine);

        $childrenTree = $umiHierarchy->getChildrenTree($pageId, false);

        $result = array();

        $objectTypeId = $umiHierarchy->getDominantTypeId($pageId);
        $objectType   = $objectTypes->getType($objectTypeId);
        $fieldsGroup  = $objectType->getFieldsGroupByName('additional');
        $fieldGroup   = $fieldsGroup->getFields();

        $fieldArr = array();



        foreach ($childrenTree as $childrenProductId => $bool) {
            $children = $umiHierarchy->getElement($childrenProductId);



            foreach ($fieldGroup as $field) {
                $fname = $children->getValue($fieldName);
                $fval  = $children->getValue($field->getName());

                if ($fval and ($field->getName()!=='table_'.$fieldName)) {
                	
                    $rowLine[intval($fname[0])][$field->getName()]['label'] = $field->getTitle();
                    $rowLine[intval($fname[0])][$field->getName()]['val'] = $fval;
                }
            }

            $umiHierarchy->unloadElement($childrenProductId);
        }

        $finalArray = [];
        foreach ($rowLine as $row) {
        	foreach ($row as $rowName => $rowVal) {
        		$finalArray[$rowName]['name'] = $rowVal['label'];
        		$finalArray[$rowName]['val'][] = $rowVal['val'];
        	}
        }

        $result = array();
        $items  = array();
        foreach ($finalArray as $fKey => $fVal) {
            $item                 = array();
            $item['attribute:label'] = $fKey;
            $item['attribute:name'] = $fVal['name'];
            $subitems = [];
            foreach ($fVal['val'] as $lastVal) {
            	$lItem = array();
            	$lItem['attribute:value'] = $lastVal;
            	$subitems[] = catalog::parseTemplate($itemTemplate, $lItem, $pageId);
            }
        	$item['subnodes:items'] = $subitems;

            $items[] = catalog::parseTemplate($itemTemplate, $item, $pageId);

        }
        $result['subnodes:lines'] = $items;
        return catalog::parseTemplate($itemsTemplate, $result, $pageId);
    }

    /**
     * получение названий объектов
     * @date   2017-07-05
     * @author Lum - Kornilov
     * @param  int     $objectId id объекта
     * @return string               название объекта
     */
    public function getObjectName($objectId = '')
    {
        if (!$objectId) {
            throw new publicException(__METHOD__ . ': cant get objectId');
        }

        $objects = umiObjectsCollection::getInstance();

        $object = $objects->getObject($objectId);

        if (!$object instanceof umiObject) {
            throw new publicException(__METHOD__ . ': cant get object by id = ' . $objectId);
        }

        $objectName = $object->getName();
        return $objectName;
    }
}
