<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на просмотр каталога
		 */
		'view' => [
			'category',
			'object',
			'viewobject',
			'getcategorylist',
			'getsmartfilters',
			'makeemptyfilterresponse',
			'getsmartcatalog',
			'makeemptycatalogresponse',
			'parseCommonValue',
			'getSmartFiltersCustom',
			'getDataCalculation',
			'getObjectParam',
		]
	];
?>