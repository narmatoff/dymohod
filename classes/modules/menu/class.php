<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexey
 * Date: 18.04.2017
 * Time: 12:26
 */
class menu_custom extends def_module
{

    public $module;

    /**
     * Возвращает данные меню
     * @param null|int $menuId идентификатор меню
     * @param string $template имя шаблона (для tpl)
     * @return mixed
     * @throws publicException
     * @throws selectorException
     */
    public function draw($menuId = null, $template = "default") {
        if ($menuId === false) {
            throw new publicException(getLabel('error-object-does-not-exist', null, $menuId));
        }

        $menu = null;
        $umiObjectsCollection = umiObjectsCollection::getInstance();

        if (!is_numeric($menuId)) {
            $selector = new selector('objects');
            $selector->types('object-type')->name('menu', 'item_element');
            $selector->where("menu_id")->equals($menuId);
            $selector->option('no-length')->value(true);
            $selector->limit(0, 1);

            if (!$selector->first) {
                throw new publicException(getLabel('error-object-does-not-exist', null, $menuId));
            }

            $menu = $selector->first;
        } else {
            $menu = $umiObjectsCollection->getObject($menuId);
        }

        if (!$menu instanceof umiObject) {
            throw new publicException(getLabel('error-object-does-not-exist', null, $menuId));
        }

        if (!$template) {
            $template = "default";
        }

        $templates = menu::loadTemplates("menu/" . $template);
        $menuHierarchy = $menu->getValue('menuhierarchy');

        if (!$menuHierarchy) {
            throw new publicException(getLabel('error-prop-not-found', null, 'menuHierarchy'));
        }

        $menuHierarchyArray = json_decode($menuHierarchy);
        $currentElementId = cmsController::getInstance()->getCurrentElementId();
        $allParents = umiHierarchy::getInstance()->getAllParents($currentElementId, true);
        $this->module->loadMenuElements((is_array($menuHierarchyArray)) ? $menuHierarchyArray : (array) $menuHierarchyArray);

        $menuResult = $this->module->drawLevel($menuHierarchyArray, $templates, 0, $allParents);

        $menuResult['name']['attribute:id'] = $menu->getId();
        $menuResult['name']['attribute:name'] = $menu->getName();


        return $menuResult;

    }
}