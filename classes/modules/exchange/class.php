<?php

class exchange_custom extends def_module
{

    public $module;

    public function removeProductTree($e)
    {
        if ($e->getMode() == "after") {
    		cmsController::getInstance()->setCurrentMode('admin');
            $pages = new selector('pages');
            $pages->types('hierarchy-type')->name('catalog', 'object');
            $pages->where('hierarchy')->page(0)->childs(1);
            $pages->where('1c_product_id')->like('%#%');

            $total = $pages->length;
            $hierarchy = umiHierarchy::getInstance();

            if ($total > 0) {
                foreach ($pages as $page) {
                    $product1c       = $page->getValue('1c_product_id');
                    $parentProduct1c = substr($product1c, 0, strpos($product1c, '#'));

                    $products = new selector('pages');
                    $products->types('hierarchy-type')->name('catalog', 'object');
                    $products->where('1c_product_id')->equals($parentProduct1c);

                    $parentId = 0;

                    foreach ($products as $product) {
                        $parentId = $product->id;
                    }

                    if ($products->length() > 0) {
                        $page->setRel($parentId);
                        $page->commit();
                        $hierarchy->rebuildRelationNodes($parentId);
                    }

                    $hierarchy->unloadElement($parentId);
                    $hierarchy->unloadElement($page->id);
                }
            }
        }
    }

}
