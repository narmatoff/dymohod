<?php
class filemanager_custom extends def_module
{

    public $module;

    /**
		 * Возвращает информацию о странице со скачиваемым файлом
		 * @param string $template имя шаблона (для tpl шаблонизатора)
		 * @param bool|int|string $element_path идентификатор или адрес страницы
		 * @return mixed
		 */
		public function shared_file($template = "default", $element_path = false) {
			if (!$template) {
				$template = "default";
			}
			list(
				$s_download_file,
				$s_broken_file,
				$s_upload_file
				) = filemanager::loadTemplates(
				"filemanager/".$template,
				"shared_file",
				"broken_file",
				"upload_file"
			);

			$element_id = $this->module->analyzeRequiredPath($element_path);
			$umiHierarchy = umiHierarchy::getInstance();
			$element = $umiHierarchy->getElement($element_id);

			$block_arr = Array();
			$template_block = $s_broken_file;
			if ($element) {
				$permissionsCollection = permissionsCollection::getInstance();
				$iUserId = $permissionsCollection->getUserId();
				list($bAllowRead, $bAllowWrite) = $permissionsCollection->isAllowedObject($iUserId, $element_id);
				$block_arr['upload_file'] = "";
				if ($bAllowWrite) {
					$block_arr['upload_file'] = $s_upload_file;

					if (count($_FILES)) {
						$oUploadedFile = umiFile::upload("shared_files", "upload", "./files/");
						if ($oUploadedFile instanceof umiFile) {
							$element->setValue("fs_file", $oUploadedFile);
							$element->commit();
						}
					}
				}

				$block_arr['id'] = $element_id;
				$block_arr['descr'] = ($descr = $element->getValue("descr")) ? $descr : $element->getValue("content");
				$block_arr['h1'] = $element->getValue("h1");
				$block_arr['versiya_fajla'] = $element->getValue("versiya_fajla");
				$block_arr['menu_pic_a'] = $element->getValue("menu_pic_a");
				$block_arr['alt_name'] = $element->getAltName();
				$block_arr['link'] = $umiHierarchy->getPathById($element_id);
				$block_arr['download_link'] = "";
				$block_arr['ext'] = "";
				$block_arr['file_name'] = "";
				$block_arr['file_size'] = 0;

				$o_file = $element->getValue("fs_file");

				if ($o_file instanceof umiFile) {
					if (!$o_file->getIsBroken()) {
						$template_block = $s_download_file;
						$block_arr['download_link'] = $this->module->pre_lang."/filemanager/download/".$element_id;
						$block_arr['file_name'] = $o_file->getFileName();
						$block_arr['ext'] = $o_file->getExt();
						$block_arr['file_size'] = round($o_file->getSize()/1024, 2);
					}
				}
			} else {
				/**
				 * @var users $userModule
				 */
				$cmsController = cmsController::getInstance();
				$userModule = $cmsController->getModule("users");
				return $userModule->auth();
			}

			filemanager::pushEditable("filemanager", "shared_file", $element_id);
			return filemanager::parseTemplate($template_block, $block_arr);
		}
}
?>