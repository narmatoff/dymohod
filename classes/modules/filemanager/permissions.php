<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на просмотр файлов для скачивания
		 */
		'list_files' => [
			'shared_file',
		],
	];
?>