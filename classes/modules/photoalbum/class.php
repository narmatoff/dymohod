<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexey
 * Date: 18.04.2017
 * Time: 12:26
 */
class photoalbum_custom extends def_module
{

    public $module;

/**
 * Возвращает фотографии заданного фотоальбома
 * @param bool|int|string $path идентификатор или адрес фотоальбома
 * @param string $template имя шаблона (для tpl)
 * @param bool|int $limit ограничение на количество выводимых элементов
 * @param bool $ignore_paging игнорировать пагинацию
 * @param string $fname field name
 * @param string $fval field val
 * @param int $typeId typeId
 * @return mixed
 * @throws publicException
 * @throws selectorException
 */
    public function album($path = false, $template = "default", $limit = false, $ignore_paging = false, $fname = false, $fval = false, $typeId = false,$fieldName = false, $ord = false)
    {
        $curr_page = (int) getRequest('p');
        $per_page  = ($limit) ? $limit : $this->module->per_page;

        if ($ignore_paging) {
            $curr_page = 0;
        }

        $element_id = $this->module->analyzeRequiredPath($path);

        if ($element_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }

        list($template_block, $template_block_empty, $template_line) = photoalbum::loadTemplates(
            "photoalbum/" . $template,
            "album_block",
            "album_block_empty",
            "album_block_line"
        );

        $photos = new selector('pages');
        $photos->types('hierarchy-type')->name('photoalbum', 'photo');

        if ($typeId) {
	        $photos->types('object-type')->id($typeId);
        }

        if ($path != KEYWORD_GRAB_ALL) {
            $photos->where('hierarchy')->page($element_id);
        }

        if ($fname and $fval) {
        	$photos->where($fname)->isnotnull($fval);
        }
        if ($fieldName) {
			if (in_array($ord, array('asc', 'desc', 'rand'))) {
				$photos->order($fieldName)->$ord();
			}
		}

        $photos->option('load-all-props')->value(true);
        $photos->limit($curr_page * $per_page, $per_page);
        $result = $photos->result();
        $total  = $photos->length();
        $block_arr       = array();
        $block_arr['id'] = $block_arr['void:album_id'] = $element_id;

        if ($total == 0) {
            return photoalbum::parseTemplate($template_block_empty, $block_arr, $element_id);
        }

        $lines          = array();
        $umiLinksHelper = umiLinksHelper::getInstance();

        foreach ($result as $photo) {
            $line_arr = array();

            if (!$photo instanceof umiHierarchyElement) {
                continue;
            }

            $photo_element_id           = $photo->getId();
            $line_arr['attribute:id']   = $photo_element_id;
            $line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($photo);
            $line_arr['xlink:href']     = "upage://" . $photo_element_id;
            $line_arr['node:name']      = $photo->getName();

            photoalbum::pushEditable("photoalbum", "photo", $photo_element_id);
            $lines[] = photoalbum::parseTemplate($template_line, $line_arr, $photo_element_id);
        }

        $block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
        $block_arr['total']          = $total;
        $block_arr['per_page']       = $per_page;
        $block_arr['link']           = umiHierarchy::getInstance()->getPathById($element_id);
        return photoalbum::parseTemplate($template_block, $block_arr, $element_id);
    }

    /**
		 * Возвращает список фотоальбомов
		 * @param string $template имя шаблона (для tpl)
		 * @param bool|int $limit ограничение на количество выводимых элементов
		 * @param bool $ignore_paging игнорировать пагинацию
		 * @param bool|int $parentElementId идентификатор родителькой страницы,
		 * среди дочерних страниц которой необходимо искать фотоальбомы
		 * @param string $order тип сортировки
		 * @return mixed
		 * @throws selectorException
		 */
		public function albums($template = "default", $limit = false, $ignore_paging = false, $parentElementId = false, $order = 'asc', $fieldName = false, $ord = 'asc') {
			list($template_block, $template_block_empty, $template_line) = photoalbum::loadTemplates(
				"photoalbum/" . $template,
				"albums_list_block",
				"albums_list_block_empty",
				"albums_list_block_line"
			);

			$block_arr = Array();
			$curr_page = (int) getRequest('p');

			if ($ignore_paging) {
				$curr_page = 0;
			}

			$offset = $limit * $curr_page;

			$sel = new selector('pages');
			$sel->types('object-type')->name('photoalbum', 'album');

			$parentElementId = $this->module->analyzeRequiredPath($parentElementId);
			$parentElementId = (int) $parentElementId;

			if ($parentElementId) {
				$sel->where('hierarchy')->page($parentElementId);
			}

			if (in_array($order, array('asc', 'desc', 'rand'))) {
				$sel->order('ord')->$order();
			}

			if ($fieldName) {
				if (in_array($ord, array('asc', 'desc', 'rand'))) {
					$sel->order($fieldName)->$ord();
				}
			}

			$sel->option('load-all-props')->value(true);
			$sel->limit($offset, $limit);
			$result = $sel->result();
			$total = $sel->length();

			$lines = Array();

			if ($total == 0) {
				return photoalbum::parseTemplate($template_block_empty, $block_arr);
			}

			$umiLinksHelper = umiLinksHelper::getInstance();

			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $element
			 */
			foreach ($result as $element) {
				$line_arr = Array();

				if (!$element instanceof umiHierarchyElement) {
					continue;
				}

				$element_id = $element->getId();
				$line_arr['attribute:id'] = $element_id;
				$line_arr['attribute:link'] = $umiLinksHelper->getLinkByParts($element);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['node:name'] = $element->getName();

				photoalbum::pushEditable("photoalbum", "album", $element_id);
				$lines[] = photoalbum::parseTemplate($template_line, $line_arr, $element_id);
			}

			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $limit;
			return photoalbum::parseTemplate($template_block, $block_arr);
		}

}
