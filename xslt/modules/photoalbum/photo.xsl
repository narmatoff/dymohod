<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

<xsl:template match="result[@module = 'photoalbum'][@method = 'photo'][page/@type-id=&videoTypeId;]">
<div class="full_width full_width__light">
    <div class="main_width main_width__white">
        <section class="section_centered">
        	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
            <h1 class="middle_heading"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
             <article>
            	<xsl:value-of select=".//property[@name='descr']/value"  disable-output-escaping="yes"/>
            </article>
        </section>

        <section class="video_twocolumns">
        	<xsl:apply-templates select="page" mode="video-item"/>
        </section>
    </div>
</div>
</xsl:template>

<xsl:template match="result[@module = 'photoalbum'][@method = 'photo'][page/@type-id=&sertTypeId;]">
<div class="full_width full_width__light">
    <div class="main_width main_width__white">
        <section class="section_centered">
        	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
            <h1 class="middle_heading"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
        </section>
    </div>
</div>
 <div class="full_width full_width__light">
    <div class="main_width main_width__white">
        <section class="section_twocolumns">
            <article>
            	<xsl:value-of select=".//property[@name='descr']/value"  disable-output-escaping="yes"/>
            </article>
        	<article>
        		<div class="view_object">
					<a href="{.//property[@name='photo']/value}" class="view_object_image view_object_image_cert bigpic">
						<xsl:call-template name="makeThumbnail">
		                    <xsl:with-param name="alt" select="name"/>
		                    <xsl:with-param name="pwd" select=".//property[@name='photo']/value/@path"/>
		                    <xsl:with-param name="element_id" select="@id"/>
		                    <xsl:with-param name="width" select="'250'"/>
		                    <xsl:with-param name="height" select="'auto'"/>
		                </xsl:call-template>
					</a>
                </div>
            </article>
    	</section>
    </div>
</div>

</xsl:template>



</xsl:stylesheet>