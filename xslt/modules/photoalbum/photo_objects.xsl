<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <!-- страница объектов -->
    <xsl:template match="result[@module = 'photoalbum'][@method = 'album'][page/@type-id=&photoObjectTypeId;]">
        <script>
            document.addEventListener("DOMContentLoaded", function(event) { 
                var $objects = $('.objects_list_items').isotope({
                    // options
                    itemSelector: '.objects_list_item',
                    // layoutMode: 'fitColumns',
                    percentPosition: true,
                    masonry: {
                        columnWidth: '.grid-sizer',
                        gutter: '.gutter-sizer',
                    }
                });

                $('.object_tag').click(function() {
                    $('.object_tag').removeClass('object_tag__selected');
                    $(this).addClass('object_tag__selected');
                    console.debug($(this).data('filter'));
                    switch ($(this).data('filter')) {
                        <xsl:apply-templates select="document(concat('udata://data/getGuideItems//', &threeMainTypeId;))/udata/items/item" mode="tagger" />

                        case '.show_all':
                            $objects.isotope({ filter: '*' });
                            break;
                    }
                });
				
				if(window.location.hash !== ''){
					var hash = window.location.hash.substr(1);
					console.debug(hash);
                    if($('.'+hash)){
						$('.'+hash).trigger('click');
						window.location.hash = '';
					}
					else{
                		$('.show_all').trigger('click');
					}
				}
				else{
	                $('.show_all').trigger('click');
				}

            });
        </script>
<!--         <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <div class="pre_h"><xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h" /></div>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                    <xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes" />
                </section>
            </div>
        </div> -->
        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <div class="objects_tags filter">
                    <xsl:apply-templates select="document(concat('udata://data/getGuideItems//', &threeMainTypeId;))/udata/items/item" mode="tipy_rabot" />
                    <!--                     <a class="object_tag" href="javascript://" data-filter=".category-a"><img src="img/mansion.png" alt="" /><span>
                            Для частных<br/> клиентов</span></a>
                    <a class="object_tag" href="javascript://" data-filter=".category-b"><img src="img/prom.png" alt="" /><span>
                            Для промышленных<br/> объектов</span></a>
                    <a class="object_tag" href="javascript://" data-filter=".category-c"><img src="img/humans_builds.png" alt="" /><span>
                            Поквартирное<br/> отопление</span></a>

 -->
                     <a href="javascript://" class="show_all" style="display:none;"></a>
                </div>
                <section>


                    <div class="pre_h"><xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h" /></div>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                    <xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes" />


                    <div class="objects_list_items">

                        <xsl:apply-templates select="document(concat('udata://photoalbum/album/',/result/page/@id,'//500/1?extProps=h1,photo,descr,tip_obekta,razmer_bloka'))/udata/items/item" mode="portf_strup" />
                        
                        <div class="grid-sizer"></div>
                        <div class="gutter-sizer"></div>
                    </div>
                </section>
            </div>
        </div>


        </xsl:template>


    <xsl:template match="item" mode="tagger">
        <xsl:text/>
            case '.filter-<xsl:value-of select="./@id"/>':
            $objects.isotope({ filter: '.filter-<xsl:value-of select="./@id"/>' });
            break;
        <xsl:text/>
    </xsl:template>



    <xsl:template match="item" mode="tipy_rabot">

        <a class="object_tag {@id}" href="javascript://" data-filter=".filter-{./@id}">
            <img src="{$template-resources}dist/img/mansion{position()}.png" alt="" />
            <span><xsl:value-of select="."/></span>
        </a>

    </xsl:template>



    <xsl:template match="item" mode="portf_strup">
        <a class="objects_list_item mix filter-{.//property[@name='tip_obekta']/value/item/@id}" href="{.//property[@name='photo']/value}">
            

            <xsl:choose>
                <xsl:when test=".//property[@name='razmer_bloka']/value/item/@id = 4341">
                    <xsl:attribute name="class">objects_list_item mix objects_list_item__2height <xsl:apply-templates select=".//property[@name='tip_obekta']/value/item" mode="port_attributeset" /></xsl:attribute>
                </xsl:when>
                <xsl:when test=".//property[@name='razmer_bloka']/value/item/@id = 4340">
                    <xsl:attribute name="class">objects_list_item mix objects_list_item__2width <xsl:apply-templates select=".//property[@name='tip_obekta']/value/item" mode="port_attributeset" /></xsl:attribute>

                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">
                        <xsl:apply-templates select=".//property[@name='tip_obekta']/value/item" mode="port_attributeset" />
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <div class="hoverer_block__dishover">
            <!-- <img src="{.//property[@name='photo']/value}" alt="" /> -->
<!--                 <xsl:call-template name="makeThumbnailFull">
                    <xsl:with-param name="alt" select="name"/>
                    <xsl:with-param name="pwd" select=".//property[@name='photo']/value/@path"/>
                    <xsl:with-param name="element_id" select="@id"/>
                    <xsl:with-param name="width" select="'746'"/>
                    <xsl:with-param name="height" select="'auto'"/>
                </xsl:call-template> -->
                <span class="hoverer_block__dishover_image" style="background-image:url({document(concat('udata://system/makeThumbnailFull/(', .//property[@name='photo']/value/@path,')/746/auto/notemplate/0/1'))/udata/src});"></span>
                <span><xsl:value-of select=".//property[@name='h1']/value"/></span>
            </div>
            <div class="hoverer_block__hover"><span><xsl:value-of select=".//property[@name='h1']/value"/></span>
                <xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes"/>
            </div>
        </a>

    </xsl:template>

    <xsl:template match="item" mode="port_attributeset">objects_list_item mix filter-<xsl:value-of select="./@id" /><xsl:text> </xsl:text></xsl:template>
    
</xsl:stylesheet>
