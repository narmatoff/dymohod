<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!-- страница выставок -->
	
	<xsl:template match="result[@module = 'photoalbum'][@method = 'album'][@pageId=&expoId;]">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                	<xsl:value-of select=".//property[@name='descr']/value"  disable-output-escaping="yes"/>
                </section>
            </div>
        </div>

        <div class="full_width full_width__light">
        	<xsl:variable name="expo" select="document(concat('udata://photoalbum/albums//10//', $pageId, '//data_nachala/desc?extProps=h1,descr,data_nachala,data_okonchaniya,menu_pic_a'))/udata" />

            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
                    	<xsl:apply-templates select="$expo/items/item" mode="expo-album"/>
                    </article>
                </section>
	            <xsl:call-template name="numpages">
			        <xsl:with-param name="total" select="$expo/total" />
			        <xsl:with-param name="limit" select="$expo/per_page" />
			    </xsl:call-template>
            </div>
        </div>
	</xsl:template>
	
<!-- 	<xsl:template match="item[position() = 1]" mode="expo-album">
		<div class="view_object_big">
			<xsl:call-template name="makeThumbnailFull">
                <xsl:with-param name="alt" select="name"/>
                <xsl:with-param name="pwd" select=".//property[@name='menu_pic_a']/value/@path"/>
                <xsl:with-param name="element_id" select="@id"/>
                <xsl:with-param name="width" select="'729'"/>
                <xsl:with-param name="height" select="'482'"/>
            </xsl:call-template>
            <div class="view_object_big_content">
                <div class="view_object_big_h"><a href="{@link}">ближайшая выставка</a></div>
                <div class="view_object_big_smlh"><xsl:value-of select=".//property[@name='h1']/value"/></div>
                <div class="view_object_big_date">
					<xsl:value-of
                        select="document(concat('udata://system/convertDate/', .//property[@name='data_nachala']/value/@unix-timestamp, '/(d.m.Y)'))"/>
                	<xsl:text> - </xsl:text>
                	<xsl:value-of
                        select="document(concat('udata://system/convertDate/', .//property[@name='data_okonchaniya']/value/@unix-timestamp, '/(d.m.Y)'))"/>
				</div>
                <div class="view_object_big_text">
					<xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes"/>
                </div>
            </div>
        </div>
	</xsl:template> -->

	<xsl:template match="item" mode="expo-album">
		<xsl:choose>
			<xsl:when test="position()=1 and $p=0">
				<div class="view_object_big">
					<xsl:call-template name="makeThumbnailFull">
		                <xsl:with-param name="alt" select="name"/>
		                <xsl:with-param name="pwd" select=".//property[@name='menu_pic_a']/value/@path"/>
		                <xsl:with-param name="element_id" select="@id"/>
		                <xsl:with-param name="width" select="'729'"/>
		                <xsl:with-param name="height" select="'482'"/>
		            </xsl:call-template>
		            <div class="view_object_big_content">
		                <div class="view_object_big_h"><a href="{@link}">ближайшая выставка</a></div>
		                <div class="view_object_big_smlh"><xsl:value-of select=".//property[@name='h1']/value"/></div>
		                <div class="view_object_big_date">
							<xsl:value-of
		                        select="document(concat('udata://system/convertDate/', .//property[@name='data_nachala']/value/@unix-timestamp, '/(d.m.Y)'))"/>
		                	<xsl:text> - </xsl:text>
		                	<xsl:value-of
		                        select="document(concat('udata://system/convertDate/', .//property[@name='data_okonchaniya']/value/@unix-timestamp, '/(d.m.Y)'))"/>
						</div>
		                <div class="view_object_big_text">
							<xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes"/>
		                </div>
		            </div>
		        </div>
			</xsl:when>
			<xsl:otherwise>
				<div class="view_object">
					<a class="" href="{@link}">
						<xsl:call-template name="makeThumbnailFull">
			                <xsl:with-param name="alt" select="name"/>
			                <xsl:with-param name="pwd" select=".//property[@name='menu_pic_a']/value/@path"/>
			                <xsl:with-param name="element_id" select="@id"/>
			                <xsl:with-param name="width" select="'203'"/>
			                <xsl:with-param name="height" select="'204'"/>
			            </xsl:call-template>
					</a>
		            <div class="view_object_descr">
		                <h2 class="h_exp"><a href="{@link}"><xsl:value-of select=".//property[@name='h1']/value"/></a></h2>
		                <div class="view_object_descr_date">
							<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='data_nachala']/value/@unix-timestamp, '/(d.m.Y)'))"/>
		                	<xsl:text> - </xsl:text>
		                	<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='data_okonchaniya']/value/@unix-timestamp, '/(d.m.Y)'))"/>
		                </div>
						<xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes"/>
		            </div>
		        </div>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>	

	<!-- страница выставки -->
	
	<xsl:template match="result[@module = 'photoalbum'][@method = 'album'][page/@type-id=&expoTypeId;]">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
					<div class="pre_h">
	                	<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='data_nachala']/value/@unix-timestamp, '/(d.m.Y)'))"/>
	                	<xsl:text> - </xsl:text>
	                	<xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='data_okonchaniya']/value/@unix-timestamp, '/(d.m.Y)'))"/>
					</div>

                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                	<xsl:value-of select=".//property[@name='descr']/value"  disable-output-escaping="yes"/>
                </section>
            </div>
        </div>

    	<xsl:apply-templates select="document(concat('udata://photoalbum/album/',$pageId,'//100'))/udata" mode="expo_item"/>
        
	</xsl:template>

	<xsl:template match="udata[@module = 'photoalbum'][@method = 'album']" mode="expo_item">
		<xsl:apply-templates select="items" mode="expo_item-items"/>
	</xsl:template>

	<xsl:template match="items" mode="expo_item-items">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section>
                    <div class="objects_list_items">
                    	<xsl:apply-templates select="item" mode="news-photo_items_item"/>
                    	
                        <div class="grid-sizer"></div>
                        <div class="gutter-sizer"></div>
                    </div>
                </section>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="cls_img_size">
		<xsl:variable name="obj" select="document(concat('uobject://',@id))" />
		<xsl:value-of select="$obj//property[@name='razmer']/value"/>
	</xsl:template>

	<!-- обои рабочего стола -->

	<xsl:template match="udata[@module = 'photoalbum'][@method = 'album']" mode="wallpapers_album">
		<xsl:param name="root-class" />
		<xsl:apply-templates select="items" mode="wallpapers_album-items">
			<xsl:with-param name="root-class" select="$root-class"/>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="items" mode="wallpapers_album-items">
		<xsl:param name="root-class" />
		<div class="{$root-class}">
            <div class="wallpaper_item_dim">
            	<xsl:apply-templates select="item" mode="wallpapers_album-item"/>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="wallpapers_album-item">
		<xsl:variable name="wallpaper" select="document(concat('upage://', @id))" />
    	<a href="{$wallpaper//property[@name='photo']/value}" class="bigpic"><xsl:value-of select="$wallpaper//name"/></a>
	</xsl:template>

	<!-- страница сертификатов -->
	
	<xsl:template match="result[@module = 'photoalbum'][@method = 'album'][@pageId=&sertsId;]">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                	<xsl:value-of select=".//property[@name='descr']/value"  disable-output-escaping="yes"/>
                </section>
            </div>
        </div>
        <div class="full_width full_width__light">
        	<xsl:variable name="photos" select="document(concat('udata://photoalbum/album/',$pageId,'//10'))/udata" />
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
                    	<xsl:apply-templates select="$photos/items/item" mode="photos-sert"/>
                    </article>
                </section>
            </div>
            <xsl:call-template name="numpages">
		        <xsl:with-param name="total" select="$photos/total" />
		        <xsl:with-param name="limit" select="$photos/per_page" />
		    </xsl:call-template>
            
        </div>
	</xsl:template>

	<xsl:template match="item" mode="photos-sert">
        <xsl:variable name="photo" select="document(concat('upage://', @id))/udata"/>

		<div class="view_object">
			<a class="view_object_image view_object_image_cert bigpic" href="{$photo//property[@name='photo']/value}">
                <xsl:call-template name="makeThumbnail">
                    <xsl:with-param name="alt" select="name"/>
                    <xsl:with-param name="pwd" select="$photo//property[@name='photo']/value/@path"/>
                    <xsl:with-param name="element_id" select="@id"/>
                    <xsl:with-param name="width" select="'248'"/>
                    <xsl:with-param name="height" select="'auto'"/>
                </xsl:call-template>
			</a>
            <div class="view_object_descr">
                <h2><a href="{$photo/page/@link}"><xsl:value-of select="$photo/page/name"/></a></h2>
                <xsl:value-of select="$photo//property[@name='descr']/value" disable-output-escaping="yes"/>
            </div>
        </div>
	</xsl:template>

	<!-- страница видеообзоров -->

	<xsl:template match="result[@module = 'photoalbum'][@method = 'album'][@pageId=&mainVideoId;]">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name">популярное</div>
                    </div>
                    <article>
                    	<xsl:value-of select=".//property[@name='descr']/value"  disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>
        <div class="full_width full_width__light">
            <div class="video_row">
				<xsl:apply-templates select="document(concat('udata://photoalbum/album/',$pageId,'//3/1/izbrannye_video/true'))/udata/items/item"  mode="video-item"/>
            </div>
        </div>
        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <h1 class="middle_heading"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                </section>
                <xsl:variable name="videos" select="document(concat('udata://photoalbum/album/',$pageId,'//8/////create_time/desc'))/udata" />

                <section class="video_twocolumns">
                	<xsl:apply-templates select="$videos/items/item" mode="video-item"/>
                </section>
                <xsl:call-template name="numpages">
			        <xsl:with-param name="total" select="$videos/total" />
			        <xsl:with-param name="limit" select="$videos/per_page" />
			    </xsl:call-template>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="item|page" mode="video-item">
        <xsl:variable name="video" select="document(concat('upage://', @id))/udata"/>

		<div class="video_row_item video_row_item_stopped">
            <div class="video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/{$video//property[@name='ssylka_na_video']/value}?autoplay=0&amp;showinfo=0&amp;controls=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                <div class="video_container" id="{$video//property[@name='ssylka_na_video']/value}" data-ytid="{$video//property[@name='ssylka_na_video']/value}"></div>
            </div>
            <div class="video_row_item_descr"><span><xsl:value-of select="$video//property[@name='h1']/value"/></span></div>
        </div>
	</xsl:template>

    <xsl:template match="udata[@module = 'photoalbum'][@method = 'album']" mode="sert">
        <div class="swiper_block_slider swiper-wrapper">


            <xsl:apply-templates select="items/item" mode="sert-item"/>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="sert-item">
        <xsl:variable name="photo" select="document(concat('upage://', @id))/udata"/>
        <div class="swiper_block_slide swiper_block_slide_light swiper_block_slide__cert swiper-slide">
            <a href="{$photo//property[@name='photo']/value}" class="certificat">
                <xsl:call-template name="makeThumbnail">
                    <xsl:with-param name="alt" select="name"/>
                    <xsl:with-param name="pwd" select="$photo//property[@name='photo']/value/@path"/>
                    <xsl:with-param name="element_id" select="@id"/>
                    <xsl:with-param name="width" select="'217'"/>
                    <xsl:with-param name="height" select="'305'"/>
                </xsl:call-template>
            </a>
            <a href="{$photo//property[@name='photo']/value}" class="swiper_block_name">
                <xsl:value-of select="$photo//name"/>
            </a>
            <div class="swiper_block_text">Действителен с
                <xsl:value-of
                        select="document(concat('udata://system/convertDate/', $photo//property[@name='srok_dejstviya_s']/value/@unix-timestamp, '/(d.m.Y)'))"/>
                <xsl:if test="$photo//property[@name='srok_dejstviya_po']"> по <xsl:value-of
                        select="document(concat('udata://system/convertDate/', $photo//property[@name='srok_dejstviya_po']/value/@unix-timestamp, '/(d.m.Y)'))"/></xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="udata[@module = 'photoalbum'][@method = 'album']" mode="sert-not_main">
        <div class="swiper_block_slider swiper-wrapper">
            <xsl:apply-templates select="items/item" mode="sert-item-not_main"/>
        </div>
    </xsl:template>

    <xsl:template match="udata[@module = 'photoalbum'][@method = 'albums']" mode="sert-not_main2">
        <div class="swiper_block_slider swiper-wrapper">
            <xsl:apply-templates select="items/item" mode="sert-item-not_main2"/>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="sert-item-not_main">
        <xsl:variable name="photo" select="document(concat('upage://', @id))/udata"/>
        
        <div class="swiper_block_slide swiper_block_slide__cert swiper-slide">
            <a href="{$photo//property[@name='photo']/value}" class="certificat">
                <xsl:call-template name="makeThumbnail">
                    <xsl:with-param name="alt" select="name"/>
                    <xsl:with-param name="pwd" select="$photo//property[@name='photo']/value/@path"/>
                    <xsl:with-param name="element_id" select="@id"/>
                    <xsl:with-param name="width" select="'217'"/>
                    <xsl:with-param name="height" select="'305'"/>
                </xsl:call-template>
            </a>
            <a href="{$photo//property[@name='photo']/value}" class="swiper_block_name">
                <xsl:value-of select="$photo//name"/>
            </a>
            <div class="swiper_block_text">С 
                <xsl:value-of
                        select="document(concat('udata://system/convertDate/', $photo//property[@name='srok_dejstviya_s']/value/@unix-timestamp, '/(d.m.Y)'))"/>
                <xsl:if test="$photo//property[@name='srok_dejstviya_po']"> по <xsl:value-of
                        select="document(concat('udata://system/convertDate/', $photo//property[@name='srok_dejstviya_po']/value/@unix-timestamp, '/(d.m.Y)'))"/></xsl:if>
            </div>
          </div>
    </xsl:template>

    <xsl:template match="item" mode="sert-item-not_main2">
        <xsl:variable name="photo" select="document(concat('upage://', @id))/udata"/>
    	
    	<div class="swiper_block_slide swiper_block_slide__cert swiper-slide">
    		<a href="{$photo//property[@name='photo']/value}" class="certificat" style="background-color: #ebeced;">
                <xsl:call-template name="makeThumbnailFull">
                    <xsl:with-param name="alt" select="name"/>
                    <xsl:with-param name="pwd" select="$photo//property[@name='menu_pic_a']/value/@path"/>
                    <xsl:with-param name="element_id" select="@id"/>
                    <xsl:with-param name="width" select="'217'"/>
                    <xsl:with-param name="height" select="'150'"/>
                </xsl:call-template>
            </a>
            <a href="{$photo//property[@name='photo']/value}" class="swiper_block_name">
                <xsl:value-of select="$photo//name"/>
            </a>
            <div class="swiper_block_text">С 
                <xsl:value-of
                        select="document(concat('udata://system/convertDate/', $photo//property[@name='data_nachala']/value/@unix-timestamp, '/(d.m.Y)'))"/>
                <xsl:if test="$photo//property[@name='data_okonchaniya']"> по <xsl:value-of
                        select="document(concat('udata://system/convertDate/', $photo//property[@name='data_okonchaniya']/value/@unix-timestamp, '/(d.m.Y)'))"/></xsl:if>
            </div>
		  </div>
    </xsl:template>


    <xsl:template match="udata[@module = 'photoalbum'][@method = 'album']" mode="news-photo">
    	<xsl:apply-templates select="items" mode="news-photo_items"/>
    </xsl:template>

    <xsl:template match="items" mode="news-photo_items">

    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section>
                	<div class="pre_h">
		            	
		            </div>
		            <div class="middle_heading">
		                <h2 class="middle_heading_name">
		                    Фотогалерая
		                </h2>
		            </div>
                    <div class="objects_list_items">
                    	<xsl:apply-templates select="item" mode="news-photo_items_item"/>
                    	<div class="grid-sizer"></div>
                        <div class="gutter-sizer"></div>
                    </div>
                </section>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="news-photo_items_item">
        <xsl:variable name="photo" select="document(concat('upage://', @id))/udata" />
		<xsl:variable name="class_for_img">
			<xsl:apply-templates select="$photo//property[@name='razmer_izobrazheniya']/value/item" mode="cls_img_size"/>
		</xsl:variable>
		<a href="{$photo//property[@name='photo']/value}">
			<xsl:attribute name="class">
				<xsl:text>objects_list_item mix category-a view_object_image</xsl:text>
				<xsl:value-of select="$class_for_img"/>
			</xsl:attribute>
			
			<xsl:variable name="w">
    			<xsl:choose>
    				<xsl:when test="$class_for_img = 'objects_list_item__2width'">
    					<xsl:text>746</xsl:text>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:text>360</xsl:text>
    				</xsl:otherwise>
    			</xsl:choose>
    		</xsl:variable>

    		<xsl:variable name="h">
    			<xsl:choose>
    				<xsl:when test="$class_for_img = 'objects_list_item__2height'">
    					<xsl:text>558</xsl:text>
    				</xsl:when>
    				<xsl:otherwise>
    					<xsl:text>279</xsl:text>
    				</xsl:otherwise>
    			</xsl:choose>
    		</xsl:variable>
			<xsl:call-template name="makeThumbnailFull">
				<xsl:with-param name="alt" select="$photo//name"/>
				<xsl:with-param name="pwd" select="$photo//property[@name='photo']/value/@path"/>
				<xsl:with-param name="element_id" select="@id"/>
				<xsl:with-param name="width" select="$w"/>
				<xsl:with-param name="height" select="$h"/>
				<xsl:with-param name="rel" select="'news-photo'"/>
			</xsl:call-template>
		</a>     

    </xsl:template>


</xsl:stylesheet>