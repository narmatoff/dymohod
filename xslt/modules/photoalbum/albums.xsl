<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	
	<xsl:template match="udata[@module = 'photoalbum'][@method = 'albums']" mode="wallpapers_albums">
		<div class="wallpapers_wr">
			<xsl:apply-templates select="items/item" mode="wallpapers_albums-item"/>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="wallpapers_albums-item">
		<xsl:variable name="albums_photo" select="document(concat('upage://',@id, '.header_pic'))/udata/property" />
		<div class="wallpaper_item">
			<xsl:call-template name="makeThumbnailFull">
        		<xsl:with-param name="alt" select="$albums_photo/title"/>
        		<xsl:with-param name="pwd" select="$albums_photo/value/@path"/>
        		<xsl:with-param name="element_id" select="@id"/>
        		<xsl:with-param name="width" select="'714'"/>
        		<xsl:with-param name="height" select="'411'"/>
        	</xsl:call-template>

        	<div class="wallpaper_item_dwnlds">
        		<xsl:apply-templates select="document(concat('udata://photoalbum/album/', @id, '//100/1///153'))/udata" mode="wallpapers_album">
        			<xsl:with-param name="root-class" select="'wallpaper_item_phone'"/>
        		</xsl:apply-templates>
				
				<xsl:apply-templates select="document(concat('udata://photoalbum/album/', @id, '//100/1///154'))/udata" mode="wallpapers_album">
        			<xsl:with-param name="root-class" select="'wallpaper_item_tablet'"/>
        		</xsl:apply-templates>

        		<xsl:apply-templates select="document(concat('udata://photoalbum/album/', @id, '//100/1///155'))/udata" mode="wallpapers_album">
        			<xsl:with-param name="root-class" select="'wallpaper_item_desktop'"/>
        		</xsl:apply-templates>
	            
	        </div>
        </div>
	</xsl:template>
</xsl:stylesheet>