<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <!--_______________________________________________________Main page________________________________________________________-->

    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@is-default='1']" priority="1">

        <div class="full_width full_width__light">
			<div class="main_width">
				<div class="section ctalog_widgets">
					<div class="header_block">
						<div class="header_block_descr">Дымоходы</div>
						<div class="header_block_name">Каталог продукции<a href="{document('upage://300')/udata/page/@link}" class="button button__yellow_d button__yellow_d_darkfont sliderbtn_pos">Весь каталог</a></div>
					</div>
                	<xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//', &catMainId;, '/100/1/0?extProps=menu_pic_ua,pervaya_stroka_v_menyu,vtoraya_stroka_v_menyu,bolshoj_blok'))/udata" mode="getCategoryList_main"/>
				</div>
			</div>
		</div>
		<div class="full_width" id="preferzzzz">
			<div class="section">
				<div class="prefers">
					<div class="middle_heading middle_heading__light"><img src="{$template-resources}dist/img/svg/mini_logo.svg" alt="" class="middle_heading_mini_logo"/>
						<div class="middle_heading_descr">наши преимущества</div>
						<div class="middle_heading_name">Дымоход Вулкан</div>
						<div class="middle_heading_text">
							<xsl:value-of select=".//property[@name='tekst_pered_zagolovkom']/value" disable-output-escaping="yes"/>
						</div>
					</div>
					<div class="prefers_counters">
						<xsl:call-template name="main-favorit">
							<xsl:with-param name="count" select="1"/>
							<xsl:with-param name="prefer_counter" select=".//property[@name='bolshoj_identifikator_1']/value"/>
							<xsl:with-param name="prefer_name" select=".//property[@name='podzagolovok_1']/value"/>
							<xsl:with-param name="prefer_content" select=".//property[@name='tekst_1']/value"/>
						</xsl:call-template>

						<xsl:call-template name="main-favorit">
							<xsl:with-param name="count" select="2"/>
							<xsl:with-param name="prefer_counter" select=".//property[@name='bolshoj_identifikator_2']/value"/>
							<xsl:with-param name="prefer_name" select=".//property[@name='podzagolovok_2']/value"/>
							<xsl:with-param name="prefer_content" select=".//property[@name='tekst_2']/value"/>
						</xsl:call-template>

						<xsl:call-template name="main-favorit">
							<xsl:with-param name="count" select="3"/>
							<xsl:with-param name="prefer_counter" select=".//property[@name='bolshoj_identifikator_3']/value"/>
							<xsl:with-param name="prefer_name" select=".//property[@name='podzagolovok_3']/value"/>
							<xsl:with-param name="prefer_content" select=".//property[@name='tekst_3']/value"/>
						</xsl:call-template>

					</div><a href="{document(concat('upage://', &aboutFactoryId;))/udata/page/@link}" class="button button__yellow_d button__yellow_d_lightfont">узнать подробнее</a>
				</div>
			</div>
		</div>
		<div class="full_width full_width__light">
			<div class="section">
				<div id="where_buy" class="main_width where_buy">
					<div class="middle_heading middle_heading__dark"><img src="{$template-resources}dist/img/svg/mini_logo_dark.svg" alt="" class="middle_heading_mini_logo"/>
						<div class="middle_heading_descr">адреса</div>
						<div class="middle_heading_name">где купить</div>
					</div>
					<xsl:apply-templates select="document(concat('udata://content/getList//', &whereBuyPageId;,'/2/1000/1/id//126/oficialnyj_predstavitel/true'))/udata" mode="off-dlr-list" />

				</div>
			</div>
		</div>
<!-- 		<div class="full_width full_width__light">
			<div class="section">
				<div onClick="style.pointerEvents='none'" class="overlay"></div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d8016.243509838737!2d30.491625850000002!3d59.84812915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1485512768773" width="100%" height="639" frameborder="0" style="border:0" allowfullscreen="allowfullscreen" class="map_search"></iframe>
				<div class="main_width position_absolute">
					<div class="map_searchblock"><img src="{$template-resources}dist/img/map_searchblock.png" alt=""/>
						<div class="map_searchblock_name">Найдите ближайшего к Вам дилера</div>
						<input type="text" placeholder="Введите название города" class="map_searchblock_input"/>
					</div>
				</div>
			</div>
		</div> -->
        <div class="full_width full_width__light full_width_map">
            <div class="section">
                <div class="overlay" onClick="style.pointerEvents='none'"></div>
                <div id="map"></div>
                <div class="map_searchblock"><img src="{$template-resources}dist/img/map_searchblock.png" alt=""/>
                    <div class="map_searchblock_name">Найдите ближайшего к Вам дилера</div>
                    <input class="map_searchblock_input" type="text" placeholder="Введите город или название магазина" id="project"/>
                    <div class="ui-widget map_searchblock_details"><img class="ui-state-default" src="" alt="" id="project-icon"/>
                        <p id="project-addr"></p>
                        <p id="project-phone"></p>
                        <p id="project-time"></p>
                        <p id="project-site"></p>
                        <p id="project-email"></p>
                        <p id="project-manager"></p>
                    </div>
                    <div class="map_searchblock_results"></div>
                </div>
            </div>
            <div class="map_spinner"></div>
        </div>
		<!-- новости на главной -->
		<xsl:apply-templates select="document('udata://news/lastlist/16/notemplate/20/1')" mode="main-news"/>
		
		<!-- ёбаный слайдер на главной -->
		<!-- <div class="full_width full_width__light">
			<div class="main_width">
				<div class="swiper_block swiper_block_one_color cert_slider section">
					<div class="middle_heading middle_heading__dark">
						<div class="middle_heading_descr">Наши</div>
						<div class="middle_heading_name middle_heading_name_throwed"><span>Серитификаты</span></div>
					</div>
					<xsl:apply-templates select="document('udata://photoalbum/album/(/sertifikaty/)//20/1')" mode="sert"/>
				</div>
			</div>
		</div> -->
    </xsl:template>


	
	<xsl:template match="value" mode="about-sertifikaty">
	    <a class="cert_list_item a_img_hoverer" href="{.}">
        	<xsl:call-template name="makeThumbnail">
        		<xsl:with-param name="alt" select="@alt"/>
        		<xsl:with-param name="pwd" select="@path"/>
        		<xsl:with-param name="element_id" select="@id"/>
        		<!-- <xsl:with-param name="width" select="'211'"/> -->
        		<xsl:with-param name="height" select="'334'"/>
        	</xsl:call-template>
	    </a>
	</xsl:template>

    <!--_______________________________________________________presscenter page________________________________________________________-->

    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&presscenterId;']" priority="1">
        <xsl:variable name="sertsPage" select="document(concat('upage://', &sertsId;))" />
        <xsl:variable name="sertsPageeeee" select="document('upage://232')" />
        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></div>
                    </div>
                </section>
            </div>
        </div>

        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <article>
                        <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>
        <xsl:apply-templates select="document('udata://news/lastlist/16/notemplate/20/1')" mode="main-news"/>
        <div class="main_width" style="height:32em;">
            <div class="swiper_block cert_slider section">
                <div class="header_block">
                    <div class="header_block_descr">наши</div>
                    <div class="header_block_name">выставки</div>
                    <div class="swiper_block_slider_nav">
                        <div class="swiper_block_slider_nav_prev"></div>
                        <div class="swiper_block_slider_nav_next"></div>
                    </div>
                    <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$sertsPageeeee/udata/page/@link}">все выставки</a>
                </div>
                <xsl:apply-templates select="document('udata://photoalbum/albums//20//232//data_nachala/desc')" mode="sert-not_main2" />
            </div>
        </div>
        <xsl:apply-templates select="document('udata://news/lastlist/18/notemplate/20/1')" mode="main-news2"/>

    </xsl:template>

	
	<!--_______________________________________________________Три страницы с главной________________________________________________________-->
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id='&threeMainTypeId;']" priority="1">
		


		<div class="full_width full_width__light">
			<div class="middle_nav middle_nav_centered main_width">
				<xsl:apply-templates select="document('udata://menu/draw/threemain')/udata/item" mode="middle_nav"/>
			</div>
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></div>
                    </div>
                </section>
            </div>
        </div>

		<xsl:if test=".//property[@name='menu_pic_a']/value">
	        <div class="full_width">
	            <div class="section">
	                <div class="infoblock" data-0-start="background-position:0 -2000px;" data-0-end="background-position:0 500px;" style="background-image:url('{.//property[@name='menu_pic_a']/value}');">
	                    <div class="middle_heading middle_heading__light">
	                        <div class="infoblock_name">
	                        	<xsl:value-of select=".//property[@name='zagolovok_na_izobrazhenii']/value" disable-output-escaping="yes"/>
	                        </div>
	                        <div class="infoblock_text">
	                        	<xsl:value-of select=".//property[@name='tekst_na_izobrazhenii']/value" disable-output-escaping="yes"/>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
		</xsl:if>

 		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
	                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>

		<xsl:variable name="videoPage" select="document(concat('upage://', &mainVideoId;))" />
		
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select="$videoPage//property[@name='h1']/value"/></div>
                    </div>
	                <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$videoPage/udata/page/@link}">смотреть все</a>

                    <article>
                    </article>
                </section>
            </div>
        </div>
        <div class="full_width full_width__light">
            <div class="video_row">
				<xsl:apply-templates select=".//property[@name='video']/value/page"  mode="video-item"/>
            </div>
        </div>




        <script>
            document.addEventListener("DOMContentLoaded", function(event) { 
                var $objects = $('.objects_list_items').isotope({
                    // options
                    itemSelector: '.objects_list_item',
                    // layoutMode: 'fitColumns',
                    percentPosition: true,
                    masonry: {
                        columnWidth: '.grid-sizer',
                        gutter: '.gutter-sizer',
                    }
                });

                $('.object_tag').click(function() {
                    $('.object_tag').removeClass('object_tag__selected');
                    $(this).addClass('object_tag__selected');
                    console.debug($(this).data('filter'));
                    switch ($(this).data('filter')) {
                        <xsl:apply-templates select="document('usel://type_photo_obj')/udata/item" mode="tagger" />

                        case '.show_all':
                            $objects.isotope({ filter: '*' });
                            break;
                    }
                });

                $('.show_all').trigger('click');
            });
        </script>

        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                
                <section>


                    <div class="pre_h"><xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h" /></div>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
	                <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{document(concat('upage://', &allObjectsId;))/udata/page/@link}#{page/@object-id}">смотреть все</a>



                    <div class="objects_list_items">

                        <xsl:apply-templates select="document(concat('udata://photoalbum/album/', &allObjectsId;,'//5/1/tip_obekta/', page/@object-id,'/?extProps=h1,photo,descr,tip_obekta,razmer_bloka'))/udata/items/item" mode="portf_strup" />
                        
                        <div class="grid-sizer"></div>
                        <div class="gutter-sizer"></div>
                    </div>
                </section>
            </div>
        </div>

        <div class="full_width full_width__light">
            <div class="main_width">
                <div class="section ctalog_widgets">
                    <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//', &catMainId;, '/100/1/0?extProps=menu_pic_ua,pervaya_stroka_v_menyu,vtoraya_stroka_v_menyu,bolshoj_blok'))/udata" mode="getCategoryList_main"/>
                    
                </div>
            </div>
        </div>

    </xsl:template>

	<!--_______________________________________________________Карта сайта________________________________________________________-->
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&sitemapId;']" priority="1">
    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></div>
                    </div>
                </section>
            </div>
        </div>

 		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
                    	<p>
	                    	<xsl:apply-templates select="document('udata://content/sitemap/')/udata"/>
                    	</p>
	                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>
    </xsl:template>

	<!--_______________________________________________________Страница стать дилером________________________________________________________-->
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&dealerId;']" priority="1">
    	<div class="full_width full_width__light">
            <div class="main_width where_buy" id="where_buy">
                <section class="section_centered">
	                <div class="middle_heading middle_heading__dark">
						<div class="middle_heading_descr"><xsl:value-of select=".//property[@name='predzagolovok']/value"/></div>
	                    <h1 class="middle_heading_name">
	                        <xsl:value-of select=".//property[@name='h1']/value"/>
	                    </h1>
					</div>
	                <article>
	                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
						<xsl:apply-templates select="document('udata://webforms/add/145')/udata" mode="dealer-form"/>
	                </article>
	            </section>
            </div>
        </div>
    </xsl:template>

    <!--_______________________________________________________Страница о заводе________________________________________________________-->
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@type-id='&aboutTypeId;']" priority="1">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                </section>
            </div>
        </div>
        <div class="full_width">
            <div class="section">
                <div class="infoblock" data-0-start="background-position:0 -2000px;" data-0-end="background-position:0 500px;" style="background-image:url('{.//property[@name='menu_pic_a']/value}');">
                    <div class="middle_heading middle_heading__light">
                        <div class="infoblock_name">
                        	<xsl:value-of select=".//property[@name='zagolovok_na_izobrazhenii']/value" disable-output-escaping="yes"/>
                        </div>
                        <div class="infoblock_text">
                        	<xsl:value-of select=".//property[@name='tekst_na_izobrazhenii']/value" disable-output-escaping="yes"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
			        	<xsl:apply-templates select=".//property[@name='video']/value/page" mode="video-item"/>
                    	<xsl:value-of select=".//property[@name='levaya_tekstovaya_kolonka']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>	    
    </xsl:template>
	
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&downloadId;']" priority="1">
    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                </section>
            </div>
        </div>
		
		<xsl:variable name="filemanager-items" select="document(concat('udata://filemanager/list_files/', $pageId, '/24'))/udata"/>

        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
				<xsl:apply-templates select="$filemanager-items"/>
				
				<xsl:call-template name="numpages">
			    	<xsl:with-param name="total" select="$filemanager-items/total" />
			        <xsl:with-param name="limit" select="$filemanager-items/per_page" />
			    </xsl:call-template>
            </div>
        </div>
	</xsl:template>

	<!-- wallpapers page -->
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&wallpapersId;']" priority="1">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                </section>
            </div>
        </div>

		<xsl:variable name="wallpapers_albums" select="document(concat('udata://photoalbum/albums//10//', $pageId))/udata"/>

        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
            	<xsl:apply-templates select="$wallpapers_albums" mode="wallpapers_albums"/>
                
                 <xsl:call-template name="numpages">
			        <xsl:with-param name="total" select="$wallpapers_albums/total" />
			        <xsl:with-param name="limit" select="$wallpapers_albums/per_page" />
			    </xsl:call-template>
            </div>
        </div>
    </xsl:template>

    <!-- whre buy page -->
    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id=  '&whereBuyPageId;' or page/@type-id='&whereBuyTypeId;']" priority="1">

    	<div class="full_width full_width__light">
    		<div class="middle_nav middle_nav_centered main_width">
			    <a class="button button__yellow_d button__yellow_d_darkfont anchor" href="#zavod">
			        представительства завода
			    </a>
			    <a class="button button__yellow_d button__yellow_d_darkfont anchor" href="#general">
			        ГЕНЕРАЛЬНЫЕ ДИСТРИБЬЮТОРЫ
			    </a>
			    <a class="button button__yellow_d button__yellow_d_darkfont anchor" href="#authdlr">
			        АВТОРИЗОВАННЫЕ ДИЛЕРСКИЕ ЦЕНТРЫ
			    </a>
			    <a class="button button__yellow_d button__yellow_d_darkfont anchor" href="#map">
			        ДИЛЕРЫ
			    </a>
			</div>

            <div class="main_width where_buy" id="zavod">
                <section class="section_centered">
                    <div class="middle_heading middle_heading__dark">
                        <div class="middle_heading_descr"><xsl:value-of select=".//property[@name='predzagolovok']/value"/></div>
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                    <article >
                    	<xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/> 
                    </article>
                </section>

                <!-- тут мы как то выбираем только оффициалов -->
                <xsl:apply-templates select="document(concat('udata://content/getList//', $pageId,'/2/1000/1/id//126/oficialnyj_predstavitel/true'))/udata" mode="off-dlr-list" />

                
            </div>
        </div>
      
        <div class="full_width full_width__light full_width_map">
            <div class="section">
                <div class="overlay" onClick="style.pointerEvents='none'"></div>
                <div id="map"></div>
                <div class="map_searchblock"><img src="{$template-resources}dist/img/map_searchblock.png" alt=""/>
                    <div class="map_searchblock_name">Найдите ближайшего к Вам дилера</div>
                    <input class="map_searchblock_input" type="text" placeholder="Введите город или название магазина" id="project"/>
                    <a class="map_searchblock_backbutton" id="backbutton" href="javascript://">Назад к результатам</a>
                    <div class="ui-widget map_searchblock_details"><img class="ui-state-default" src="" alt="" id="project-icon"/>
                        <p id="project-addr"></p>
                        <p id="project-phone"></p>
                        <p id="project-time"></p>
                        <p id="project-site"></p>
                        <p id="project-email"></p>
                        <p id="project-manager"></p>
                    </div>
                    <div class="map_searchblock_results"></div>
                </div>
            </div>
            <div class="map_spinner"></div>
        </div>
        

		<div class="full_width full_width__light" id="general">
            <div class="main_width where_buy" id="where_buy">
                <section class="section_centered">
                    <div class="middle_heading middle_heading__dark">
                        <div class="middle_heading_descr"><xsl:value-of select=".//property[@name='tekst_nad_zagolovkom']/value"/></div>
                        <div class="middle_heading_name">ГЕНЕРАЛЬНЫЕ ДИСТРИБЬЮТОРЫ</div>
                    </div>
                    <article>
                    	<!-- <xsl:value-of select=".//property[@name='prizyvayuwij_tekst']/value" disable-output-escaping="yes" /> -->
<!-- 
                        <fieldset data-filter-group="data-filter-group" data-logic="and" id="FilterSelect">
                            <input type="radio" name="radiog_lite" value="" id="radio0"/>
                            <label class="radGroup1 radGroup1 css-radio" for="radio0">Все</label>
                            <xsl:apply-templates select="document('usel://type_gde_kupit')//item" mode="gdeKupit"/>
                        </fieldset> -->

                        
                        <div>
                            <div class="choose_twn">Выберите город</div>
                            <!-- <select class="middle_form filter_select" data-filter-group="data-filter-group" data-logic="and" id="SortSelect"> -->
                            <select class="middle_form filter_select" id="SortSelect">
                                <option value="*">Все</option>
                                <xsl:apply-templates select="document('udata://content/dlrCytyList/126/distribyutor/gorod')/udata/guide/item" mode="dlr-city-list">
                                <xsl:with-param name="id" select="'Container'" />
                            </xsl:apply-templates>
                            </select>
                        </div>
                    </article>
                </section>
            	<xsl:apply-templates select="document(concat('udata://content/getList//', $pageId,'/2/10000/1/ord/asc/126/distribyutor/true'))/udata" mode="dlr-list" >
                <xsl:with-param name="id" select="'Container'" />
                </xsl:apply-templates>

            </div>
        </div>

        <div class="full_width full_width__light" id="authdlr">
            <div class="main_width where_buy" id="where_buy">
                <section class="section_centered">
                    <div class="middle_heading middle_heading__dark">
                        <div class="middle_heading_descr"><xsl:value-of select=".//property[@name='tekst_nad_zagolovkom']/value"/></div>
                        <div class="middle_heading_name"><xsl:value-of select=".//property[@name='zagolovok']/value"/></div>
                    </div>
                    <article>
                    	<xsl:value-of select=".//property[@name='prizyvayuwij_tekst']/value" disable-output-escaping="yes" />
<!-- 
                        <fieldset data-filter-group="data-filter-group" data-logic="and" id="FilterSelect">
                            <input type="radio" name="radiog_lite" value="" id="radio0"/>
                            <label class="radGroup1 radGroup1 css-radio" for="radio0">Все</label>
                            <xsl:apply-templates select="document('usel://type_gde_kupit')//item" mode="gdeKupit"/>
                        </fieldset> -->

                        
                        <div>
                            <div class="choose_twn">Выберите город</div>
                            <!-- <select class="middle_form filter_select" data-filter-group="data-filter-group" data-logic="and" id="SortSelect"> -->
                            <select class="middle_form filter_select" id="SortSelect2">
                                <option value="*">Все</option>
                                <xsl:apply-templates select="document('udata://content/dlrCytyList/126/izbrannyj/gorod')/udata/guide/item" mode="dlr-city-list">
                                <xsl:with-param name="id" select="'Container'" />
                            </xsl:apply-templates>
                            </select>
                        </div>
                    </article>
                </section>
            	<xsl:apply-templates select="document(concat('udata://content/getList//', $pageId,'/2/10000/1///126/izbrannyj/true'))/udata"  mode="dlr-list">
                    <xsl:with-param name="id" select="'Container2'" />
                </xsl:apply-templates>

            </div>
        </div>
    </xsl:template>
    <xsl:template match="item" mode="gdeKupit">
        <input type="radio" name="radiog_lite" value=".category-{@id}" id="radio{position()}"/>
        <label class="radGroup1 radGroup1 css-radio" for="radio{position()}"><xsl:value-of select="@name"/></label>
    </xsl:template>

	<xsl:template match="item" mode="dlr-city-list">
        <option value=".category-{@md5}"><xsl:value-of select="@name"/></option>
	</xsl:template>

<!--_______________________________________________________Поддержка________________________________________________________-->

    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&podderzhkaId;']" priority="1">

    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></div>
                    </div>
                </section>
            </div>
        </div>

        <div class="full_width">
            <div class="section">
                <div class="infoblock infoblock_support" data-0-start="background-position:0 0;" data-0-end="background-position:0 -200px;" style="background-image: url('{.//property[@name='header_pic']/value}');">
                    <div class="middle_heading middle_heading__light">
                        <div class="infoblock_name"><xsl:value-of select=".//property[@name='tekst_nad_zagolovkom']/value"/></div>
                        <div class="infoblock_text"><xsl:value-of select=".//property[@name='zagolovok']/value"/></div>
                    </div><a class="button button__white" href="{document(concat('upage://', &aboutFactoryId;))/udata/page/@link}">узнать подробнее</a>
                </div>
            </div>
        </div>
		
		<xsl:variable name="videoPage" select="document(concat('upage://', &mainVideoId;))" />
		
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select="$videoPage//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select="$videoPage//property[@name='h1']/value"/></div>
                    </div>
	                <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$videoPage/udata/page/@link}">смотреть все</a>

                    <article>
                    	<xsl:value-of select="$videoPage//property[@name='descr']/value"  disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>
        <div class="full_width full_width__light">
            <div class="video_row">
				<xsl:apply-templates select="document(concat('udata://photoalbum/album/',&mainVideoId;,'//3/1/izbrannye_video/true'))/udata/items/item"  mode="video-item"/>
            </div>
        </div>
        
		<xsl:variable name="sertsPage" select="document(concat('upage://', &sertsId;))" />
		
        <div class="main_width">
            <div class="swiper_block cert_slider section">
                <div class="header_block">
                    <div class="header_block_descr">наши</div>
                    <div class="header_block_name">сертификаты</div>
                    <div class="swiper_block_slider_nav">
                        <div class="swiper_block_slider_nav_prev"></div>
                        <div class="swiper_block_slider_nav_next"></div>
                    </div>
	                <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$sertsPage/udata/page/@link}">смотреть все</a>
                </div>
                <xsl:apply-templates select="document('udata://photoalbum/album/8//20/1')" mode="sert-not_main" />
            </div>
        </div>

		<xsl:variable name="downloadPage" select="document(concat('upage://', &downloadId;))" />
    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select="$downloadPage//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select="$downloadPage//property[@name='h1']/value"/></h1>
                    </div>
	                <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$downloadPage/udata/page/@link}">смотреть все</a>
                    <xsl:value-of select="$downloadPage//property[@name='content']/value" disable-output-escaping="yes"/>
                </section>
            </div>
        </div>
		
		<xsl:variable name="filemanager-items" select="document(concat('udata://filemanager/list_files/', &downloadId;, '//2/1'))/udata"/>

        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
				<xsl:apply-templates select="$filemanager-items"/>
            </div>
        </div>
	
		<xsl:variable name="faqPage" select="document(concat('upage://', &faqId;))" />
		
		<xsl:variable name="faq" select="document(concat('udata://faq/category/notemplate/',&faqId;, '/2/1///publish_time/desc?extProps=publish_time,nick'))/udata"/>

		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
            	<section class="section_centered">
                	<xsl:apply-templates select="$faqPage//property[@name='predzagolovok']" mode="pre_h"/>
	                    <div class="middle_heading">
	                        <div class="middle_heading_name"><xsl:value-of select="$faqPage//property[@name='h1']/value"/></div>
	                    </div>
	                    <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$faqPage/udata/page/@link}">смотреть все</a>
	                    
	                    <xsl:value-of select="$faqPage//property[@name='content']/value" disable-output-escaping="yes"/>
	                    <a class="button button__white ajax_popup" href="/faq/addQuestionForm/">задать вопрос</a>
					<xsl:apply-templates select="$errors"/>

                </section>
				
                <section class="faq_blocks">
					<xsl:apply-templates select="$faq/items/item" mode="faqCategory"/>
                    <div class="faq_gutter"></div>
                    <div class="faq_sizer"></div>
                </section>
            </div>  
        </div>


		<xsl:variable name="wallpapersPage" select="document(concat('upage://', &wallpapersId;))" />
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select="$wallpapersPage//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select="$wallpapersPage//property[@name='h1']/value"/></div>
                    </div>
	                <a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{$wallpapersPage/udata/page/@link}">смотреть все</a>

                    <xsl:value-of select="$wallpapersPage//property[@name='content']/value" disable-output-escaping="yes"/>
                </section>
            </div>
        </div>

		<xsl:variable name="wallpapers_albums" select="document(concat('udata://photoalbum/albums//2//', &wallpapersId;))/udata"/>

        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
            	<xsl:apply-templates select="$wallpapers_albums" mode="wallpapers_albums"/>
            </div>
        </div>



<!--         <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <article>
                        <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>
 -->


<!--         <div class="main_width">
            <div class="swiper_block stati_slider section">
                <div class="header_block">
                    <div class="header_block_descr">тематические</div>
                    <div class="header_block_name">полезные статьи</div>
                    <div class="swiper_block_slider_nav">
                        <div class="swiper_block_slider_nav_prev"></div>
                        <div class="swiper_block_slider_nav_next"></div>
                    </div><a class="button button__yellow_d button__yellow_darkfont sliderbtn_pos" href="{document('udata://news/lastlist/18//10/0')/udata/archive_link}">читать все</a>
                </div>

                <div class="swiper_block_slider swiper-wrapper">
                    <xsl:apply-templates select="document('udata://news/lastlist/18//10/0')/udata/items/item" mode="statii"/>
                </div>
            </div>
        </div> -->
        
        
    </xsl:template>

    <xsl:template match="item" mode="statii">
        <xsl:variable name="itemsta" select="document(concat('upage://',@id))" />
        <xsl:variable name="itemstadata" select="$itemsta//property[@name='publish_time']/value/@unix-timestamp" />
        <xsl:variable name="itemstadatam" select="document(concat('udata://system/convertDate/',$itemstadata,'/(m)'))" />
        <xsl:variable name="itemstadatad" select="document(concat('udata://system/convertDate/',$itemstadata,'/(d)'))" />
        <div class="swiper_block_slide swiper_block_slide__cert swiper-slide">
            <div class="post_item">
                <div class="post_item_h">
                    <div class="post_item_date"><b><xsl:value-of select="$itemstadatad"/></b><span><xsl:value-of select="$itemstadatam"/></span></div>
                    <div class="post_item_nametags"><a class="post_item_name" href="{@link}">
                            <h2><xsl:value-of select="."/></h2></a></div>
                </div>
                <!-- <div class="post_item_tags"><a class="small_button" href="#">кнопка</a><a class="small_button" href="#">кнопка</a><a class="small_button" href="#">кнопка</a><a class="small_button" href="#">кнопка</a><a class="small_button" href="#">кнопка</a><a class="small_button" href="#">кнопка</a></div> -->
                <div class="post_item_text"><xsl:value-of select="$itemsta//property[@name='anons']/value" disable-output-escaping="yes"/></div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="albom">
        <xsl:variable name="itemal" select="document(concat('upage://',@id))" />
        <div class="video_row_item video_row_item_stopped">
            <div class="video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/{$itemal//property[@name='ssylka_na_video']/value}?autoplay=0&amp;showinfo=0&amp;controls=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                <div class="video_container" id="{$itemal//property[@name='ssylka_na_video']/value}" data-ytid="{$itemal//property[@name='ssylka_na_video']/value}"></div>
            </div>
            <div class="video_row_item_descr"><span><xsl:value-of select="."/></span></div>
        </div>
    </xsl:template>

    <!--_______________________________________________ЗАПРОС НА РАСЧЕТ ДЫМОХОДА ВУЛКАН____________________________________________-->

    <xsl:template match="result[@module = 'content'][@method = 'content'][page/@id='&calculateId;']" priority="1">
    	
        <div class="full_width full_width__light">
            <div class="main_width where_buy" id="where_buy">
                <section class="section_centered">
	                <div class="middle_heading middle_heading__dark">
						<div class="middle_heading_descr"><xsl:value-of select=".//property[@name='predzagolovok']/value"/></div>
	                    <h1 class="middle_heading_name">
	                        <xsl:value-of select=".//property[@name='h1']/value"/>
	                    </h1>
					</div>
	                <article>
	                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
						<xsl:apply-templates select="document('udata://webforms/add/170')/udata" mode="dealer-form"/>
	                </article>
	            </section>
            </div>
        </div>



    </xsl:template>

    <!--_______________________________________________________Content page________________________________________________________-->

    <xsl:template match="result[@module = 'content'][@method = 'content']">

 		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></div>
                    </div>
                </section>
            </div>
        </div>

 		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
	                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>

    </xsl:template>


</xsl:stylesheet>