<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi" xmlns:php="http://php.net/xsl" extension-element-prefixes="php">

	<!-- Блок для главной -->

	<xsl:template match="udata[@module = 'content'][@method = 'getList']" mode="main_for_what">
		<div class="for_what section">
			<xsl:apply-templates select="items/item" mode="main_for_what-item"/>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="main_for_what-item">
		<div class="for_what_block"><a href="{@link}">
				<div class="for_what_block__dishover">
                    <div class="for_what_block__triangle"></div>
					<xsl:variable name="w">
						<xsl:choose>
							<xsl:when test="position()='2'">350</xsl:when>
							<xsl:otherwise>330</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<xsl:variable name="h">
						<xsl:choose>
							<xsl:when test="position()='2'">415</xsl:when>
							<xsl:otherwise>385</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					
					<xsl:call-template name="makeThumbnail">
						<!--<xsl:with-param name="alt" select="@alt"/>-->
						<xsl:with-param name="pwd" select=".//property[@name='menu_pic_ua']/value/@path"/>
						<xsl:with-param name="element_id" select="@id"/>
						<xsl:with-param name="width" select="$w"/>
						<xsl:with-param name="height" select="$h"/>
					</xsl:call-template>
					<span><xsl:value-of select=".//property[@name='h1']/value"/></span>
				</div>
				<div class="for_what_block__hover"><span><xsl:value-of select=".//property[@name='h1']/value"/></span>
					<p><xsl:value-of select=".//property[@name='kratkoe_opisanie']/value"/></p>
				</div></a></div>
	</xsl:template>

    <!--_______________________________________________________Main page review getList________________________________________________________-->

    <xsl:template match="udata[@module = 'content'][@method = 'getList']" mode="main-review" >

		<xsl:apply-templates select="items" mode="main-review_items"/>
		
    </xsl:template>
	
	<xsl:template match="items" mode="main-review_items">
		<div class="full_width onepageslider">
      <div class="big_heading">&clients_request;</div>
      <div class="swiper-container call_backs">
        <div class="swiper-wrapper">
	      	<xsl:apply-templates select="item" mode="main-review_items_item"/>
        </div>
      </div>
      <div class="slider-button-next call_backs_arrow__next arrow__next"></div>
      <div class="slider-button-prev call_backs_arrow__prev arrow__prev"></div>
    </div>        
	</xsl:template>

	<xsl:template match="item" mode="main-review_items_item">

		<xsl:variable name="pwd">
			<xsl:choose>
				<xsl:when test=".//property[@name='fotografiya']/value/@path">
					<xsl:value-of select=".//property[@name='fotografiya']/value/@path"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>{$template-resources}dist/img/callbacker.jpg</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div class="swiper-slide call_backs_item">
            <div class="callbacker_img">
            	<xsl:call-template name="makeThumbnail">
            		<xsl:with-param name="alt" select="name"/>
            		<xsl:with-param name="pwd" select="$pwd"/>
            		<xsl:with-param name="element_id" select="@id"/>
            		<xsl:with-param name="width" select="'68'"/>
            		<xsl:with-param name="height" select="'68'"/>
            	</xsl:call-template>
            </div>
            <div class="callbacker_name"><xsl:value-of select="name"/></div>
            <div class="callbacker_date"><xsl:value-of select="document(concat('udata://content/convertDate/', .//property[@name='publish_time']/value/@unix-timestamp, '/d%20F%20Y'))/udata" disable-output-escaping="yes"/></div>
            <div class="callbacker_text"><xsl:value-of select=".//property[@name='tekst_otzyva']/value" disable-output-escaping="yes" /></div>
          </div>
        
	</xsl:template>


	<!--_______________________________________________________Uslugi page review getList________________________________________________________-->

    <xsl:template match="udata[@module = 'content'][@method = 'getList']" mode="uslugi-review" >
		<xsl:if test="total > 0">
			<xsl:apply-templates select="items" mode="uslugi-review_items"/>
		</xsl:if>
    </xsl:template>
	
	<xsl:template match="items" mode="uslugi-review_items">
			<div class="full_width onepageslider">
	      <div class="big_heading">&we_help;</div>
	      <div class="swiper-container call_backs">
	        <div class="swiper-wrapper">
		      	<xsl:apply-templates select="item" mode="main-review_items_item"/>
	        </div>
	      </div>
	      <div class="slider-button-next call_backs_arrow__next arrow__next"></div>
	      <div class="slider-button-prev call_backs_arrow__prev arrow__prev"></div>
	    </div>        
	</xsl:template>

  
	<!--_______________________________________________________About page review getList________________________________________________________-->

    <xsl:template match="udata[@module = 'content'][@method = 'getList']" mode="about-review" >
		
		<xsl:apply-templates select="items" mode="about-review_items"/>
		
    </xsl:template>
	
	<xsl:template match="items" mode="about-review_items">
	
	<div class="testimonials_sliderwr">
        <div class="pensils"></div>
        <div class="sunglasses"></div>
        <div class="coffeecup"></div>
        <div class="notebook"></div>
        <div class="testimonials-container swiper-container">
          <div class="swiper-wrapper">
	      	<xsl:apply-templates select="item" mode="about-review_items_item"/>
          </div>
          <div class="slider2-button-next testimonials_arrow__next arrow__next"></div>
          <div class="slider2-button-prev testimonials_arrow__prev arrow__prev"></div>
        </div>
      </div>
	</xsl:template>

	<xsl:template match="item" mode="about-review_items_item">
	
		<xsl:variable name="pwd">
			<xsl:choose>
				<xsl:when test=".//property[@name='fotografiya']/value/@path">
					<xsl:value-of select=".//property[@name='fotografiya']/value/@path"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>{$template-resources}dist/img/callbacker.jpg</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div class="swiper-slide testimonials_item">
		    <div class="testimonials_item_photo">
				<xsl:call-template name="makeThumbnail">
            		<xsl:with-param name="alt" select="name"/>
            		<xsl:with-param name="pwd" select="$pwd"/>
            		<xsl:with-param name="element_id" select="@id"/>
            		<xsl:with-param name="width" select="'128'"/>
            		<xsl:with-param name="height" select="'129'"/>
            	</xsl:call-template>
		    </div>
		    <div class="testimonials_item_name">
		        <xsl:value-of select="name"/>
		    </div>
		    <div class="testimonials_item_info">
		        <xsl:value-of select=".//property[@name='kratkaya_informaciya_o_sebe']/value" disable-output-escaping="yes" />
		    </div>
		    <div class="testimonials_item_about">
		        <xsl:value-of select=".//property[@name='tekst_otzyva']/value" disable-output-escaping="yes" />
		    </div>
		    <div class="testimonials_item_strategy">
		        <div class="testimonials_item_strategy_cel">
		            <img alt="" class="testims_img" src="{$template-resources}dist/img/svg/pricel.svg"/>
		            <div class="testims_name">
		                &cel;
		            </div>
		            <div class="testims_info">
		               	<xsl:value-of select=".//property[@name='cel_raboty']/value" disable-output-escaping="yes" />
		            </div>
		        </div>
		        <div class="testimonials_item_strategy_str">
		            <img alt="" class="testims_img" src="{$template-resources}dist/img/svg/strategy.svg"/>
		            <div class="testims_name">
		                &strateg;
		            </div>
		            <div class="testims_info">
		                <xsl:value-of select=".//property[@name='strategiya']/value" disable-output-escaping="yes" />
		            </div>
		        </div>
		        <div class="testimonials_item_strategy_result">
		            <img alt="" class="testims_img" src="{$template-resources}dist/img/svg/result.svg"/>
		            <div class="testims_name">
		                &result;
		            </div>
		            <div class="testims_info">
		                <xsl:value-of select=".//property[@name='rezultat']/value" disable-output-escaping="yes" />
		            </div>
		        </div>
		    </div>
		    <div class="testimonials_item_about">
		    	<xsl:if test=".//property[@name='fajl_s_otzyvom']/value">
					<a href="{.//property[@name='fajl_s_otzyvom']/value}" target="_blank">&read_letter;</a>
		    	</xsl:if>
		    </div>
		</div>

        
	</xsl:template>


	<xsl:template match="udata[@module = 'content'][@method = 'getList']" mode="off-dlr-list">
		<div class="column_lists">
			<xsl:apply-templates select="items/item" mode="off-dlr-list_item" />
            
        </div>
	</xsl:template>

	<xsl:template match="udata[@module = 'content'][@method = 'getList']" mode="dlr-list">
		<xsl:param name="id"/>
        <div class="column_lists container" id="{$id}">
			<xsl:apply-templates select="items/item" mode="off-dlr-list_item">
				<xsl:with-param name="is_filter" select="'1'"/>
			</xsl:apply-templates>
        </div>
	</xsl:template>

	<xsl:template match="item" mode="off-dlr-list_item">
		<xsl:param name="is_filter" />
		<xsl:variable name="page" select="document(concat('upage://', @id))" />
		<div>
			<xsl:attribute name="class">
				<xsl:text>column_list</xsl:text>
				<xsl:if test="$is_filter">
					<xsl:text> mix  category-</xsl:text>
					<!-- <xsl:apply-templates select="$page//property[@name='okazyvaet_uslugi']/value/item" mode="where_by_sort"/> -->
					<!-- <xsl:apply-templates select="$page//property[@name='gorod']/value" mode="where_by_sort"/> -->
					<xsl:value-of select="php:function('md5', concat($page//property[@name='gorod']/value, ''))"/>

				</xsl:if>
			</xsl:attribute>
                <div class="column_list_name">
                    <ul>
                    	<xsl:choose>
                    		<xsl:when test="$is_filter">
		                    	<xsl:apply-templates select="$page//property[@name='h1']/value" mode="dlr-column-val-h1"/>
                    		</xsl:when>
                    		<xsl:otherwise>
		                    	<xsl:apply-templates select="$page//property[@name='gorod']/value" mode="dlr-column-val-h1"/>
                    		</xsl:otherwise>
                    	</xsl:choose>
						<xsl:apply-templates select="$page//property[@name='adres']/value" mode="dlr-column-val-adres"/>
						<li>
							<div class="icon_wrap">
								<span class="icon phone_icon"></span>
							</div>
						    <span class="column_list_text">
						        <!-- <b>
						            Тел.
						        </b> -->
						        <a href="tel:{$page//property[@name='telefon']/value}">
						            <xsl:value-of disable-output-escaping="yes" select="$page//property[@name='telefon']/value"/>
						        </a>
						        <xsl:if test="$page//property[@name='telefon2']">, </xsl:if>
						        <a href="tel:{$page//property[@name='telefon2']/value}">
						            <xsl:value-of disable-output-escaping="yes" select="$page//property[@name='telefon2']/value"/>
						        </a>
						    </span>
						</li>
						<xsl:apply-templates select="$page//property[@name='rezhim_raboty']/value" mode="dlr-column-val-rezhim_raboty"/>
						<xsl:apply-templates select="$page//property[@name='email']/value" mode="dlr-column-val-email"/>
						<xsl:apply-templates select="$page//property[@name='sajt']/value" mode="dlr-column-val-sajt"/>
						<xsl:if test="$is_filter">
							<xsl:apply-templates select="$page//property[@name='okazyvaet_uslugi']/value" mode="dlr-column-val-okazyvaet_uslugi"/>
						</xsl:if>

                    </ul>
                </div>
            </div>
	</xsl:template>

	<xsl:template match="value" mode="dlr-column-val-h1">
		<li>
		    
		    <span class="column_list_text">
		        <b>
		            <xsl:value-of disable-output-escaping="yes" select="."/>
		        </b>
		    </span>
		</li>
	</xsl:template>

	<xsl:template match="value" mode="dlr-column-val-adres">
		<li>
			<div class="icon_wrap">
		    	<span class="icon baloon_icon_mini"></span>
			</div>
		    <span class="column_list_text">
		        <xsl:value-of disable-output-escaping="yes" select="."/>
		    </span>
		</li>
	</xsl:template>

	<xsl:template match="value" mode="dlr-column-val-rezhim_raboty">
		<li>
			<div class="icon_wrap">
		    <span class="icon clock_icon">
		    </span>
		</div>
		    <span class="column_list_text">
		        <!-- <b>
		            Режим работы:
		        </b> -->
		        <xsl:value-of disable-output-escaping="yes" select="."/>
		    </span>
		</li>
	</xsl:template>
	
	<xsl:template match="value" mode="dlr-column-val-email">
		<li>
			<div class="icon_wrap">
		    <span class="icon mail_icon">
		    </span>
		</div>
		    <span class="column_list_text">
		        <!-- <b>
		            E-mail:
		        </b> -->
		        <a href="mailto:{.}">
		            <xsl:value-of disable-output-escaping="yes" select="."/>
		        </a>
		    </span>
		</li>
	</xsl:template>

	<xsl:template match="value" mode="dlr-column-val-sajt">
		<li>
			<div class="icon_wrap">
	        <span class="icon site_icon">
	        </span>
	    </div>
	        <span class="column_list_text">
	            <!-- <b>
	                Сайт:
	            </b> -->
	            <a href="{.}" target="_blank" rel="nofollow noindex">
	                <xsl:value-of disable-output-escaping="yes" select="."/>
	            </a>
	        </span>
	    </li>
	</xsl:template>

	<xsl:template match="value" mode="dlr-column-val-okazyvaet_uslugi">
		<li>
			<div class="icon_wrap">
		    <span class="icon services">
		    </span>
		</div>
		    <span class="column_list_text">
		    	<!-- <b>
	                Оказывает услуги:
	            </b> -->
	            <xsl:apply-templates select="item" mode="dlr-column-val-okazyvaet_uslugi-item"/>
		        <xsl:value-of disable-output-escaping="yes" select="."/>
		    </span>
		</li>
	</xsl:template>

	<xsl:template match="item" mode="dlr-column-val-okazyvaet_uslugi-item">
		<xsl:value-of select="@name"/>
		<xsl:choose>
			<xsl:when test="position() = last()"></xsl:when>
			<xsl:otherwise>, </xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	
	

	<xsl:template match="value" mode="where_by_sort">
		<xsl:value-of select="php:function('md5', .)"/>
		<!-- <xsl:value-of select="concat(' category-', php:function('md5', .))"/> -->
		<!-- <xsl:value-of select="concat(' category-', @id)"/> -->
		
	</xsl:template>

</xsl:stylesheet>
