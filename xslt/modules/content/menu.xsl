<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <!--_______________________________________________________Main menu________________________________________________________-->
    <xsl:template match="udata[@module = 'menu'][@method = 'draw']" mode="menu-top">
		<ul class="mainmenu_items">
			<xsl:apply-templates select="item" mode="menu-top_item" />
		</ul>
    </xsl:template>
    <xsl:template match="item" mode="menu-top_item">
    	<li>
			<a href="{@link}">
	            <xsl:value-of select="@name" />
	        </a>
	    </li>
    </xsl:template>

    <xsl:template match="item[@status='active']" mode="menu-top_item">
    	<li class="current">
			<a href="{@link}">
	            <xsl:value-of select="@name" />
	        </a>
	    </li>
    </xsl:template>

	<xsl:template match="udata[@module = 'menu'][@method = 'draw']" mode="menu-bottom">
		<ul>
			<li>
				<xsl:value-of select="name/@name"/>
			</li>
			<xsl:apply-templates select="item" mode="menu-top_item" />
		</ul>

	</xsl:template>

	<xsl:template match="udata[@module = 'content'][@method = 'menu']" mode="middle_nav">
		<xsl:apply-templates select="items" mode="middle_nav"/>
	</xsl:template>

	<xsl:template match="items" mode="middle_nav">
		<div class="middle_nav middle_nav_centered main_width">
			<xsl:apply-templates select="item" mode="middle_nav"/>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="middle_nav">
		<a href="{@link}" class="button button__yellow_d button__yellow_d_darkfont">
			<xsl:attribute name="class">
				<xsl:text>button button__yellow_d button__yellow_d_darkfont</xsl:text>
				<xsl:if test="@status='active'">
					<xsl:text> button__yellow_d__active</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<xsl:value-of select="."/>
		</a> 
	</xsl:template>	
</xsl:stylesheet>
