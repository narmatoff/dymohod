<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

   <xsl:template match="udata[@method = 'lastlist']/archive"/>
   <xsl:template match="udata[@method = 'lastlist']/archive_link"/>
   <xsl:template match="udata[@method = 'lastlist']/total"/>
   <xsl:template match="udata[@method = 'lastlist']/per_page"/>
   <xsl:template match="udata[@method = 'lastlist']/category_id"/>
   <xsl:template match="udata[@method = 'lastlist']/extended"/>

   <xsl:template match="udata[@module = 'news'][@method = 'lastlist']" mode="main-news">
	<div class="full_width full_width__light">
		<div class="main_width">
			<div class="swiper_block news_slider section">
				<div class="header_block">
					<div class="header_block_descr">важные события компании</div>
					<div class="header_block_name">новости</div>
					<div class="swiper_block_slider_nav">
						<div class="swiper_block_slider_nav_prev"></div>
						<div class="swiper_block_slider_nav_next"></div>
					</div><a href="{archive_link}" class="button button__yellow_d button__yellow_d_darkfont sliderbtn_pos">Все новости</a>
				</div>
				<div class="swiper_block_slider swiper-wrapper">
					<xsl:apply-templates select="items/item" mode="main-news-item"/>
				</div>
			</div>
		</div>
	</div>
   </xsl:template>

   <xsl:template match="udata[@module = 'news'][@method = 'lastlist']" mode="main-news2">
	<div class="full_width  full_width__light">
		<div class="main_width">
			<div class="swiper_block news_slider section">
				<div class="header_block">
					<div class="header_block_descr">важные события компании</div>
					<div class="header_block_name">статьи</div>
					<div class="swiper_block_slider_nav">
						<div class="swiper_block_slider_nav_prev"></div>
						<div class="swiper_block_slider_nav_next"></div>
					</div><a href="{archive_link}" class="button button__yellow_d button__yellow_d_darkfont sliderbtn_pos">Все статьи</a>
				</div>
				<div class="swiper_block_slider swiper-wrapper">
					<xsl:apply-templates select="items/item" mode="main-news-item"/>
				</div>
			</div>
		</div>
	</div>
   </xsl:template>

   <xsl:template match="item" mode="main-news-item">
	   	<div class="swiper_block_slide swiper_block_slide_light swiper-slide">
	   		<a href="{@link}">
	   			<xsl:call-template name="makeThumbnailFull">
					<!--<xsl:with-param name="alt" select="@alt"/>-->
					<xsl:with-param name="pwd" select="document(concat('upage://', @id, '.publish_pic'))/udata/property/value/@path"/>
					<xsl:with-param name="element_id" select="@id"/>
					<xsl:with-param name="width" select="'309'"/>
					<xsl:with-param name="height" select="'196'"/>
				</xsl:call-template>
	   		</a>
			<div class="swiper_date">
				<xsl:value-of select="document(concat('udata://system/convertDate/', @publish_time,'/(d/m/Y)'))"/>
			</div>
			<a href="{@link}" class="swiper_block_name"><xsl:value-of select="."/></a>
			<div class="swiper_block_text">
				<xsl:value-of select="document(concat('upage://', @id, '.anons'))/udata/property/value" disable-output-escaping="yes"/>
			</div>
		</div>
   </xsl:template>


	<xsl:template match="udata[@method = 'lastlist']/items/item">
		<div class="statya">
        	<div class="statya_image">
        		<div class="post_item_date">
					<xsl:apply-templates select="document(concat('udata://content/convertDate/', @publish_time, '/d%20M/1'))/udata" mode="news-date"/>
				</div>
        		<a href="{@link}">
        			<xsl:call-template name="makeThumbnailFull">
						<!--<xsl:with-param name="alt" select="@alt"/>-->
						<xsl:with-param name="pwd" select=".//property[@name='publish_pic']/value/@path"/>
						<xsl:with-param name="element_id" select="@id"/>
						<xsl:with-param name="width" select="'265'"/>
						<xsl:with-param name="height" select="'183'"/>
					</xsl:call-template>
        		</a>
        	</div>
			<div class="statya_preview">
				<div class="post_item">
					<div class="post_item_h">
					<div class="post_item_nametags">
						<a href="{@link}" class="post_item_name">
					    	<h2><xsl:value-of select="@name"/></h2>
					    </a>
					    <xsl:apply-templates select=".//property[@name='tags']" mode="news-lents-tags"/>
					</div>
					</div>
					<div class="post_item_text"><xsl:value-of select=".//property[@name='anons']/value" disable-output-escaping="yes"/></div>
				</div>
			</div>
        </div>
	</xsl:template>

	<xsl:template match="property" mode="news-lents-tags">
		<div class="post_item_tags">
        	<!-- TODO: написать обработку кликов по тегам -->
        	<xsl:apply-templates select="value" mode="news-lents-tag"/>
		</div>
	</xsl:template>
	
	<xsl:template match="value|item" mode="news-lents-tag">
		<a data-tag="{.}">
			<xsl:if test="not($method = 'item')">
				<xsl:attribute name="href">
					<xsl:value-of select="concat('?fields_filter[tags][eq][]=',.)"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="$method = 'item'">
				<xsl:attribute name="onclick">
					<xsl:text>return false;</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="class">
				<xsl:text>small_button_nojs</xsl:text>
				<xsl:if test="@is_selected">
					<xsl:text> button__yellow_d__active</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<xsl:value-of select="."/>
		</a>
	</xsl:template>

	<xsl:template match="d" mode="news-date">
		<b><xsl:value-of select="."/></b>
	</xsl:template>
	
	
	<xsl:template match="M" mode="news-date">
		<span><xsl:value-of select="."/></span>
	</xsl:template>

</xsl:stylesheet>