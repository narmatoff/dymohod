<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'news'][@method = 'item']">

    <div class="full_width full_width__light">
        <div class="main_width main_width__white">
                    <h1 class="middle_heading"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
    	<section class="section_onefortwocols spoilered">
	        <article><a href="#" class="post_back"></a>
	        <div class="post_item">
				<div class="post_item_h">
				<div class="post_item_date">
					<xsl:apply-templates select="document(concat('udata://content/convertDate/', .//property[@name = 'publish_time']/value/@unix-timestamp, '/d%20M/1'))/udata" mode="news-date"/>
				</div>
				<div class="post_item_nametags">
					<a href="{@link}" class="post_item_name">
				    	<h2><xsl:value-of select="page/name"/></h2>
				    </a>
				    <xsl:apply-templates select=".//property[@name='tags']" mode="news-lents-tags"/>
				</div>
				</div>
			</div>
			<xsl:if test=".//property[@name='publish_pic']/value/@path">
				<xsl:call-template name="makeThumbnail">
	        		<xsl:with-param name="alt" select="name"/>
	        		<xsl:with-param name="pwd" select=".//property[@name='publish_pic']/value/@path"/>
	        		<xsl:with-param name="element_id" select="@id"/>
	        		<xsl:with-param name="class" select="'article_preview'"/>
	        		<xsl:with-param name="width" select="'728'"/>
	        		<xsl:with-param name="height" select="'auto'"/>
	        	</xsl:call-template>
			</xsl:if>
				<xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes" />
	        	<xsl:apply-templates select=".//property[@name='video']/value/page" mode="video-item"/>
                
	        </article>
	                        
	          <div class="social_share"><span>&share;</span>
              <div class="social_share_buttons"><script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script><script src="//yastatic.net/share2/share.js"></script><div class="ya-share2" data-services="vkontakte,facebook,gplus" data-counter=""></div></div>
            </div>
	      </section>

	<!-- 		<section class="video_twocolumns">
                    
                </section> -->

	 	 </div>
    </div>

    

    <xsl:apply-templates select="document(concat('udata://photoalbum/album/', .//property[@name='fotogalereya']/value/page/@id, '//500/1'))/udata" mode="news-photo"/>

	</xsl:template>

</xsl:stylesheet>