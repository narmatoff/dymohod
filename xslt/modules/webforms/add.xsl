<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">
<xsl:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink">


	
	<xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="dealer-form">
        <form class="validate_form" method="post" action="{$langPrefix}/webforms/send/" enctype="multipart/form-data">
            <xsl:apply-templates select="items" mode="address" />
            <xsl:apply-templates select="groups/group" mode="dealer-form-group" />
			<div>
				<input type="checkbox" name="checkbox" id="myText" required="required"/>
				<div id="dlrdiv" onclick="$('#myText').trigger('click')">Ознакомлен(а) и согласен(на) с <a href="/about/politika-obrabotki-personalnyh-dannyh/" target="_blank">политикой конфиденциальности</a></div>
			</div>
			
			<input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
			<input class="button button__white" type="submit" value="отправить"/>
		</form>		
	</xsl:template>
	
	<xsl:template match="group" mode="dealer-form-group">
		<xsl:if test="@name != 'nothead'">
			<div class="fieldset_name"><xsl:value-of select="@title"/></div>
		</xsl:if>
		<xsl:apply-templates select="field" mode="dealer-form-field"/>

	</xsl:template>

	<xsl:template match="field" mode="dealer-form-field">
		<div>
            <input class="middle_form" type="text" name="{@input_name}" value="">
				<xsl:if test="@required='required'">
					<xsl:attribute name="required"><xsl:text>required</xsl:text></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title"/>
					<xsl:if test="@required='required'"><xsl:text>*</xsl:text></xsl:if>
				</xsl:attribute>
            </input>
        </div>
	</xsl:template>

	<xsl:template match="field[@type='file']" mode="dealer-form-field">
		<div>
            <label for="{@input_name}"><xsl:value-of select="@title"/></label>
            <input class="middle_form" type="file" name="{@input_name}" value="">
				<xsl:if test="@required='required'">
					<xsl:attribute name="required"><xsl:text>required</xsl:text></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title"/>
					<xsl:if test="@required='required'"><xsl:text>*</xsl:text></xsl:if>
				</xsl:attribute>
            </input>
        </div>
	</xsl:template>

	<xsl:template match="field[@type='text']" mode="dealer-form-field">
		<div>
            <textarea class="middle_form" type="text" name="{@input_name}" value="">
				<xsl:if test="@required='required'">
					<xsl:attribute name="required"><xsl:text>required</xsl:text></xsl:attribute>
				</xsl:if>
				<xsl:attribute name="placeholder">
					<xsl:value-of select="@title"/>
					<xsl:if test="@required='required'"><xsl:text>*</xsl:text></xsl:if>
				</xsl:attribute>
            </textarea>
        </div>
	</xsl:template>
	

	<xsl:template match="field[@type='relation']" mode="dealer-form-field">
		<div>
            <select name="{@input_name}" class="middle_form">
            		<xsl:if test="@required='required'">
	            		<xsl:attribute name="required"><xsl:text>required</xsl:text></xsl:attribute>
            		</xsl:if>

            	<option disabled="disabled">
            		<xsl:if test="@required='required'">
	            		<xsl:attribute name="disabled"><xsl:text>disabled</xsl:text></xsl:attribute>
            		</xsl:if>
            		<xsl:attribute name="selected"><xsl:text>selected</xsl:text></xsl:attribute>

            		<xsl:value-of select="@title"/>
            	</option>
            	<xsl:apply-templates select="values/item" mode="dealer-form-field-item"/>
            </select>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="dealer-form-field-item">
    	<option value="{@id}"><xsl:value-of select="."/></option>
	</xsl:template>

	<xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="main-form">
		<form method="post" action="{$langPrefix}/webforms/send/" class="callback_block_form">
            <xsl:apply-templates select="items" mode="address" />
            <xsl:apply-templates select="groups/group" mode="main-form" />
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
			<input type="submit" value="Отправить заявку" class="button button__white"/>
			<div id="checkboxdivdiv">
				<div id="checkboxdiv">
					<!-- <input type="checkbox" name="checkboxG1" id="checkboxG1" class="css-checkbox" required="required" />
                    <label for="checkboxG1" class="css-label"><a href="/about/politika-obrabotki-personalnyh-dannyh/" target="_blank">Ознакомлен(а) и согласен(на) с политикой конфиденциальности</a></label> -->
					<input type="checkbox" id="checkbox" required="required"/>
					<label for="checkbox">Ознакомлен(а) и согласен(на) с <a href="/about/politika-obrabotki-personalnyh-dannyh/" target="_blank">политикой конфиденциальности</a></label>
				</div>
			</div>
        </form>
	</xsl:template>

    <!--_______________________________________________________Левая плашка________________________________________________________-->
    <xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="recall-webforms">
        <form method="post" action="{$langPrefix}/webforms/send/" class="form">
            <xsl:apply-templates select="items" mode="address" />
            <xsl:apply-templates select="groups/group" mode="simple" />
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
			<input type="submit" value="Перезвоните мне" class="button button__yellow"/>
        </form>
    </xsl:template>

        <!--_______________________________________________________плашка внизу страницы________________________________________________________-->
    <xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="q-webforms">
        <form method="post" action="{$langPrefix}/webforms/send/" class="form">
            <xsl:apply-templates select="items" mode="address" />
            <xsl:apply-templates select="groups/group" mode="simple-question" />
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
			<input type="submit" value="Получить консультацию" class="button button__yellow button__centered questions_button"/>
        </form>
    </xsl:template>

    <xsl:template match="group" mode="simple-question">
        <xsl:apply-templates select="field" mode="simple-question" />
    </xsl:template>

     <xsl:template match="field" mode="simple-question">
        <input type="text" name="{@input_name}" id="{@name}_field" placeholder="{@title}" class="questions_input">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="required">
                    <xsl:text>required</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@name = 'telefon'">
            	<xsl:attribute name="placeholder">
                    <xsl:text>+7 (___) ___-__-__</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="data-inputmask">
                    <xsl:text>'mask': '+7 (999)-999-99-99'</xsl:text>
                </xsl:attribute>
            </xsl:if>
        </input>
    </xsl:template>
	
	<xsl:template match="group" mode="main-form">
        <xsl:apply-templates select="field" mode="main-form" />
    </xsl:template>

	<xsl:template match="field" mode="main-form">
        <input type="text" name="{@input_name}" id="{@name}_field" placeholder="{@tip}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="required">
                    <xsl:text>required</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@name = 'phone'">
                <xsl:attribute name="data-inputmask">
                    <xsl:text>'mask': '+7 (999)-999-99-99'</xsl:text>
                </xsl:attribute>
            </xsl:if>
        </input>
    </xsl:template>	
	
	<xsl:template match="field[@type='relation']" mode="main-form">
		<select id="callback_select" size="1" name="{@input_name}">
			<option selected="selected" disabled="disabled" >Выберите регион</option>
			<xsl:apply-templates select="values/item" mode="main-form-val"/>
		</select>
	</xsl:template>

	<xsl:template match="item" mode="main-form-val">
            <option value="{@id}"><xsl:value-of select="."/></option>

	</xsl:template>

    <xsl:template match="group" mode="simple">
        <xsl:apply-templates select="field" mode="simple" />
    </xsl:template>

    <xsl:template match="field" mode="simple">
        <input type="text" name="{@input_name}" id="{@name}_field" placeholder="{@title}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="required">
                    <xsl:text>required</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@name = 'telefon'">
            	<xsl:attribute name="placeholder">
                    <xsl:text>+7 (___) ___-__-__</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="data-inputmask">
                    <xsl:text>'mask': '+7 (999)-999-99-99'</xsl:text>
                </xsl:attribute>
            </xsl:if>
        </input>
    </xsl:template>
    <!--_______________________________________________________Callbacl Form________________________________________________________-->

    <xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="callback">
        <form method="post" action="{$langPrefix}/webforms/send/" class="callback-form">
            <xsl:apply-templates select="items" mode="address" />
            <xsl:apply-templates select="groups/group" mode="webforms" />
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
            <input type="submit" value="&send;" class="send"/>
        </form>
    </xsl:template>

    <!--******************************Feedback hide form************************************-->
    <xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="feedback_hide">
        <div class="hide-popup mfp-hide" id="leave-bid">
            <h4>&submit-request;</h4>

            <form method="post" action="{$langPrefix}/webforms/send/" class="feedback-popup">
                <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
                <input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
                <xsl:apply-templates select="items" mode="address" />
                <xsl:apply-templates select="groups/group" mode="webforms-contacts" />
                <input type="submit" value="&send;" class="send"/>
            </form>

            <div class="close">
            </div>
        </div>
    </xsl:template>


    <!--_______________________________________________________________________________________________________________-->

	<xsl:template match="udata[@module = 'webforms'][@method = 'add']">
		<form method="post" action="{$langPrefix}/webforms/send/" onsubmit="site.forms.data.save(this); return site.forms.check(this);">
			<xsl:apply-templates select="items" mode="address" />
			<xsl:apply-templates select="groups/group" mode="webforms" />
			<input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
			<input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
			<div class="form_element">
				<xsl:apply-templates select="document('udata://system/captcha/')/udata" />
			</div>
			<div class="form_element">
				<input type="submit" class="button" value="Отправить" />
			</div>
		</form>
	</xsl:template>

	<xsl:template match="udata[@module = 'webforms'][@method = 'add']" mode="contacts">
		<form method="post" class="feedback" action="/webforms/send/" onsubmit="site.forms.data.save(this); return site.forms.check(this);">
			<input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
			<input type="hidden" name="ref_onsuccess" value="/webforms/posted/{/udata/@form_id}" />
			<xsl:apply-templates select="items" mode="address" />
			<xsl:apply-templates select="groups/group" mode="webforms-contacts" />
			<input type="submit" value="отправить" class="send"/>
		</form>
	</xsl:template>

	<xsl:template match="group" mode="webforms-contacts" >
		<xsl:apply-templates select="field" mode="webforms-contacts" />
	</xsl:template>

	<xsl:template match="field" mode="webforms-contacts">
		<label for="{@name}_field">
            <xsl:value-of select="@title"/>
            <xsl:apply-templates select="." mode="webforms_required-contacts" />
        </label>
		<xsl:apply-templates select="." mode="webforms_input_type" />
	</xsl:template>

	<xsl:template match="group" mode="webforms">
		<xsl:apply-templates select="field" mode="webforms" />
	</xsl:template>

	<xsl:template match="field" mode="webforms">
		<div class="form_element">
			<label>
				<xsl:apply-templates select="." mode="webforms_required" />
				<span><xsl:value-of select="@title" /><xsl:text>:</xsl:text></span>
				<xsl:apply-templates select="." mode="webforms_input_type" />
			</label>
		</div>
	</xsl:template>

	<xsl:template match="field" mode="webforms_input_type">
		<input type="text" name="{@input_name}" id="{@name}_field">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">
					<xsl:text>required</xsl:text>
				</xsl:attribute>
			</xsl:if>
		</input>
	</xsl:template>

	<xsl:template match="field[@type = 'text']" mode="webforms_input_type">
		<textarea name="{@input_name}" id="{@name}_field">
			<xsl:if test="@required = 'required'">
				<xsl:attribute name="required">
					<xsl:text>required</xsl:text>
				</xsl:attribute>
			</xsl:if>
		</textarea>
	</xsl:template>

	<xsl:template match="field[@type = 'relation']" mode="webforms_input_type">
		<select name="{@input_name}">
			<xsl:if test="@multiple">
				<xsl:attribute name="multiple">
					<xsl:text>multiple</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<option value=""></option>
			<xsl:apply-templates select="values/item" mode="webforms_input_type" />
		</select>
	</xsl:template>

	<xsl:template match="item" mode="webforms_input_type">
		<option value="{@id}"><xsl:apply-templates /></option>
	</xsl:template>

	<xsl:template match="field" mode="webforms_required" />

	<xsl:template match="field[@required = 'required']" mode="webforms_required">
		<xsl:attribute name="class">
			<xsl:text>required</xsl:text>
		</xsl:attribute>
	</xsl:template>
	<xsl:template match="field[@required = 'required']" mode="webforms_required-contacts">
		<span>*</span>
	</xsl:template>
	<xsl:template match="items" mode="address">
		<xsl:apply-templates select="item" mode="address" />
	</xsl:template>

	<xsl:template match="item" mode="address">
		<input type="hidden" name="system_email_to" value="{@id}" />
	</xsl:template>

	<xsl:template match="items[count(item) &gt; 1]" mode="address">
		<xsl:choose>
			<xsl:when test="count(item[@selected='selected']) != 1">
				<div class="form_element">
					<label class="required">
						<span><xsl:text>Кому отправить:</xsl:text></span>
						<select name="system_email_to">
							<option value=""></option>
							<xsl:apply-templates select="item" mode="address_select" />
						</select>
					</label>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="item[@selected='selected']" mode="address" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="item" mode="address_select">
		<option value="{@id}"><xsl:apply-templates /></option>
	</xsl:template>

</xsl:stylesheet>