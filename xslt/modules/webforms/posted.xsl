<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="xsl date udt xlink">

	<xsl:param name="template" />

	<xsl:template match="result[@module = 'webforms'][@method = 'posted']">
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <div class="pre_h">Дымоходы Вулкан</div>
                    <div class="middle_heading">
                        <div class="middle_heading_name"><xsl:value-of select="@header"/></div>
                    </div>
					<xsl:apply-templates select="document(concat('udata://webforms/posted/', $template,'/'))/udata" />
                </section>
            </div>
        </div>

		<div>
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'webforms'][@method = 'posted']">
		    <xsl:value-of select="." disable-output-escaping="yes" />
	</xsl:template>

</xsl:stylesheet>