<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM    "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                exclude-result-prefixes="xsl date udt umi">

<!-- родительская категория -->
    <xsl:template match="result[@module = 'faq'][@method = 'project'] | result[@module = 'faq'][@method = 'category']">
		

		<xsl:variable name="faq" select="document(concat('udata://faq/category/notemplate/',$pageId, '/////publish_time/desc?extProps=publish_time,nick'))/udata"/>

		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
            	<section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
	                    <div class="middle_heading">
	                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
	                    </div>
	                    <xsl:value-of select=".//property[@name='content']/value" disable-output-escaping="yes"/>
	                    <a class="button button__white ajax_popup" href="/faq/addQuestionForm/">задать вопрос</a>
					<xsl:apply-templates select="$errors"/>

                </section>
				
                <section class="faq_blocks">
					<xsl:apply-templates select="$faq/items/item" mode="faqCategory"/>
                    <div class="faq_gutter"></div>
                    <div class="faq_sizer"></div>
                </section>
            </div>

            <xsl:call-template name="numpages">
		        <xsl:with-param name="total" select="$faq/total" />
		        <xsl:with-param name="limit" select="$faq/per_page" />
		    </xsl:call-template>

           
        </div>     	
    </xsl:template>

	<xsl:template match="item" mode="faqCategory">
		<div class="faq_block" data-sort="{position()}">
            <div class="faq_block_q">            	
                <div class="faq_block_name"><b><xsl:value-of select=".//property[@name='nick']/value"/>, <xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='publish_time']/value/@unix-timestamp ,'/(d.m.Y)/udata'))"/></b></div>
                <p>
                	<!-- <a href="{@link}"> -->
                		<xsl:value-of select="question" disable-output-escaping="yes"/>
                	<!-- </a> -->
                </p>
            </div>
            <div class="faq_block_a">
                <div class="faq_block_name"><b>Ответ профессионала</b></div>
                <p><xsl:value-of select="answer" disable-output-escaping="yes"/></p>
            </div>
        </div>
	</xsl:template>




	<xsl:template match="udata[@module = 'faq'][@method = 'project']" mode="tags_block_faq">
		<xsl:param name="parentsLink" />
		<xsl:apply-templates select="lines/item" mode="faq_project_item">
			<xsl:with-param name="parentsLink" select="$parentsLink"/>
		</xsl:apply-templates>
	</xsl:template>

    <xsl:template match="lines/item" mode="faq_project_item">
		<xsl:param name="parentsLink" />

    	<a href="{@link}">
    		<xsl:attribute name="class">
    			<xsl:choose>
    				<xsl:when test="@link = $pageLink">
						<xsl:text>selected</xsl:text>
    				</xsl:when>
    				<xsl:when test="@link = $parentsLink">
						<xsl:text>selected</xsl:text>
    				</xsl:when>
    			</xsl:choose>
    		</xsl:attribute>
    		<xsl:value-of select="@name"/>
    	</a>
    </xsl:template>
	
	<xsl:template name="addQuestionForm">
    	<xsl:param name="pageCat" />
    	<xsl:param name="parentsLink" />

	    <div class="full_width full_width__lightgrey our_certs">
	    	<div class="big_heading big_heading_black">&add_question;</div>
				<form action="/faq/post_question" method="post" class="form_faq">
			        <input type="text" placeholder="Ваше имя" class="questions_input" name="nick" required="required"/>
			        <input type="text" placeholder="Тема вопроса" class="questions_input" name="title" required="required"/>
					<textarea name="question" class="questions_input" placeholder="Ваш вопрос" required="required"></textarea>
			        <select name="cat" class="questions_input" required="required" id="faq_cat_id">
			        	<option selected="selected" disabled="disabled">
			        		<xsl:text disable-output-escaping="yes">&select_question_cat;</xsl:text>
			        	</option>
			        	<xsl:apply-templates select="document(concat('udata://faq/project//', $pageCat, '/100/1'))" mode="faq_cat_option">
			        		<xsl:with-param name="parentsLink" select="$parentsLink"/>
			        	</xsl:apply-templates>
					</select>
			        <input type="submit" value="Задать вопрос" class="button button__yellow button__centered questions_button"/>

			    </form>
    	</div>
    </xsl:template>

    <xsl:template match="udata[@module = 'faq'][@method = 'project']" mode="faq_cat_option">
    	<xsl:param name="parentsLink" />
		<xsl:apply-templates select="lines/item" mode="faq_cat_option_item">
			<xsl:with-param name="parentsLink" select="$parentsLink"/>
		</xsl:apply-templates>
    </xsl:template>

    <xsl:template match="item" mode="faq_cat_option_item">
    	<xsl:param name="parentsLink" />
		<option value="{@id}">
			<xsl:choose>
				<xsl:when test="@link = $parentsLink">
					<xsl:attribute name="selected">
						<xsl:text>selected</xsl:text>
					</xsl:attribute>
				</xsl:when>
			</xsl:choose>
			<xsl:value-of select="@name"/>
		</option>
    </xsl:template>
    

</xsl:stylesheet>