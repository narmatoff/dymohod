<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM    "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                exclude-result-prefixes="xsl date udt umi">

    <xsl:template match="result[@module = 'faq'][@method = 'addQuestionForm']">
    	<div class="main_width where_buy good_wr_magnific_pos good_wr">
    		<section class="section_centered">
    			<article>
    				
    			
			<form class="validate_form" method="post" action="{udata/action}/5">
				<div class="fieldset_name2">Задать вопрос</div>

				<div>
					<input type="text" placeholder="Ваше имя" class="middle_form" name="nick" required="required"/>
				</div>
				<div>
					<input type="text" placeholder="Ваш email" class="middle_form" name="email" required="required"/>
				</div>
				<div>
					<input type="text" placeholder="Тема" class="middle_form" name="title" required="required"/>
				</div>
				<div>
					<textarea name="question" class="middle_form" placeholder="Ваш вопрос" required="required"></textarea>
				</div>
				
				<div>
					<input type="checkbox" name="checkbox" id="myText" required="required"/>
					<div id="adivdrl" onclick="$('#myText').trigger('click')">Ознакомлен(а) и согласен(на) с <a id="adlr" href="/about/politika-obrabotki-personalnyh-dannyh/" target="_blank">политикой конфиденциальности</a></div>
				</div>
				
				<input class="button button__white" type="submit" value="отправить"/>
			</form>		
    			</article>
    		</section>
    		
    	</div>
	</xsl:template>


					<!-- <input type="text" placeholder="Ваше имя" class="questions_input" name="nick" required="required"/>
			        <input type="text" placeholder="Тема вопроса" class="questions_input" name="title" required="required"/>
					<textarea name="question" class="questions_input" placeholder="Ваш вопрос" required="required"></textarea>
			        <select name="cat" class="questions_input" required="required" id="faq_cat_id">
			        	<option selected="selected" disabled="disabled">
			        		<xsl:text disable-output-escaping="yes">&select_question_cat;</xsl:text>
			        	</option>
			        	<xsl:apply-templates select="document(concat('udata://faq/project//', $pageCat, '/100/1'))" mode="faq_cat_option">
			        		<xsl:with-param name="parentsLink" select="$parentsLink"/>
			        	</xsl:apply-templates>
					</select>
			        <input type="submit" value="Задать вопрос" class="button button__yellow button__centered questions_button"/> -->
</xsl:stylesheet>