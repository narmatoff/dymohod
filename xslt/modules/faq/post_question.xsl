<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                exclude-result-prefixes="xsl date udt umi">

    <xsl:template match="result[@module = 'faq'][@method = 'post_question']">

		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                	<xsl:apply-templates select=".//property[@name='predzagolovok']" mode="pre_h"/>
                    <div class="middle_heading">
                        <div class="middle_heading_name">Вопрос успешно отправлен!</div>
                    </div>
                </section>
            </div>
        </div>



    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
            	<section class="section_centered">
		        	<xsl:apply-templates select="document('udata://faq/post_question')/udata"/>
            	</section>
            </div>
        </div>
	</xsl:template>

	<xsl:template match="udata[@module = 'faq'][@method = 'post_question']">
    		<xsl:value-of select="." disable-output-escaping="yes" />
	</xsl:template>


</xsl:stylesheet>