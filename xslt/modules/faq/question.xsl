<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                exclude-result-prefixes="xsl date udt umi">

    <xsl:template match="result[@module = 'faq'][@method = 'question']">
    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">


                <section class="faq_blocks">
                	<div class="faq_block one_page" data-sort="1">
			            <div class="faq_block_q">
			                <div class="faq_block_name"><b><xsl:value-of select=".//property[@name='nick']/value"/>, <xsl:value-of select="document(concat('udata://system/convertDate/', .//property[@name='publish_time']/value/@unix-timestamp ,'/(d.m.Y)/udata'))"/></b></div>
			                <p><xsl:value-of select=".//property[@name='question']/value" disable-output-escaping="yes"/></p>
			            </div>
			            <div class="faq_block_a">
			                <div class="faq_block_name"><b>Ответ профессионала</b></div>
			                <p><xsl:value-of select=".//property[@name='answer']/value" disable-output-escaping="yes"/></p>
			            </div>
			        </div>
			        <div class="faq_gutter"></div>
                    <div class="faq_sizer"></div>
                </section>

                <section class="section_centered">
                    <a class="button button__white ajax_popup" href="/faq/addQuestionForm/">задать вопрос</a>
                </section>
            </div>
        </div>




		
	</xsl:template>


</xsl:stylesheet>