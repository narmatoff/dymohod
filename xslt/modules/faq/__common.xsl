<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- <xsl:include href="projects-ajax.xsl"/> -->
    <xsl:include href="project.xsl"/>
    <!-- <xsl:include href="category.xsl"/> -->
	<xsl:include href="question.xsl" />
	<xsl:include href="addQuestionForm.xsl" />
	<xsl:include href="post_question.xsl" />

</xsl:stylesheet>