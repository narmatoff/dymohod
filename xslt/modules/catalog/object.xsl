<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <xsl:template match="result[@module = 'catalog'][@method = 'object']">
        <div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="header_withprint">
                    <h1><xsl:value-of select="//property[@name='h1']/value"/></h1><a class="print_link" href="/udata://catalog/getCommercePDF/{$pageId}">Версия для печати</a>
                </section>
                <div class="good_wr_page">
                    <div class="good_images">
                        <div class="swiper-container good_slider">
                            <div class="swiper-wrapper">
                                <xsl:apply-templates select="//property[@name='izobrazheniya']/value" mode="big-product-img" />
                            </div>
                        </div>
                        <div class="swiper-container good_thumbs">
                            <div class="swiper-wrapper">
                                <xsl:apply-templates select="//property[@name='izobrazheniya']/value" mode="small-product-img" />
                            </div>
                        </div>
                    </div>
                    <xsl:apply-templates select=".//group[@name='special']" mode="data-sheet" />
                    <div class="good_btns_page">
                        <a class="button button__black" href="/gde-kupit/">Где купить</a>
                        <!-- <span>Товар доступен в магазинах дилеров</span> -->
                    </div>
                </div>
                <div class="respTabs" id="responsiveTabsDemo">
                    <ul>
                        <li><a href="#tab-1">все параметры</a></li>
                        <li><a href="#tab-2">загрузки</a></li>
                        <li><a href="#tab-3">видео</a></li>
                    </ul>
                    <xsl:variable name="select_param">
                    	<xsl:choose>
                    		<xsl:when test=".//property[@name='diametr_mm']">
                    			<xsl:text>diametr_mm</xsl:text>
                    		</xsl:when>
                    		<xsl:otherwise>
                    			<xsl:text>wxb_mm</xsl:text>
                    		</xsl:otherwise>
                    	</xsl:choose>
                    </xsl:variable>
                    <div id="tab-1">
                        <table class="t_dotted_lines">

                        	<xsl:apply-templates select="document(concat('udata://catalog/getObjectParam/', $pageId, '/', $select_param))/udata/lines" mode="data-sheet-items2"/>

                            <!-- <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',$pageId,'///1?extProps=title,diametr_mm,marka_stali,marka_stali_vnutrennyaya,marka_stali_naruzhnaya,tolwina_stali_mm,weight,volume'))/udata/lines/item" mode="data-sheet-items2" /> -->
                            <!-- <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',$pageId,'///1?extProps=title,diametr_mm,marka_stali,marka_stali_vnutrennyaya,marka_stali_naruzhnaya,tolwina_stali_mm,weight,volume'))/udata/lines/item" mode="data-sheet-items4" /> -->
                        </table>
                    </div>
                    <div id="tab-2">
                        <table class="t_dotted_lines">
                            <xsl:apply-templates select="document('udata://filemanager/list_files/1509')/udata/items" mode="file" />
                        </table>
                    </div>
                    <div id="tab-3">
                        <div class="video_good_block">
                            <xsl:apply-templates select="document('udata://photoalbum/album/1587//3/1/izbrannye_video/true')//item" mode="video-item" />
                            <a href="/podderzhka/video-videoinstrukcii/" class="button button__black">Все видео</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="items" mode="file">
    	<xsl:variable name="items-download" select="document(concat('udata://filemanager/shared_file//', @id))/udata" />
        <tr>
            <td>
                <a href="{@link}">
                    <svg class="download_icon" xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17">
                        <path d="M14.1 16.14H1.05c-.58 0-1.05-.47-1.05-1.05v-4.63h2.1v3.57h10.95v-3.57h2.1v4.63c0 .58-.47 1.05-1.05 1.05zm-6.21-6.01c-.26.26-.51-.03-.51-.03L4.37 6.46s-.46-.43.04-.43h1.7v-.74V.73s-.07-.26.32-.26h2.39c.28 0 .27.22.27.22v5.23h1.57c.6 0 .15.45.15.45s-2.57 3.4-2.92 3.76z" fill="#2F2F2F"></path>
                    </svg>
                </a>
                &nbsp;
                <a href="{$items-download/download_link}">
                    <xsl:value-of select="$items-download/h1" />&nbsp;(версия: <xsl:value-of select="document(concat('udata://system/convertDate/', $items-download/versiya_fajla/@unix-timestamp ,'/(m.Y)/udata'))"/>)
                </a>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="group" mode="data-sheet">
        <div class="goog_info_page">
            <div class="goog_info_props">
                <xsl:apply-templates select="property[@name='diametr_mm']" mode="diametr-title" />
                <xsl:apply-templates select="property[@name='wxb_mm']" mode="diametr-title" />
                <xsl:apply-templates select="property[@name='marka_stali']" mode="diametr-title2" />
            </div>
            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',$pageId,'///1?extProps=diametr_mm,title,marka_stali,marka_stali_vnutrennyaya,marka_stali_naruzhnaya,tolwina_stali_mm,weight,volume,wxb_mm'))/udata/lines/item" mode="data-sheet-items" />
            <xsl:apply-templates select="document(concat('udata://catalog/getSmartCatalog//',$pageId,'///1?extProps=diametr_mm,title,marka_stali,marka_stali_vnutrennyaya,marka_stali_naruzhnaya,tolwina_stali_mm,weight,volume,wxb_mm'))/udata/lines/item" mode="data-sheet-items3" />
            <xsl:value-of select="//property[@name='opisanie']/value" disable-output-escaping="yes" />
            <div class="goodZoom"></div>
        </div>
    </xsl:template>
    <xsl:template match="item" mode="data-sheet-items">
        <div class="displaynone filteredarticul">
            <table class="t_dotted_lines">
                <xsl:apply-templates select="extended/properties/property" mode="data-sheet-item" />
            </table>
        </div>
    </xsl:template>
    <xsl:template match="property[@name='title']" mode="data-sheet-item">
        <h4 style="margin:0 20px 0 20px;"><br/><xsl:value-of select="value"/></h4>
    </xsl:template>
    <xsl:template match="property[@name='diametr_mm']" mode="data-sheet-item">
        <tr>
            <xsl:attribute name="class">
                <xsl:text>valuetr</xsl:text>
            </xsl:attribute>
            <td>
                <xsl:value-of select="title" />
            </td>
            <td class="valuetd">
                <xsl:choose>
                    <xsl:when test="value/item/@name">
                        <xsl:value-of select="value/item/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="value" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="property[@name='wxb_mm']" mode="data-sheet-item">
        <tr>
            <xsl:attribute name="class">
                <xsl:text>valuetr</xsl:text>
            </xsl:attribute>
            <td>
                <xsl:value-of select="title" />
            </td>
            <td class="valuetd">
                <xsl:choose>
                    <xsl:when test="value/item/@name">
                        <xsl:value-of select="value/item/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="value" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="property[@name='marka_stali']" mode="data-sheet-item">
        <tr>
            <xsl:attribute name="class">
                <xsl:text>valuetr</xsl:text>
            </xsl:attribute>
            <td>
                <xsl:value-of select="title" />
            </td>
            <td class="valuetdtolwina">
                <xsl:choose>
                    <xsl:when test="value/item/@name">
                        <xsl:value-of select="value/item/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="value" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="property" mode="data-sheet-item">
        <xsl:if test="title">
            <tr>
                <xsl:attribute name="class">
                    <xsl:choose>
                        <xsl:when test="(position() mod 2) != 1">
                            <xsl:text>even</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>odd</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <td>
                    <xsl:value-of select="title" />
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="value/item/@name">
                            <xsl:value-of select="value/item/@name" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="value" />
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </xsl:if>
    </xsl:template>
    <xsl:template match="property[@name='diametr_mm']" mode="diametr-title">
        <div class="prop_itempage diam_mm">
            <div class="prop_name">
                <xsl:value-of select="title" />
            </div>
            <div class="prop_variants">
                <xsl:apply-templates select="value/item" mode="diametr-item" >
                    <xsl:sort select="@id" order="ascending"/>
                </xsl:apply-templates>
            </div>
            <div style="clear: both;"></div>
        </div>
    </xsl:template>

    <xsl:template match="property[@name='wxb_mm']" mode="diametr-title">
        <div class="prop_itempage diam_mm">
            <div class="prop_name">
                <xsl:value-of select="title" />
            </div>
            <div class="prop_variants">
                <xsl:apply-templates select="value/item" mode="diametr-item" >
                    <xsl:sort select="@id" order="ascending"/>
                </xsl:apply-templates>
            </div>
            <div style="clear: both;"></div>
        </div>
    </xsl:template>
    <xsl:template match="item" mode="diametr-item">
        <input type="radio" name="{../../@name}" id="{@name}" style="display:none" />
        <label for="{@name}"><span><xsl:value-of select="@name"/></span></label>
    </xsl:template>
    <xsl:template match="property[@name='marka_stali']" mode="diametr-title2">
        <div class="prop_itempage tolwina_stali_mm" style="display:none">
            <div class="prop_name">
                <xsl:value-of select="title" />
            </div>
            <div class="prop_variants">
                <xsl:apply-templates select="value/item" mode="diametr-item" />
            </div>
            <div style="clear: both;"></div>
        </div>
    </xsl:template>
    <xsl:template match="item" mode="diametr-item2">
        <input type="radio" name="{../../@name}" id="{@name}" style="display:none" />
        <label for="{@name}"><span><xsl:value-of select="@name"/></span></label>
    </xsl:template>
    <xsl:template match="group" mode="special_group">
        <div class="goog_info_props">
            <xsl:apply-templates select="property" mode="special_group_property" />
        </div>
    </xsl:template>
    <xsl:template match="property[@is-important='1']" mode="special_group_property">
        <div class="prop">
            <div class="prop_name">
                <xsl:value-of select="title" />
            </div>
            <div class="prop_variants">
                <xsl:choose>
                    <xsl:when test="@type='relation'">
                        <xsl:apply-templates select="value/item" mode="special_group_property-relation" />
                    </xsl:when>
                    <xsl:otherwise>
                        <span>
                            <xsl:value-of select="value" disable-output-escaping="yes"/>
                        </span>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="property" mode="special_group_property" />
    <xsl:template match="item" mode="special_group_property-relation">
        <span>
            <xsl:value-of select="@name"/>
        </span>
    </xsl:template>
    <xsl:template match="property" mode="product-images">
        <div class="good_images">
            <div class="swiper-container good_slider">
                <div class="swiper-wrapper">
                    <xsl:apply-templates select="value" mode="big-product-img" />
                </div>
            </div>
            <div class="swiper-container good_thumbs">
                <div class="swiper-wrapper">
                    <xsl:apply-templates select="value" mode="small-product-img" />
                </div>
            </div>
        </div>
    </xsl:template>
    <!-- 320*320 -->
    <xsl:template match="value" mode="big-product-img">
        <div class="swiper-slide">
            <xsl:variable name="zoom" select="document(concat('udata://system/makeThumbnail/(', @path, ')/500/500'))/udata/src" />
            <xsl:call-template name="makeThumbnail">
                <xsl:with-param name="class" select="'swiper-lazy imgTrigger'" />
                <xsl:with-param name="pwd" select="@path" />
                <xsl:with-param name="element_id" select="@id" />
                <xsl:with-param name="data-src" select="." />
                <xsl:with-param name="data-zoom" select="$zoom" />
                <xsl:with-param name="width" select="'320'" />
                <xsl:with-param name="height" select="'320'" />
            </xsl:call-template>
            <div class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
        </div>
    </xsl:template>
    <!-- 320*320 -->
    <xsl:template match="value" mode="small-product-img">
        <div class="swiper-slide">
            <xsl:call-template name="makeThumbnail">
                <xsl:with-param name="pwd" select="@path" />
                <xsl:with-param name="element_id" select="@id" />
                <xsl:with-param name="width" select="'70'" />
                <xsl:with-param name="height" select="'70'" />
            </xsl:call-template>
        </div>
    </xsl:template>
	
	<xsl:template match="lines" mode="data-sheet-items2">
		<xsl:apply-templates select="item" mode="data-sheet-item2"/>
	</xsl:template>

	<xsl:template match="item" mode="data-sheet-item2">
		<tr>
			<td><xsl:value-of select="@name"/></td>
			<xsl:apply-templates select="items/item" mode="data-sheet-item2-val"/>
		</tr>
	</xsl:template>
	<xsl:template match="item" mode="data-sheet-item2-val">
		<td><xsl:value-of select="@value"/></td>
	</xsl:template>
    <!-- <xsl:template match="item" mode="data-sheet-items2">
        <tr>
            <xsl:apply-templates select="extended/properties/property" mode="data-sheet-item2" />
        </tr>
    </xsl:template>
    <xsl:template match="property[@name='title']" mode="data-sheet-item2">
        <td>
            <xsl:value-of select="value" />
        </td>
    </xsl:template>
    <xsl:template match="property" mode="data-sheet-item2">
        <td>
            <xsl:value-of select="title" />
        </td>
        <td>
            <xsl:choose>
                <xsl:when test="value/item/@name">
                    <xsl:value-of select="value/item/@name" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="value" />
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </xsl:template> -->

    <xsl:template match="item" mode="data-sheet-items3">
        <div class="displaynone filteredarticul">
            <table class="t_dotted_lines">
                <xsl:apply-templates select="extended/properties/property" mode="data-sheet-item3" />
            </table>
        </div>
    </xsl:template>
    <xsl:template match="property[@name='title']" mode="data-sheet-item3">
        <h4 style="margin:0 20px 0 20px;"><br/><xsl:value-of select="value"/></h4>
    </xsl:template>
    <xsl:template match="property[@name='diametr_mm']" mode="data-sheet-item3">
        <tr>
            <xsl:attribute name="class">
                <xsl:text>valuetr</xsl:text>
            </xsl:attribute>
            <td>
                <xsl:value-of select="title" />
            </td>
            <td class="valuetd">
                <xsl:choose>
                    <xsl:when test="value/item/@name">
                        <xsl:value-of select="value/item/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="value" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="property[@name='marka_stali']" mode="data-sheet-item3">
        <tr>
            <xsl:attribute name="class">
                <xsl:text>valuetr</xsl:text>
            </xsl:attribute>
            <td>
                <xsl:value-of select="title" />
            </td>
            <td class="valuetdtolwina">
                <xsl:choose>
                    <xsl:when test="value/item[2]/@name">
                        <xsl:value-of select="value/item[2]/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="value" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="property" mode="data-sheet-item3">
        <xsl:if test="title">
            <tr>
                <xsl:attribute name="class">
                    <xsl:choose>
                        <xsl:when test="(position() mod 2) != 1">
                            <xsl:text>even</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>odd</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <td>
                    <xsl:value-of select="title" />
                </td>
                <td>
                    <xsl:choose>
                        <xsl:when test="value/item/@name">
                            <xsl:value-of select="value/item/@name" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="value" />
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </xsl:if>
    </xsl:template>
    <xsl:template match="item" mode="data-sheet-items4">
        <tr>
            <xsl:apply-templates select="extended/properties/property" mode="data-sheet-item4" />
        </tr>
    </xsl:template>
    <xsl:template match="property[@name='title']" mode="data-sheet-item4">
        <td>
            <xsl:value-of select="value" />
        </td>
    </xsl:template>
    <xsl:template match="property[@name='marka_stali']" mode="data-sheet-item4">
        <td>
            <xsl:value-of select="title" />
        </td>
        <td>
            <xsl:choose>
                <xsl:when test="value/item[2]/@name">
                    <xsl:value-of select="value/item[2]/@name" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="value" />
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </xsl:template>
    <xsl:template match="property" mode="data-sheet-item4">
        <td>
            <xsl:value-of select="title" />
        </td>
        <td>
            <xsl:choose>
                <xsl:when test="value/item/@name">
                    <xsl:value-of select="value/item/@name" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="value" />
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </xsl:template>
</xsl:stylesheet>
