<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="udata[@module = 'catalog'][@method = 'getCategoryList']" mode="getCategoryList_main">
        <div class="catalog_block">		
			<xsl:apply-templates select=".//items/item" mode="getCategoryList_main-item"/>
			<div class="gutter_catalog"></div>
            <div class="catalog_sizer"></div>
		</div>
	</xsl:template>
	
	<xsl:template match="item" mode="getCategoryList_main-item">
		<div>
			<xsl:attribute name="class">
				<xsl:text>catalog_block_item</xsl:text>
				<xsl:if test=".//property[@name='bolshoj_blok']">
					<xsl:text> catalog_block_item__long</xsl:text>
				</xsl:if>
			</xsl:attribute>

			<xsl:variable name="w">
				<xsl:choose>
					<xsl:when test=".//property[@name='bolshoj_blok']">701</xsl:when>
					<xsl:otherwise>338</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<a href="{@link}">
			<xsl:call-template name="makeThumbnail">
				<!--<xsl:with-param name="alt" select="@alt"/>-->
				<xsl:with-param name="pwd" select=".//property[@name='menu_pic_ua']/value/@path"/>
				<xsl:with-param name="element_id" select="@id"/>
				<xsl:with-param name="width" select="$w"/>
				<xsl:with-param name="height" select="'309'"/>
			</xsl:call-template>
            <div class="catalog_block_item_content">
                <div class="catalog_block_item_name"><xsl:value-of select=".//property[@name='pervaya_stroka_v_menyu']/value"/></div>
                <div class="catalog_block_item_descr"><xsl:value-of select=".//property[@name='vtoraya_stroka_v_menyu']/value"/></div><!-- <a class="button button__circled" href="{@link}"></a> -->
            </div>
            </a>
        </div>
	</xsl:template>

</xsl:stylesheet>