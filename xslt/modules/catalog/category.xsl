<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <!--******************************Главная категория************************************-->
    <xsl:template match="result[@module = 'catalog'][@method = 'category'][page/@id='&catMainId;']" priority="1">
		
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_centered">
                    <div class="pre_h"><xsl:value-of select=".//property[@name='predzagolovok']/value"/></div>
                    <div class="middle_heading">
                        <h1 class="middle_heading_name"><xsl:value-of select=".//property[@name='h1']/value"/></h1>
                    </div>
                </section>
            </div>
        </div>

        
        <div class="full_width full_width__light">
            <div class="main_width">
                <div class="section ctalog_widgets">
                    <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList//', &catMainId;, '/100/1/0?extProps=menu_pic_ua,pervaya_stroka_v_menyu,vtoraya_stroka_v_menyu,bolshoj_blok'))/udata" mode="getCategoryList_main"/>
                    
                </div>
            </div>
        </div>
        
    	<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
                    	<xsl:apply-templates select="document(concat('udata://filemanager/shared_file//', .//property['ssylka_na_fajl_kataloga']/value/page/@id))/udata" mode="shared_file"/>
                        
	          			
	          			<xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes" />
                        
                    </article>
                   
                </section>
            </div>
        </div>

        <div class="main_width">
			<div class="swiper_block cert_slider section">
				<div class="header_block">
					<div class="header_block_descr">наши</div>
					<div class="header_block_name">сертификаты</div>
					<div class="swiper_block_slider_nav">
				    	<div class="swiper_block_slider_nav_prev"></div>
				    	<div class="swiper_block_slider_nav_next"></div>
				  </div>
                  <a href="/podderzhka/sertifikaty/" class="button button__yellow_d button__yellow_d_darkfont sliderbtn_pos">Все сертификаты</a>
				</div>
				<xsl:apply-templates select="document('udata://photoalbum/album/8//20/1')" mode="sert-not_main"/>
			</div>
		</div>
		
		<div class="full_width full_width__light">
            <div class="main_width main_width__white">
                <section class="section_onefortwocols spoilered">
                    <article>
                    	<xsl:value-of select=".//property[@name='seotekst']/value" disable-output-escaping="yes"/>
                    </article>
                </section>
            </div>
        </div>
		
        <!-- <xsl:call-template name="numpages">
            <xsl:with-param name="total" select="$catalog-items/total" />
            <xsl:with-param name="limit" select="25" />
        </xsl:call-template> -->
    </xsl:template>

    <xsl:template match="udata[@module = 'catalog'][@method = 'getCategoryList']" mode="subcategory-menu-top">
		<xsl:apply-templates select="items" mode="subcategory-menu-top_items"/>
    </xsl:template>

    <xsl:template match="items" mode="subcategory-menu-top_items">
    	<div class="category">
			<xsl:apply-templates select="item" mode="subcategory-menu-top_items_item"/>
		</div>
    </xsl:template>

    <xsl:template match="item" mode="subcategory-menu-top_items_item">
    	<a href="{@link}">
        	<!-- <img src="img/catitem1.png" alt=""/> -->
    		<xsl:attribute name="class">
    			<xsl:text>category_item</xsl:text>
    			<xsl:if test="@link = $pageLink">
					<xsl:text> category_item_selected</xsl:text>
    			</xsl:if>
    		</xsl:attribute>
    		<xsl:call-template name="makeThumbnail">
				<!--<xsl:with-param name="alt" select="@alt"/>-->
				<xsl:with-param name="pwd" select=".//property[@name='menu_pic_a']/value/@path"/>
				<xsl:with-param name="element_id" select="@id"/>
				<xsl:with-param name="width" select="'217'"/>
				<xsl:with-param name="height" select="'150'"/>
			</xsl:call-template>
    		 <div class="category_item_name">
    		 	<xsl:apply-templates select=".//property[@name='pervaya_stroka_v_menyu']"/>
    			<xsl:apply-templates select=".//property[@name='vtoraya_stroka_v_menyu']"/>
    		</div>
    	</a>
    </xsl:template>

    <xsl:template match="property[@name='pervaya_stroka_v_menyu']">
       <xsl:value-of select="value"/>
    </xsl:template>

    <xsl:template match="property[@name='vtoraya_stroka_v_menyu']">
        <div class="category_item_descr"><xsl:value-of select="value"/></div>
    </xsl:template>


    <xsl:template match="result[@module = 'catalog'][@method = 'category']">
		
	<div class="full_width full_width__light">
	    <div class="main_width main_width__white">
	        <section class="section_centered">
	            <div class="pre_h">
	            	<xsl:value-of select=".//property[@name='predzagolovok']/value"/>
	            </div>
	            <div class="middle_heading">
	                <h1 class="middle_heading_name">
	                    <xsl:value-of select=".//property[@name='h1']/value"/>
	                </h1>
	            </div>
	        </section>
            <a href="#goodzz" class="middle_next anchor">
                <img src="{$template-resources}dist/img/svg/circled_arrow_bottom.svg" alt="перейти к товарам" title="перейти к товарам"/>
            </a>
            <section class="section_onefortwocols spoilered">
                <article>
                    <xsl:value-of select=".//property[@name='descr']/value" disable-output-escaping="yes"/>
                </article>
            </section>



	    </div>
		
		<xsl:variable name="catMenuPage">
			<xsl:choose>
				<xsl:when test="parents/page[2]">
					<xsl:value-of select="parents/page[2]/@id"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$pageId"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
        <xsl:apply-templates select="document(concat('udata://content/menu///', $catMenuPage))" mode="middle_nav" />

	</div>

<!--     <div class="full_width full_width__light">
        <div class="main_width main_width__white">
            <a href="#goodzz" class="anchor middle_next">
                <img src="{$template-resources}dist/img/svg/circled_arrow_bottom.svg" alt=""/>
            </a>
        </div>
    </div> -->
	
	<xsl:variable name="catalog-items" select="document(concat('udata://catalog/getSmartCatalog/notemplate/',$pageId,'///2/h1/asc/1/?extProps=name,opisanie&amp;extGroups=opisanie,special'))/udata"/>

	<div class="full_width full_width__light">
		<div class="main_width main_width__white">
	        <div id="goodzz" class="goods">
				<xsl:apply-templates select="$catalog-items" mode="getSmartCatalog"/>
			</div>
		
		    <xsl:call-template name="numpages">
		    	<xsl:with-param name="requesurl" select="concat('/udata://catalog/getSmartCatalog//', $pageId, '///2/?extProps=name&amp;extGroups=opisanie')" />
				<xsl:with-param name="template" select="'layouts/default.xsl'" />

		        <xsl:with-param name="total" select="$catalog-items/total" />
		        <xsl:with-param name="limit" select="$catalog-items/per_page" />
		    </xsl:call-template>
		</div>
    </div>
	
	</xsl:template>
		



</xsl:stylesheet>