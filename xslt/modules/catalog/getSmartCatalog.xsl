<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <xsl:template match="udata[@module = 'catalog'][@method = 'getSmartCatalog']" mode="getSmartCatalog">
        <xsl:apply-templates select="lines" mode="getSmartCatalog_lines" />
    </xsl:template>
    <xsl:template match="lines" mode="getSmartCatalog_lines">
        <xsl:apply-templates select="item" mode="getSmartCatalog_lines_item" />
    </xsl:template>
    <xsl:template match="item" mode="getSmartCatalog_lines_item">
        <div class="goods_item">
            <a href="#{@id}" class="goods_item_img popup-modal">
                <xsl:call-template name="makeThumbnail">
                    <xsl:with-param name="alt" select="@name" />
                    <xsl:with-param name="class" select="'goods_item_img_this'" />
                    <xsl:with-param name="pwd" select=".//property[@name='izobrazheniya']/value/@path" />
                    <xsl:with-param name="element_id" select="@id" />
                    <xsl:with-param name="width" select="'265'" />
                    <xsl:with-param name="height" select="'265'" />
                </xsl:call-template>
                <xsl:apply-templates select=".//property[@name='novinka']/value" mode="novinka" />
            </a>
            <div class="goods_item_descr">
                <div class="goods_item_descr_lil">
                    <xsl:value-of select="@name" />
                </div>
                <div class="goods_item_descr_more">
                    <xsl:value-of select=".//property[@name='kratkoe_opisanie']/value" />
                </div>
            </div><a href="{@link}" class="button button__black goods_item_btn">подробнее</a>
        </div>
        <div id="{@id}">
            <xsl:attribute name="class">
                <xsl:text>good_wr mfp-hide</xsl:text>
                <xsl:if test="$isAjax = 'yes'">
                    <xsl:text> good_wr_magnific_pos</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <xsl:apply-templates select=".//property[@name='izobrazheniya']" mode="product-images" />
            <div class="goog_info">
                <div class="middle_heading_name"><xsl:value-of select="@name" /></div>
                <!-- <div class="articul">Артикул <xsl:value-of select=".//property[@name='h1']/value"/></div> -->
                <xsl:value-of select=".//property[@name='kratkoe_opisanie']/value" disable-output-escaping="yes" />
                <xsl:apply-templates select=".//group[@name='special']" mode="special_group" />
                <div>
                    <xsl:attribute name="class">
                        <xsl:text>good_btns</xsl:text>
                        <xsl:if test="$isAjax != 'yes'">
                            <xsl:text> video_row_item_descr</xsl:text>
                        </xsl:if>
                    </xsl:attribute>
                    <a href="/gde-kupit/" class="button button__black">Где купить</a>
                    <a href="{@link}" class="button button__yellow_d button__yellow_d_darkfont">Смотреть подробнее</a>
                </div>
                <div class="goodZoom"></div>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="value" mode="novinka">
        <img src="{$template-resources}dist/img/svg/new.svg" alt="" class="goods_item_img_new" />
    </xsl:template>
</xsl:stylesheet>
