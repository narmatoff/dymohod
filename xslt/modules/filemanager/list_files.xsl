<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="udata[@module = 'filemanager'][@method = 'list_files']">
		<section class="section_onefortwocols spoilered">
            <article>
            	<xsl:apply-templates select="items" mode="list_files-items"/>
            </article>
        </section>
	</xsl:template>

	<xsl:template match="items" mode="list_files-items">
		<xsl:apply-templates select="document(concat('udata://filemanager/shared_file//', @id))/udata" mode="shared_file"/>
	</xsl:template>
	

</xsl:stylesheet>