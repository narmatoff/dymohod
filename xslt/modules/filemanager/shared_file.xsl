<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="udata[@module = 'filemanager'][@method = 'shared_file']" mode="shared_file">
		<div class="view_object">
			<a class="view_object_image bigpic" href="{menu_pic_a}">
				<xsl:call-template name="makeThumbnail">
					<xsl:with-param name="alt" select="h1"/>
					<xsl:with-param name="pwd" select="menu_pic_a/@path"/>
					<xsl:with-param name="element_id" select="@id"/>
					<xsl:with-param name="width" select="'auto'"/>
					<xsl:with-param name="height" select="'374'"/>
				</xsl:call-template>
			</a>
	        <div class="view_object_descr">
	            <h2>
	            	<a href="{download_link}">
	            		<xsl:value-of select="h1"/>
	            		<span>(версия: <xsl:value-of select="document(concat('udata://system/convertDate/', versiya_fajla/@unix-timestamp ,'/(m.Y)/udata'))"/>)</span>
	            	</a>
	            </h2>
	            	<xsl:value-of select="descr" disable-output-escaping="yes" />
	            <a class="button button__yellow_d button__yellow_d_darkfont" href="{download_link}">
	                Скачать <xsl:value-of select="ext"/>&nbsp;<svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" viewBox="0 0 16 17"><path d="M14.1 16.14H1.05c-.58 0-1.05-.47-1.05-1.05v-4.63h2.1v3.57h10.95v-3.57h2.1v4.63c0 .58-.47 1.05-1.05 1.05zm-6.21-6.01c-.26.26-.51-.03-.51-.03L4.37 6.46s-.46-.43.04-.43h1.7v-.74V.73s-.07-.26.32-.26h2.39c.28 0 .27.22.27.22v5.23h1.57c.6 0 .15.45.15.45s-2.57 3.4-2.92 3.76z" fill="#2F2F2F"/></svg></a>
	        </div>
	    </div>
	</xsl:template>
</xsl:stylesheet>