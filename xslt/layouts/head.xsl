<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template name="head">
		<meta charset="UTF-8"/>
		<xsl:choose>
			<xsl:when test="result/@method = 'object'">
			    <title><xsl:value-of select="//property[@name='h1']/value"/>: купить, цена, харатеристики, отзывы.</title>
			    <meta name="description" content="Выгодно купить {//property[@name='h1']/value}. Наша продукция выского качества и по доступным ценам. Звоните!"/>
			    <meta name="keywords" content="{//property[@name='h1']/value}, {//property[@name='h1']/value} цена, {//property[@name='h1']/value} гост, {//property[@name='h1']/value} куплю, {//property[@name='h1']/value} характеристики, {//property[@name='h1']/value} вес, {//property[@name='h1']/value} размеры, {//property[@name='h1']/value} производство, {//property[@name='h1']/value} прайс"/>
			</xsl:when>
			<xsl:otherwise>
			    <title><xsl:value-of select="result/@title"/></title>
			    <meta name="description" content="{result/meta/description}"/>
			    <meta name="keywords" content="{result/meta/keywords}"/>
			</xsl:otherwise>
		</xsl:choose>
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1"/>
	    <meta name="author" content="lum.ru"/>

        <meta name="image" content="{$template-resources}dist/img/logo.png"/>
        <meta itemprop="name" content="{result/@title}"/>
        <meta itemprop="image" content="{$template-resources}dist/img/logo.png"/>
        <meta name="og:title" content="{result/@title}"/>
        <meta name="og:description" content="{result/meta/description}"/>
        <meta name="og:image" content="{$template-resources}dist/img/logo.png"/>
        <meta name="og:url" content="http://vulkan.ru"/>
        <meta name="og:site_name" content="Дымоходы Вулкан"/>
        <meta name="og:locale" content="ru_RU"/>
        <meta name="og:video" content="Video.avi"/>
        <meta name="og:type" content="website"/>

        <!-- <meta name="fb:admins" content="#facebookadminid"/> -->
        <!-- <meta name="fb:app_id" content="facebookAppID"/> -->
        
        <script src="https://cdn.jsdelivr.net/pace/1.0.2/pace.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/pace/1.0.2/themes/yellow/pace-theme-flash.css"/>

        <link rel="apple-touch-icon" sizes="180x180" href="{$template-resources}dist/img/apple-touch-icon.png"/>
        <link rel="icon" href="{$template-resources}dist/img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="{$template-resources}dist/img/favicon.ico" type="image/x-icon"/>
        <link rel="icon" type="image/png" sizes="32x32" href="{$template-resources}dist/img/favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="{$template-resources}dist/img/favicon-16x16.png"/>
		<link rel="manifest" href="{$template-resources}dist/img/manifest.json"/>
		<link rel="mask-icon" href="{$template-resources}dist/img/safari-pinned-tab.svg" color="#5bbad5"/>
		<meta name="msapplication-TileColor" content="#b91d47"/>
		<meta name="msapplication-TileImage" content="{$template-resources}dist/img/mstile-144x144.png"/>
		<meta name="theme-color" content="#ffffff"/>




	    <xsl:if test="result/@pageId">
		   <xsl:variable name="relCanonical" select="document(concat('udata://seo/getRelCanonical/void/', result/@pageId))/udata/@link" />
		   <xsl:if test="$relCanonical">
		      <link rel="canonical">
		         <xsl:attribute name="href">
		            <xsl:value-of select="$relCanonical" />
		         </xsl:attribute>
		      </link>
		   </xsl:if>
		</xsl:if>



        <!-- <link rel="stylesheet" href="{$template-resources}dist/components/swiper/dist/css/swiper.css"/> -->
        <!-- <link rel="stylesheet" href="{$template-resources}dist/components/animate.css/animate.css"/> -->
        <!-- <link rel="stylesheet" href="{$template-resources}dist/components/magnific-popup/dist/magnific-popup.css"/> -->
        <!-- <link rel="stylesheet" href="{$template-resources}dist/components/responsive-tabs/css/responsive-tabs.css"/> -->



        <link rel="stylesheet" href="{$template-resources}dist/components/drift/dist/drift-basic.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/g/jquery.magnific-popup@1.0.0(magnific-popup.css),jquery.responsive-tabs@1.6.1(responsive-tabs.css),swiper@3.4.2(swiper.min.css),animatecss@3.5.2"/>
        <link rel="stylesheet" href="{$template-resources}dist/css/main.css?v=1.1"/>


        <xsl:if test="result/page/properties/group[@name = 'more_params']/property[@name = 'robots_deny']/value">
            <meta name="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"/>
        </xsl:if>

        <!-- styles -->
		
    </xsl:template>

</xsl:stylesheet>