<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <xsl:output encoding="utf-8" method="html" indent="yes" />
    <xsl:param name="template-name" />
    <xsl:param name="template-resources" />
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="$isAjax = 'yes'">
                <xsl:apply-templates select="result" />
                <xsl:apply-templates select="udata" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html/&gt;</xsl:text>
                <html lang="ru">

                <head>
                    <xsl:call-template name="head" />
                </head>

                <body id="fullpage">
                    <div class="handhelds_topbar">
                        <xsl:if test="$isDefault">
                            <xsl:attribute name="class">handhelds_topbar handhelds_topbar__hide</xsl:attribute>
                        </xsl:if>
                        <!--                         <xsl:if test="not($isDefault = 1)">
                            <xsl:attribute name="class">
                                <xsl:text>handhelds_topbar</xsl:text>
                                <xsl:choose>
                                    <xsl:when test="$isDefault = 1">
                                        <xsl:text></xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>handhelds_topbar</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                        </xsl:if> -->
                        <!-- блок-меню для карманных устройств -->
                        <div class="handhelds_topbar">
                            <a href="/" class="small_logo">
                                <img src="/templates/dymohod/dist/img/svg/mini_logo.svg" alt="" />
                            </a>
                            <div class="tel">
                                <a href="tel:{concat(8, $siteInfoPage//property[@name='prefiks_telefona']/value, $siteInfoPage//property[@name='telefon_v_shapke']/value)}">
                                    <span>
                                <xsl:value-of select="$siteInfoPage//property[@name='prefiks_telefona']/value"/>
                            </span>&nbsp;
                                    <xsl:value-of select="$siteInfoPage//property[@name='telefon_v_shapke']/value" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <header class="">
                        <xsl:attribute name="class">
                            <xsl:text>section header </xsl:text>
                            <xsl:choose>
                                <xsl:when test="$isDefault = 1">
                                    <xsl:text>onepage</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>header_notmain</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:if test="$isDefault">
                            <!--                        <video autoplay="autoplay" muted="muted" loop="loop" poster="{$template-resources}dist/img/PRESENTATION.png">
                        </video> -->
                            <video autoplay="autoplay" muted="muted" loop="loop" poster="{result/page//property[@name='izobrazhenie_vmesto_video']/value/@path}">
                                <!-- <xsl:apply-templates select="result/page//property[@name='video_v_shapke']" mode="video_v_shapke"/> -->
                                <xsl:apply-templates select="//group[@name='videogruppa']" mode="video_v_shapke" />
                            </video>
                            <!--TODO: расскомментить для видео и превоьюшки из админки-->
                            <!--<video autoplay="autoplay" muted="muted" loop="loop" poster="{.//property[@name='izobrazhenie']/value}">    <source src="{.//property[@name='video']/value}" type="video/{.//property[@name='video']/value/@ext}"/>-->
                            <!--</video>-->
                        </xsl:if>
                        <div class="header_filter"></div>
                        <div class="top_line">
                            <div class="tel">
                                <a href="tel:{concat(8, $siteInfoPage//property[@name='prefiks_telefona']/value, $siteInfoPage//property[@name='telefon_v_shapke']/value)}">
                                    <span>
                                <xsl:value-of select="$siteInfoPage//property[@name='prefiks_telefona']/value"/>
                            </span>&nbsp;
                                    <xsl:value-of select="$siteInfoPage//property[@name='telefon_v_shapke']/value" />
                                </a>
                            </div>
                            <div id="menu_logo" class="menu_logo">
                                <xsl:apply-templates select="document('udata://menu/draw/top_left')" mode="menu-top" />
                                <xsl:if test="not($isDefault)">
                                    <a href="/" class="small_logo">
                                        <xsl:call-template name="makeThumbnail">
                                            <!--<xsl:with-param name="alt" select="@alt"/>-->
                                            <xsl:with-param name="pwd" select="$siteInfoPage//property[@name='logotip']/value/@path" />
                                            <xsl:with-param name="element_id" select="@id" />
                                            <xsl:with-param name="width" select="'68'" />
                                            <xsl:with-param name="height" select="'56'" />
                                        </xsl:call-template>
                                    </a>
                                </xsl:if>
                                <xsl:apply-templates select="document('udata://menu/draw/top_right')" mode="menu-top" />
                            </div>
                            <!--TODO: вывод модального окошка-->
                            <a href="{document('upage://1355')/udata/page/@link}" class="button button__yellow_d button__yellow_d_lightfont header_button_margin">стать дилером</a>
                        </div>
                        <xsl:if test="$isDefault = 1">
                            <div class="h_content">
                                <a href="/" class="main_logo">
                                    <xsl:call-template name="makeThumbnail">
                                        <!--<xsl:with-param name="alt" select="@alt"/>-->
                                        <xsl:with-param name="pwd" select="$siteInfoPage//property[@name='logotip']/value/@path" />
                                        <xsl:with-param name="element_id" select="@id" />
                                        <xsl:with-param name="width" select="'282'" />
                                        <xsl:with-param name="height" select="'228'" />
                                    </xsl:call-template>
                                </a>
                                <div id="slogan" class="slogan">
                                    <xsl:value-of select="$siteInfoPage//property[@name='slogan_na_glavnoj']/value" />
                                </div>
                                <a class="button button__white anchor" href="#contact">связаться с нами</a>
                            </div>
                            <xsl:apply-templates select="document('udata://content/getList//0/1//1///144?extProps=h1,menu_pic_ua,kratkoe_opisanie')/udata" mode="main_for_what" />
                        </xsl:if>
                        <div class="handhelds_menu"></div>
                    </header>
                    <!-- проблема главной -->
                    <!-- <xsl:apply-templates select="$errors"/> -->
                    <xsl:if test="not($isDefault)">
                        <div class="full_width full_width__light menu_fix">
                            <xsl:call-template name="navi" />
                            <xsl:choose>
                                <xsl:when test="$pageId = &catMainId;"></xsl:when>
                                <xsl:when test="$module = 'catalog'">
                                    <div class="middle_nav middle_nav_centered main_width">
                                        <xsl:variable name="catForSubitems">
                                            <xsl:choose>
                                                <xsl:when test="count(result/parents/page) = 1">
                                                    <xsl:value-of select="result/parents/page/@id" />
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="result/parents/page[1]/@id" />
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:apply-templates select="document(concat('udata://catalog/getCategoryList/notemplate/',$catForSubitems,'/100/1?extProps=menu_pic_a,pervaya_stroka_v_menyu,vtoraya_stroka_v_menyu'))/udata" mode="subcategory-menu-top" />
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:apply-templates select="document(concat('udata://content/menu///', $rootPageId))" mode="middle_nav" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </xsl:if>
                    <xsl:apply-templates select="result" />
                    <xsl:choose>
                        <xsl:when test="$pageId = &catMainId;"></xsl:when>
                        <xsl:when test="$pageId = &sertsId;"></xsl:when>
                        <xsl:when test="$pageId = &podderzhkaId;"></xsl:when>
                        <xsl:when test="$typeId = &sertsypeId;"></xsl:when>
                        <xsl:otherwise>
                            <div class="main_width">
                                <div class="swiper_block cert_slider section">
                                    <div class="header_block">
                                        <div class="header_block_descr">наши</div>
                                        <div class="header_block_name">сертификаты</div>
                                        <div class="swiper_block_slider_nav">
                                            <div class="swiper_block_slider_nav_prev"></div>
                                            <div class="swiper_block_slider_nav_next"></div>
                                        </div>
                                        <a href="/podderzhka/sertifikaty/" class="button button__yellow_d button__yellow_d_darkfont sliderbtn_pos">Все сертификаты</a>
                                    </div>
                                    <xsl:apply-templates select="document('udata://photoalbum/album/8//20/1')" mode="sert-not_main" />
                                </div>
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>
                    <!--                <div class="full_width full_width__light section">
                    <div id="where_buy" class="main_width where_buy">
                        <div class="middle_heading middle_heading__dark">
                            <div class="middle_heading_descr">Наши</div>
                            <div class="middle_heading_name">Партнеры</div>
                        </div>
                    </div>
                    <div class="logo_list main_width">
                        <xsl:apply-templates select="$siteInfoPage//property[@name='nashi_partnery']/value" mode="nashi_partnery"/>
                    </div>
                </div> -->
                    <div class="full_width section" id="contact">
                        <div class="callback_block dym_back">
                            <div class="middle_heading middle_heading__light" id="middle_heading_mini_logo"><img src="{$template-resources}dist/img/svg/mini_logo.svg" alt="" class="middle_heading_mini_logo" />
                                <div class="middle_heading_descr">возникли вопросы?</div>
                                <div class="middle_heading_name">мы вам перезвоним</div>
                                <div class="middle_heading_text">Только по вопросам промышленного и многоквартирного строительства!<br/><br/>
                                Для консультаций частного порядка воспользуйтесь формой в разделе <a href="/podderzhka/vopros-otvet/">Вопрос-Ответ</a></div>
                            </div>
                            <xsl:apply-templates select="document('udata://webforms/add/127')" mode="main-form" />
                        </div>
                    </div>
                    <footer class="section onepage">
                        <div class="footer_block">
                            <div class="footer_link_block">
                                <ul>
                                    <li class="wtht_mrkr">
                                        <a href="/">
                                            <xsl:call-template name="makeThumbnail">
                                                <!--<xsl:with-param name="alt" select="@alt"/>-->
                                                <xsl:with-param name="pwd" select="$siteInfoPage//property[@name='logotip']/value/@path" />
                                                <xsl:with-param name="element_id" select="@id" />
                                                <xsl:with-param name="width" select="'84'" />
                                                <xsl:with-param name="height" select="'69'" />
                                            </xsl:call-template>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="tel">
                                            <a href="tel:{concat(8, $siteInfoPage//property[@name='prefiks_telefona']/value, $siteInfoPage//property[@name='telefon_v_shapke']/value)}">
                                                <span>
                                            <xsl:value-of select="$siteInfoPage//property[@name='prefiks_telefona']/value"/>
                                        </span>&nbsp;
                                                <xsl:value-of select="$siteInfoPage//property[@name='telefon_v_shapke']/value" />
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="mailto:{$siteInfoPage//property[@name='email']/value}">
                                            <xsl:value-of select="$siteInfoPage//property[@name='email']/value" />
                                        </a>
                                    </li>
                                    <li>
                                        <input type="button" value="вход для дилеров" class="button button__yellow_d button__yellow_d_lightfont header_button_margin" id="button_support"/>
                                    </li>
                                    <li class="copyright">© 2012 -
                                        <xsl:value-of select="document('udata://system/convertDate/now/(Y)')" /> ООО «Дымоход»</li>
                                </ul>
                                <!--меню в подвале-->
                                <xsl:apply-templates select="document('udata://menu/draw/o_zavode')" mode="menu-bottom" />
                                <xsl:apply-templates select="document('udata://menu/draw/catalog')" mode="menu-bottom" />
                                <xsl:apply-templates select="document('udata://menu/draw/uslugi')" mode="menu-bottom" />
                                <xsl:apply-templates select="document('udata://menu/draw/podderzhka')" mode="menu-bottom" />
                            </div>
                        </div>
                    </footer>
                    <div class="loading_resources">
                        <xsl:call-template name="makeThumbnail">
                            <xsl:with-param name="pwd" select="$siteInfoPage//property[@name='logotip']/value/@path" />
                            <xsl:with-param name="element_id" select="@id" />
                            <xsl:with-param name="class" select="'element-animation'" />
                            <xsl:with-param name="width" select="'136'" />
                            <xsl:with-param name="height" select="'112'" />
                        </xsl:call-template>
                       <!--  <svg class="element-animation" xmlns="http://www.w3.org/2000/svg" width="71" height="69" viewBox="0 0 41 39">
                            <path d="M30 17l-8.39 20H17l-.03-9.4 4.2-10.6H30zm3.56-1L23.5 39H23h-.5-11.4-1-.1v-.24L.42 16H0v-1h12v1H1.42l9.26 22H15V20h1v18h6.94l9.62-22H20v-1h14v1h-.44zM12 17v20L4 17h8z" fill="#FFF"/>
                            <circle cx="16" cy="16" r="3" fill="#FFF"/>
                            <path d="M16.23 12.26s1.97-3.29 6.56-3.29h9.73c3.89 0 8.32-4.93 8.32-9.26-.15 4.08-4.69 5.24-8.57 5.24s-5.5-.05-8.82-.05c-3.33 0-7.22 2.17-7.22 7.36z" fill="#FFF"/>
                        </svg> -->

                    </div>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9iCxyR97TuK7IOxdKlx5lf6FsfuQm4MI"></script>

                    <!-- <script src="{$template-resources}dist/components/swiper/dist/js/swiper.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/jquery/dist/jquery.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/scrollreveal/dist/scrollreveal.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/waypoints/lib/noframework.waypoints.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/skrollr/src/skrollr.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/jquery.easing/js/jquery.easing.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/magnific-popup/dist/jquery.magnific-popup.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/jquery-validation/dist/jquery.validate.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/jquery.maskedinput/dist/jquery.maskedinput.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/isotope/dist/isotope.pkgd.min.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/responsive-tabs/js/jquery.responsiveTabs.min.js"></script> -->

                    <script src="https://cdn.jsdelivr.net/g/jquery@3.2.1,jquery.ui@1.11.4,swiper@3.4.2,waypoints@4.0.1,scrollreveal.js@3.3.1,skrollr@0.6.30,jquery.easing@1.3,jquery.magnific-popup@1.0.0,jquery.validation@1.16.0,jquery.maskedinput@1.4.1,isotope@3.0.4,jquery.responsive-tabs@1.6.1"></script>


                    <script src="{$template-resources}dist/components/mixitup/dist/mixitup.min.js"></script>
                    <script src="{$template-resources}dist/components/drift/dist/Drift.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-marker-clusterer/1.0.0/markerclusterer.js"></script>
                    <script src="{$template-resources}dist/js/all.min.js?v=1.1"></script>

                    <!-- <script src="https://cdn.jsdelivr.net/g/jquery.ui@1.11.4"></script> -->
                    <!-- <script src="{$template-resources}dist/components/get-size/get-size.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/ev-emitter/ev-emitter.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/fizzy-ui-utils/utils.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/desandro-matches-selector/matches-selector.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/letteringjs/jquery.lettering.js"></script> -->
                    <!-- <script src="{$template-resources}dist/components/textillate/jquery.textillate.js"></script> -->

                    


                    <!-- <script src="{$template-resources}js/custom.js"></script> -->
                    <!--<script async="async" defer="defer" src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyA0_o-_q0LpshBxPq-YAFdmz6COmzdqbw0&amp;callback=initMap" />-->
                    <!--<script src="{$template-resources}js/maps.js"></script>-->
                    <!-- <xsl:value-of select="document('udata://content/customIncludeQuickEditJs')/udata" disable-output-escaping="yes"/> -->
                    <img src="{$template-resources}dist/img/svg/upbutton.svg" alt="Подняться наверх" title="Подняться наверх" class="upbutton"/>
                </body>

                </html>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:include href="__common.xsl" />
    <xsl:include href="../__common.xsl" />
</xsl:stylesheet>
