<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" exclude-result-prefixes="php">
    <xsl:output encoding="utf-8" indent="yes" method="html" />
    <!-- Вывод каталогов -->
    <xsl:decimal-format name="df" grouping-separator=" " />
    <xsl:template match="pfd_to_dlr">
		<xsl:variable name="page" select="document(concat('upage://',id))/udata" />
    	<xsl:variable name="select_param">
        	<xsl:choose>
        		<xsl:when test="$page//property[@name='diametr_mm']">
        			<xsl:text>diametr_mm</xsl:text>
        		</xsl:when>
        		<xsl:otherwise>
        			<xsl:text>wxb_mm</xsl:text>
        		</xsl:otherwise>
        	</xsl:choose>
        </xsl:variable>
		<html>
			<head>
				<link rel="stylesheet" href="/mpdf60/print_pdf.css" />
			</head>
			<body>
				<div class="www">
					<table id="printtable3">
					    <tr>
					        <td>
					            <img src="/mpdf60/logo2.png" style="width: 150px"/>
					        </td>
					        <td style="font-size: 17px;padding:5px;text-align: right;">
					            ПРОИЗВОДСТВО СИСТЕМ
					            <br/>
					                МОДУЛЬНЫХ ДЫМОХОДОВ
					                <br/>
					                    <div id="printurl">
					                        DymohodVulkan.ru
					                    </div>
					        </td>
					        <td valign="top">
					            <br>
					                <div id="printphone">
					                </div>
					            </br>
					        </td>
					    </tr>
					</table>
					<hr/>
					<table id="printtable1">
						<tr>
							<td colspan="2"><h1 id="printh1"><xsl:value-of select="$page//property[@name='h1']/value"/></h1></td>
						</tr>
						<tr>
			                <td><img class="" alt="" src="{$page//property[@name='izobrazheniya']/value[1]}" style="width: 300px;padding-left:80px; display: block;  max-height: 300px;" /></td>
			                <td><img class="" alt="" src="{$page//property[@name='izobrazheniya']/value[2]}" style="width: 300px;padding-left:80px; display: block;  max-height: 300px;" /></td>
						</tr>
					</table>
					<table class="table">
						<xsl:apply-templates select="$page//group[@name='special']/property" mode="group_item"/>
					</table>
					<div id="printdesc">
				         <xsl:value-of select="$page//property[@name='opisanie']/value" disable-output-escaping="yes"/>
				    </div>
				    <table id="printtable1">
						<tr>
							<xsl:apply-templates select="$page//property[@name='izobrazheniya']/value" mode="itemiz" />
                		</tr>
                	</table>
					<table class="t_dotted_lines">
                		<xsl:apply-templates select="document(concat('udata://catalog/getObjectParam/', id, '/', $select_param))/udata/lines" mode="data-sheet-itemsw"/>
                	</table>
				</div>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="value[1]" mode="itemiz"/>
	<xsl:template match="value[2]" mode="itemiz"/>


	<xsl:template match="value" mode="itemiz">
		<td>
			<img class="" alt="" src="{.}" style="width: 300px;padding-left:80px; display: block;  max-height: 300px;"/>
		</td>
	</xsl:template>


	<xsl:template match="lines" mode="data-sheet-itemsw">
		<xsl:apply-templates select="item" mode="data-sheet-itemq"/>
	</xsl:template>

	<xsl:template match="item" mode="data-sheet-itemq">
		<tr>
			<td><xsl:value-of select="@name"/></td>
			<xsl:apply-templates select="items/item" mode="data-sheet-item2-val"/>
		</tr>
	</xsl:template>
	<xsl:template match="item" mode="data-sheet-item2-val">
		<td><xsl:value-of select="@value"/></td>
	</xsl:template>

	<xsl:template match="property" mode="group_item">
		<tr>
			<th>
				<xsl:value-of select="title"/>
			</th>
			<td>
				<table>
					<tr>
						<xsl:apply-templates select="value/item" mode="property_item">
							<xsl:sort select="@id" order="ascending"/>
						</xsl:apply-templates>
					</tr>
				</table>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="item" mode="property_item">
		<td><xsl:value-of select="@name"/></td>
	</xsl:template>



</xsl:stylesheet>
