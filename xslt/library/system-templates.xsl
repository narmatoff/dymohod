<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">
	
	<xsl:template match="value" mode="nashi_partnery">
		<div class="logo_list_item">
			<xsl:call-template name="makeThumbnail">
				<xsl:with-param name="alt" select="@alt"/>
				<xsl:with-param name="pwd" select="@path"/>
				<xsl:with-param name="width" select="'127'"/>
				<xsl:with-param name="height" select="'auto'"/>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="main-favorit">
		<xsl:param name="count" />
		<xsl:param name="prefer_counter"/>
		<xsl:param name="prefer_name"/>
		<xsl:param name="prefer_content"/>

		<div class="prefers_counters_{$count}">
			<div class="prefer_counter"><xsl:value-of select="$prefer_counter" disable-output-escaping="yes"/></div>
			<div class="prefer_name"><xsl:value-of select="$prefer_name"/></div>
			<div class="prefer_content"><xsl:value-of select="$prefer_content" disable-output-escaping="yes"/></div>
		</div>
	</xsl:template>
	
	<xsl:template match="udata[@module = 'data'][@method = 'getGuideItems']" mode="main-partner">
		<xsl:apply-templates select="items" mode="main-partner_items"/>
	</xsl:template>

	<xsl:template match="items" mode="main-partner_items">
		<div class="full_width full_width__darkgrey our_partners">
	      	<div class="big_heading big_heading_white">Наши партнеры</div>
	      	<div class="logo_list">
		     	<xsl:apply-templates select="item" mode="main-partner_items_item"/>   
		    </div>
    	</div>
	</xsl:template>
	
	<xsl:template match="item" mode="main-partner_items_item">
		<xsl:variable name="item" select="document(concat('uobject://', @id))" />
		<div class="logo_list_item">
			<!-- <xsl:call-template name="makeThumbnail">
			        		<xsl:with-param name="alt" select="name"/>
			        		<xsl:with-param name="pwd" select="$item//property[@name='izobrazhenie_chb']/value/@path"/>
			        		<xsl:with-param name="element_id" select="@id"/>
			        		<xsl:with-param name="width" select="'auto'"/>
			        		<xsl:with-param name="height" select="'44'"/>
			        	</xsl:call-template> -->
			<img src="{$item//property[@name='izobrazhenie_chb']/value/@path}" alt="{name}"/>
		</div>
	</xsl:template>

	<xsl:template match="item" mode="nashi_priemuwestva">
		<xsl:variable name="item" select="document(concat('uobject://', @id))" />
		<div class="ease_with_us_item"><img src="{$item//property[@name='izobrazhenie']/value}" alt="{@name}"/>
			<span><xsl:value-of select="@name"/></span>
          	<xsl:value-of select="$item//property[@name='opisanie']/value" disable-output-escaping="yes"/>
        </div>
	</xsl:template>

    <xsl:template name="date">
        <xsl:param name="time"/>
        <xsl:value-of select="document(concat('udata://system/convertDate/',$time,'/(d.m.Y)'))/udata"/>
    </xsl:template>

    <xsl:template match="property" mode="video_v_shapke">
    	<source src="{value/@path}">
    		<xsl:attribute name="type">
    			<xsl:text>video/</xsl:text>
    			<xsl:choose>
	    			<xsl:when test="value/@ext='ogv'"><xsl:text>ogg</xsl:text></xsl:when>
	    			<xsl:otherwise><xsl:value-of select="value/@ext"/></xsl:otherwise>
    			</xsl:choose>
    		</xsl:attribute>
		</source>
    </xsl:template>

    <!--_______________________________________________________Simple menu________________________________________________________-->

    <xsl:template match="items|value|udata" mode="simple-menu">
        <ul>
            <xsl:apply-templates select="item|page" mode="simple-menu"/>
        </ul>
    </xsl:template>

    <xsl:template match="item|page" mode="simple-menu">
        <li>
            <a href="{@link}">
                <xsl:value-of select="@name|name"/>
            </a>
        </li>
    </xsl:template>

  
	<xsl:template match="property" mode="pre_h">
		<div class="pre_h"><xsl:value-of select="value"/></div>
	</xsl:template>


</xsl:stylesheet>